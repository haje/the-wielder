import json
from io import open

datapath = "../Assets/Resources/Data/"

try:
    unicode('')
except NameError:
    unicode = str

def openjson(filename):
	try: 
		f = open(filename, encoding="utf-8")
		result = json.load(f)
		f.close()
		return result

	except IOError as e:
		print(e)
		return {}

def process(path, filename):
	for key, value in openjson(filename).items():
		for innerkey, innervalue in value.items():
			with open(path + key + "/" + innerkey + ".json", "w", encoding="utf-8") as f:
				f.write(unicode(json.dumps(innervalue, ensure_ascii=False, sort_keys=True, indent=2)))

if __name__ == "__main__":
	process(datapath, "data.json")