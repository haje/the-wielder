﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Wielder.Item
{
    public class ItemSetBase : SerializableBase
    {

        public readonly ReadOnlyCollection<string> ItemSet;
        public readonly ReadOnlyCollection<string> CardSet;
        public readonly ReadOnlyCollection<string> TrinketSet;


        [SerializeField]
        private List<string> itemSet;

        [SerializeField]
        private List<string> cardSet;

        [SerializeField]
        private List<string> trinketSet;

        public ItemSetBase(string fileName) : base(fileName)
        {
            ItemSet = itemSet.AsReadOnly();
            CardSet = cardSet.AsReadOnly();
            TrinketSet = trinketSet.AsReadOnly();
        }

        protected override string FileDirectory
        {
            get
            {
                return "ItemSets/";
            }
        }
    }
}
