﻿using System;
using System.Reflection;
using UnityEngine;
using Wielder.Passive;
using Wielder.Units;

namespace Wielder.Item
{
    class Trinket : IItem, IPassiveOwner
    {
        public int Price
        {
            get
            {
                return baseData.Price;
            }
        }

        public Sprite Image
        {
            get
            {
                return baseData.ItemImage;
            }
        }

        public string Name
        {
            get
            {
                return baseData.Name;
            }
        }

        public string Description
        {
            get
            {
                return baseData.Description;
            }
        }

        public IItemOwner Owner
        {
            get
            {
                return owner;
            }
            set
            {
                owner = value;
                if (owner == null)
                {
                    GameManager.RemovePassiveOwner(this);
                }
                else if (owner is Unit)
                {
                    //먼저 다른 장신구가 있으면 그 장신구를 버림
                    IItem removeTrinket = null;
                    foreach (IItem item in ((Unit)owner).Inventory)
                    {
                        if (item is Trinket)
                        {
                            removeTrinket = item;
                            break;
                        }
                    }
                    if (removeTrinket != null) ((Unit)owner).DiscardItem(removeTrinket);
                    GameManager.AddPassiveOwner(this);
                }
            }
        }

        public PassiveHandler PassiveHandler
        {
            get
            {
                return passiveHandler;
            }
        }

        private PassiveHandler passiveHandler;
        private IItemOwner owner;
        private ItemBase baseData;

        public int Attrib1;

        public Trinket(string fileName) :
            this(FileResourceManager.GetItemBase(fileName))
        {

        }

        public Trinket(ItemBase data)
        {
            if (data.Type != ItemType.Trinket) throw new ItemException(string.Format("{0}: 아이템 형식이 Trinket이 아닙니다", data.Name));
            baseData = data;
            passiveHandler = new PassiveHandler();
            Attrib1 = 0;
            MethodInfo effectGetter = typeof(ItemEffectContainer).GetMethod(data.EffectFunctionName, BindingFlags.NonPublic | BindingFlags.Static);
            if (effectGetter == null) throw new ItemException(string.Format("{0}: 함수 이름 {1}에 오류가 있습니다.", data.Name, data.EffectFunctionName));
            effectGetter.Invoke(null, new object[] { this });

        }
    }
}
