﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wielder.Cards;

namespace Wielder.Item
{
    class ItemSet
    {

        public List<PassiveItem> PassiveItems { get; private set; }
        public List<Card> Cards { get; private set; }
        public List<Trinket> Trinkets { get; private set; }
        public ItemSet(string filebase) : this(FileResourceManager.GetItemSetBase(filebase))
        {

        }

        public ItemSet(ItemSetBase isBase)
        {
            PassiveItems = new List<PassiveItem>();
            Trinkets = new List<Trinket>();
            Cards = new List<Card>();
            foreach(string cardName in isBase.CardSet)
            {
                Cards.Add(new Card(cardName));
            }
            foreach(string itemName in isBase.ItemSet)
            {
                PassiveItems.Add(new PassiveItem(itemName));
            }
            foreach(string trinketName in isBase.TrinketSet)
            {
                Trinkets.Add(new Trinket(trinketName));
            }
        }
    }
}
