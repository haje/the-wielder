﻿using System;
using UnityEngine;
using Wielder.Passive;
using Wielder.Units;

namespace Wielder.Item
{
    class PassiveItem : IItem
    {
        private ItemBase baseData;
        private UnitModifierEffect itemEffect;

        public PassiveItem(string baseFileName) :
            this(FileResourceManager.GetItemBase(baseFileName))
        { }

        private PassiveItem(ItemBase data)
        {
            if (data.Type != ItemType.PassiveItem) throw new ItemException(string.Format("{0}: 아이템 형식이 PassiveItem이어야 합니다.", data.Name));
            baseData = data;
            Effect = new UnitModifier(baseData.Name, baseData.Description, Unit.UnitModifierEffectContainer.GetUnitModifierEffectFromName(baseData.EffectFunctionName), false);
        }

        public int Price
        {
            get
            {
                return baseData.Price;
            }
        }

        public Sprite Image
        {
            get
            {
                return baseData.ItemImage;
            }
        }

        public string Name
        {
            get
            {
                return baseData.Name;
            }
        }

        public string Description
        {
            get
            {
                return baseData.Description;
            }
        }

        public IItemOwner Owner
        {
            get
            {
                return owner;
            }
            set
            {
                owner = value;
                if (owner as Unit != null) //아이템 안 빼는것을 전제로 함
                    ((Unit)Owner).AddModifier(Effect);
            }
        }

        private IItemOwner owner;

        public UnitModifier Effect;
    }
}
