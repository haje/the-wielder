﻿namespace Wielder.Item
{
    public enum ItemType
    {
        CardPack,
        Trinket,
        PassiveItem,
    }
}
