﻿using System.Collections.Generic;
using UnityEngine;
using Wielder.Cards;

namespace Wielder.Item
{
    public class CardPack : IItem
    {

        public int Price
        {
            get
            {
                return price;
            }
        }

        public Sprite Image
        {
            get
            {
                return FileResourceManager.GetSprite("Card");
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public string Description
        {
            get { return description; }
        }

        public IItemOwner Owner
        {
            get; set;
        }
        
        public List<Card> Cards;
        int price;
        string name;
        string description;

        private CardPack(CardClass cardClass, Rarity rarity)
        {
            Cards = new List<Card>();
            List<string> cardList;
            Card card;
            name = " 계열 팩";
            description = " 계열 ";
            price = Random.value > 0.2 ? 15 : 7; // pseudo-아이작식 구현

            int random;
            switch (rarity)
            {
                case Rarity.Common:
                    description += "무작위 등급 카드를 3장 얻습니다.";
                    for (int i = 0; i < 3; i++)
                    {
                        Rarity select = selectRarity(cardClass);
                        cardList = FileResourceManager.GetCardList(cardClass, select);

                        if(cardList.Count == 0)
                        {
                            Debug.LogAssertion(cardClass + "계열" + select + "등급 카드가 존재하지 않음, 카드팩 생성");
                            break;
                        }

                        random = Random.Range(0, cardList.Count);
                        card = new Card(cardList[random]);
                        Cards.Add(card);
                    }
                    break;
                case Rarity.Epic:
                    description += "영웅 등급 카드를 1장 얻습니다.";
                    cardList = FileResourceManager.GetCardList(cardClass, rarity);

                    if (cardList.Count == 0)
                    {
                        Debug.LogAssertion(cardClass + "계열" + rarity + "등급 카드가 존재하지 않음, 카드팩 생성");
                        break;
                    }

                    random = Random.Range(0, cardList.Count);
                    card = new Card(cardList[random]);
                    Cards.Add(card);
                    break;
                case Rarity.Legendary:
                    description += "전설 등급 카드를 1장 얻습니다.";
                    cardList = FileResourceManager.GetCardList(cardClass, rarity);

                    if (cardList.Count == 0)
                    {
                        Debug.LogAssertion(cardClass + "계열" + rarity + "등급 카드가 존재하지 않음, 카드팩 생성");
                        break;
                    }

                    random = Random.Range(0, cardList.Count);
                    card = new Card(cardList[random]);
                    Cards.Add(card);
                    break;
                default:
                    Debug.LogAssertion("카드 등급 에러, cardpack 생성자");
                    break;
            }
            switch (cardClass)
            {
                case (CardClass)0:
                    name = "생존술" + name;
                    description = "생존술" + description;
                    break;
                case (CardClass)1:
                    name = "교전술" + name;
                    description = "교전술" + description;
                    break;
                case (CardClass)2:
                    name = "사격술" + name;
                    description = "사격술" + description;
                    break;
                case (CardClass)3:
                    name = "대지 마법" + name;
                    description = "대지 마법" + description;
                    break;
                case (CardClass)4:
                    name = "가호" + name;
                    description = "가호" + description;
                    break;
                case (CardClass)5:
                    name = "주술" + name;
                    description = "주술" + description;
                    break;
                case (CardClass)6:
                    name = "지략" + name;
                    description = "지략" + description;
                    break;
                case (CardClass)7:
                    name = "금전주의" + name;
                    description = "금전주의" + description;
                    break;
                default:
                    Debug.LogAssertion("카드 계열 오류, 카드팩 생성자");
                    break;
            }
        }

        public static CardPack GenerateCardPack(CardClass cardClass, Rarity rarity)
        {
            CardPack pack = new CardPack(cardClass, rarity);
                
            return pack;
        }

        private Rarity selectRarity(CardClass cardClass)
        {
            Rarity rarity = (Rarity)0;
            int[] percent;
            switch (cardClass)
            {
                case (CardClass)0:
                    percent = new int[] { 60, 30, 10, 0 };
                    break;
                case (CardClass)1:
                case (CardClass)2:
                case (CardClass)3:
                    percent = new int[] { 60, 25, 10, 5 };
                    break;
                case (CardClass)4:
                    percent = new int[] { 35, 35, 15, 15 };
                    break;
                case (CardClass)5:
                    percent = new int[] { 100, 0, 0, 0 };
                    break;
                case (CardClass)6:
                    percent = new int[] { 60, 40, 0, 0 };
                    break;
                case (CardClass)7:
                    percent = new int[] { 60, 30, 10, 0 };
                    break;
                default:
                    Debug.LogError("잘못된 카드 계열 인풋, CardPack");
                    percent = new int[] { 100, 0, 0, 0 };
                    break;
            }

            int random = Random.Range(0, 100);
            int seed = 0;
            for(int i = 0; i< percent.Length; i++)
            {
                seed += percent[i];
                if (random < seed)
                {
                    rarity = (Rarity)(i + 1);
                    return rarity;
                }
            }
            Debug.LogError("잘못된 카드 등급 반환, CardPack");
            return rarity;
        }
    }
}
