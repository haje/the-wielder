﻿using Wielder.Cards;
using Wielder.Passive.Triggers;
using Wielder.Passive.Replacements;
using Wielder.Units;
using Wielder.Visual;

namespace Wielder.Item
{
    public static class ItemEffectContainer
    {
        static void PowerUp(Trinket trinket)
        {
            trinket.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                (amount, damageDealer, damageTaker) => amount + 1,
                (amount, damageDealer, damageTaker) => trinket.Owner is Unit && damageDealer is Card && ((Unit)trinket.Owner).GetCards().Contains((Card)damageDealer)));
        }

        static void WhiteGem(Trinket trinket)
        {
            trinket.PassiveHandler.DealDamage.Add(new DealDamageTrigger(
                (passiveOwner, dealer, damageTaker, amount) => damageTaker.GetShield(1),
                (passiveOwner, dealer, damageTaker, amount) => GeneralVisualEffectContainer.GenerateTempFlashEffect(trinket)(),
                (passiveOwner, dealer, damageTaker, amount) => damageTaker == ((Trinket)passiveOwner).Owner && UnityEngine.Random.value > 0.9
                ));
        }

        static void BloodRose(Trinket trinket)
        {
            trinket.PassiveHandler.UnitDeath.Add(new UnitDeathTrigger(
                (passiveOwner, deadUnit) => {
                    ((Unit)((Trinket)passiveOwner).Owner).GetHealed(1);
                    trinket.Attrib1 = 0;
                },
                (passiveOwner, deadUnit) => GeneralVisualEffectContainer.GenerateTempFlashEffect(trinket)(),
                (passiveOwner, deadUnit) => trinket.Attrib1 >= 20
                ));
            trinket.PassiveHandler.UnitDeath.Add(new UnitDeathTrigger(
                (passiveOwner, deadUnit) => trinket.Attrib1++,
                (passiveOwner, deadUnit) => GeneralVisualEffectContainer.GenerateTempFlashEffect(trinket)(),
                (passiveOwner, deadUnit) => true
                ));
        }

        static void HolyGreenTreeAmulet(Trinket trinket)
        {
            trinket.PassiveHandler.AfterCast.Add(new AfterCastTrigger(
                (passiveOwner, effectParam) => ((Unit)((Trinket)passiveOwner).Owner).GetShield(1),
                (passiveOwner, effectParam) => GeneralVisualEffectContainer.GenerateTempFlashEffect(trinket)(),
                (passiveOwner, effectParam) => effectParam.Caster == ((Unit)((Trinket)passiveOwner).Owner) && UnityEngine.Random.value > 0.95
                ));
        }

        static void Coin(Trinket trinket)
        {
            trinket.PassiveHandler.AfterCast.Add(new AfterCastTrigger(
                (passiveOwner, effectParam) => ((Unit)((Trinket)passiveOwner).Owner).CurrentAP++,
                (passiveOwner, effectParam) => GeneralVisualEffectContainer.GenerateTempFlashEffect(trinket)(),
                (passiveOwner, effectParam) => effectParam.Caster == ((Unit)((Trinket)passiveOwner).Owner) && UnityEngine.Random.value > 0.95
                ));
        }

        static void PurifiedHeart(Trinket trinket)
        {
            trinket.PassiveHandler.DealDamage.Add(new DealDamageTrigger(
                (passiveOwner, dealer, damageTaker, amount) =>
                {
                    damageTaker.GetHealed(1);
                    damageTaker.DiscardItem(trinket);
                },
                (passiveOwner, dealer, damageTaker, amount) => GeneralVisualEffectContainer.GenerateTempFlashEffect(trinket)(),
                (passiveOwner, dealer, damageTaker, amount) => damageTaker == ((Trinket)passiveOwner).Owner
                ));
        }
    }

}
