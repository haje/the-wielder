﻿namespace Wielder.Item
{
    public enum Rarity
    {
        Banned,
        Common,
        Rare,
        Epic,
        Legendary,
    }
}
