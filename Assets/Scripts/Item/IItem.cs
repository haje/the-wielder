﻿using System;
using UnityEngine;

namespace Wielder.Item
{
    public interface IItem
    {
        int Price
        {
            get;
        }

        Sprite Image
        {
            get;
        }

        string Name
        {
            get;
        }

        string Description
        {
            get;
        }

        IItemOwner Owner
        {
            get;
            set;
        }
        
    }

    public class ItemException : Exception
    {
        public ItemException() : base() { }
        public ItemException(string msg) : base(msg) { }
    }
}
