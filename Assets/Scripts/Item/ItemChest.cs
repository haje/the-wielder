﻿using System.Collections.Generic;
using UnityEngine;
using Wielder.Cards;

namespace Wielder.Item
{
    public class ItemChest : IItem
    {

        public int Price
        {
            get
            {
                return price;
            }
        }

        public Sprite Image
        {
            get
            {
                return FileResourceManager.GetSprite("Chest_Brown");
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public string Description
        {
            get { return description; }
        }

        public IItemOwner Owner
        {
            get; set;
        }
        
        int price;
        string name;
        string description;

        public ItemChest(Rarity rarity = Rarity.Common)
        {
            name = "일반 상자";
            description = "상자를 얻습니다.\n계속 살 수 있습니다.";
            price = 15;
        }
    }
}
