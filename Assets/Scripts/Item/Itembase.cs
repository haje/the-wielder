﻿using UnityEngine;

namespace Wielder.Item
{    
    public class ItemBase : SerializableBase
    {
        public readonly ItemType Type;
        public readonly string Name;
        public readonly int Price;
        public Sprite ItemImage;
        public readonly string Description;
        public readonly string EffectFunctionName;
        public readonly string EffectFunctionType;

        [SerializeField]
        private ItemType type;

        [SerializeField]
        private string name;

        [SerializeField]
        private int price;

        [SerializeField]
        private string imageFileName;

        [SerializeField]
        private string description;

        [SerializeField]
        private string effectFunctionName;

        [SerializeField]
        private string effectFunctionType;

        public ItemBase(string fileName) : base(fileName)
        {
            Type = type;
            Name = name;
            Price = price;
            ItemImage = FileResourceManager.GetSprite(imageFileName);
            Description = description;
            EffectFunctionName = effectFunctionName;
            EffectFunctionType = effectFunctionType;
        }
        
        protected override string FileDirectory
        {
            get
            {
                return "Items/";
            }
        }

    }
}
