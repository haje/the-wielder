﻿using UnityEngine;

namespace Wielder
{
    public abstract class SerializableBase
    {
        protected abstract string FileDirectory { get; }

        public SerializableBase(string fileName)
        {
            //Debug.Log(fileName);
            TextAsset textAsset = Resources.Load(
                "Data/" + FileDirectory + fileName) as TextAsset;
            if (textAsset == null)
            {
                throw new FileNotFoundException("잘못된 파일 이름: " + FileDirectory + fileName);
            }

            JsonUtility.FromJsonOverwrite(textAsset.text, this);
        }
    }
    public class FileNotFoundException : System.Exception
    {
        public FileNotFoundException(string msg) : base(msg) { }
    }
}
