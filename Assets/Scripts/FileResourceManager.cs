﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Wielder.Cards;
using Wielder.Units;
using Wielder.Maps;
using Wielder.Item;

namespace Wielder
{
    public struct CardInfo
    {
        public CardClass CardClass;
        public Rarity Rarity;

        public CardInfo(CardClass cardClass, Rarity rarity)
        {
            CardClass = cardClass;
            Rarity = rarity;
        }
    }

    public struct CardList
    {
        public List<string> CardNames;
        
        public CardList(int notUsed)
        {
            CardNames = new List<string>();
        }

        public void AddCard(string name)
        {
            CardNames.Add(name);
        }
    }

    public static class FileResourceManager
    {
        private static Dictionary<string, CardBase> cardBases = new Dictionary<string, CardBase>();
        private static Dictionary<string, UnitBase> unitBases = new Dictionary<string, UnitBase>();
		private static Dictionary<string, RoomBase> roomBases = new Dictionary<string, RoomBase>();
        private static Dictionary<string, ItemSetBase> itemSetBases = new Dictionary<string, ItemSetBase>();
        private static Dictionary<string, ItemBase> itemBases = new Dictionary<string, ItemBase>();
        private static Dictionary<CardInfo, CardList> cardDictionary = new Dictionary<CardInfo, CardList>();
        private static T GetBase<T>(string fileName, Dictionary<string, T> dict) 
            where T : SerializableBase
        {
            if (dict.ContainsKey(fileName))
                return dict[fileName];
            else
            {
                T newT = (T)Activator.CreateInstance(typeof(T), fileName);
                dict.Add(fileName, newT);
                return newT;
            }
        }

        public static CardBase GetCardBase(string fileName)
        {
            return GetBase(fileName, cardBases);
        }

        public static UnitBase GetUnitBase(string fileName)
        {
            return GetBase(fileName, unitBases);
        }

		public static RoomBase GetRoomBase(string fileName)
		{
			return GetBase(fileName, roomBases);
		}

        public static Sprite GetSprite(string fileName)
        {
            return Resources.Load<Sprite>("images/" + fileName);
        }

        public static GameObject GetVisualEffect(string filename)
        {
            return Resources.Load<GameObject>("VisualEffects/" + filename);
        }

        public static ItemSetBase GetItemSetBase(string fileName)
        {
            return GetBase(fileName, itemSetBases);
        }

        public static ItemBase GetItemBase(string fileName)
        {
            return GetBase(fileName, itemBases);
        }
        
        public static AudioClip GetBGM(string filename)
        {
            return Resources.Load<AudioClip>("audio/bgm/" + filename);
        }
        
        public static void AddCardToDictionary(CardBase cardBase, string fileName)
        {
            CardInfo info = new CardInfo(cardBase.CardClass, cardBase.Rarity);
            if (!cardDictionary.ContainsKey(info))        
                cardDictionary.Add(info, new CardList(0));

            cardDictionary[info].AddCard(fileName);
        }

        public static List<string> GetCardList(CardClass cardClass, Rarity rarity)
        {
            List<string> result = new List<string>();
            CardInfo info = new CardInfo(cardClass, rarity);

            if (cardDictionary.ContainsKey(info))
                foreach (string name in cardDictionary[info].CardNames)
                    result.Add(name);

            return result;
        }
    }
}
