﻿using System;
using System.Collections.Generic;
using Wielder.Units;

namespace Wielder.AI
{
    class AIFunctionChainObject
    {
        public Func<Unit, bool> condition { get; private set; }
        public Func<Unit, bool> execute { get; private set; }

        public AIFunctionChainObject(Func<Unit, bool> cond, Func<Unit, bool> func)
        {
            condition = cond;
            execute = func;
        }
    }

    class AIFunctionChain
    {
        private List<AIFunctionChainObject> chain;
        
        private Unit target;
        public AIFunctionChain(Unit executer)
        {
            
            chain = new List<AIFunctionChainObject>();
            
            target = executer;
        }

        public void AddChain(Func<Unit, bool> conditionCheckFunction, Func<Unit, bool> func)
        {
            chain.Add(new AIFunctionChainObject(conditionCheckFunction, func));
        }

        public void AddChain(Func<Unit, bool> func)
        {
            AddChain((unit) => true, func);
        }

        public bool ExecuteChain()
        {
            int index = 0;
            while(index < chain.Count)
            {
                if(chain[index].condition(target) && chain[index].execute(target))
                {
                    return true;
                }
                index++;
            }
            return false;
        }
    }
}
