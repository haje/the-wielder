﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wielder.Cards;
using Wielder.Maps;
using Wielder.Path;
using Wielder.Units;
namespace Wielder.AI
{
    static class AIFunctionContainer
    {

        private static bool TrueFunc(Unit unused)
        {
            return true;
        }

        public static AIFunction GetFunctionFromName(string functionName)
        {
            return DelegateHelper.GetDelegateFromName<AIFunction>(typeof(AIFunctionContainer), functionName);
        }

        static void BasicMelee(Unit unit)
        {
            AIFunctionChain chain = new AIFunctionChain(unit);

            chain.AddChain(
                u => u.PlacedCell.GetDistanceFrom(GameManager.Player.PlacedCell) > 1,
                u => MoveTowards(u, GameManager.Player));

            chain.AddChain(
                TrueFunc,
                u => UseHighestCost(u, GameManager.Player.PlacedCell));

            if (!chain.ExecuteChain()) GameManager.EndUnitTurn();
        }

        static void BasicRanged(Unit unit)
        {
            AIFunctionChain chain = new AIFunctionChain(unit);

            chain.AddChain(
                u => u.PlacedCell.GetDistanceFrom(GameManager.Player.PlacedCell) > 3,
                u => MoveTowards(u, GameManager.Player));

            chain.AddChain(
                u => u.PlacedCell.GetDistanceFrom(GameManager.Player.PlacedCell) <= 2,
                u => MoveAway(u, GameManager.Player));

            chain.AddChain(
                TrueFunc,
                u => UseHighestCost(u, GameManager.Player.PlacedCell));

            if (!chain.ExecuteChain()) GameManager.EndUnitTurn();
        }

        static void Zenta(Unit unit)
        {
            Cell playerCell = GameManager.Player.PlacedCell;
            AIFunctionChain chain = new AIFunctionChain(unit);

            chain.AddChain(
                u =>
                {
                    Card c = u.FindCardInHand("함포 준비");
                    Unit artillery = GameManager.CurrentRoom.FindUnitByName("포병");

                    return artillery != null && artillery.Deck.Count == 0 && c != null && CanUseCard(u, c, u.PlacedCell);
                },
                u => UseCardByName(u, "함포 준비", u.PlacedCell));

            chain.AddChain(
                u =>
                {
                    Card c = u.FindCardInHand("포격");
                    return c != null && CanUseCard(u, c, playerCell);
                },
                u => UseCardByName(u, "포격", playerCell));

            chain.AddChain(
                u =>
                {
                    Card c = u.FindCardInHand("질주");
                    return c != null && CanUseCard(u, c, u.PlacedCell) && u.PlacedCell.GetDistanceFrom(playerCell) < 2;
                },
                u =>
                {
                    Cell furthestCell = u.PlacedCell;
                    int it = 0;
                    for (it = 0; it < 3; it++)
                    {
                        List<HexCoord> adjCoord = furthestCell.Position.AdjacentList();
                        Cell targetCell = playerCell;

                        //다음 타일과 겹치지 않는 3개의 타일을 구함
                        List<HexCoord> moveAwayList = new List<HexCoord>();
                        int distance = unit.PlacedCell.GetDistanceFrom(targetCell);
                        foreach (HexCoord hex in adjCoord)
                        {
                            if (!GameManager.CurrentRoom.Cells.ContainsKey(hex)) continue;
                            if (GameManager.CurrentRoom.Cells[hex].Occupied) continue;
                            if (GameManager.CurrentRoom.Cells[hex].GetDistanceFrom(targetCell) > distance)
                                moveAwayList.Add(hex);
                        }

                        if (moveAwayList.Count == 0) break;
                        furthestCell = GameManager.CurrentRoom.Cells[moveAwayList[UnityEngine.Random.Range(0, moveAwayList.Count)]];
                    }
                    return UseCardByName(u, "질주", furthestCell);
                });

            chain.AddChain(
                u =>
                {
                    Card c = u.FindCardInHand("총격");
                    return c != null && CanUseCard(u, c, playerCell);
                },
                u => UseCardByName(u, "총격", playerCell));


            if (!chain.ExecuteChain()) GameManager.EndUnitTurn();
        }

        static void Artillery(Unit unit)
        {
            bool executed = false;
            if (unit.Hand.Count > 0)
            {
                if (unit.PlacedCell.GetDistanceFrom(GameManager.Player.PlacedCell) > 2)
                    executed = executed || MoveTowards(unit, GameManager.Player);
                if (!executed)
                    executed = executed || UseHighestCost(unit, GameManager.Player.PlacedCell);
            }
            else
            {
                executed = executed || MoveAway(unit, GameManager.Player);
            }
            if (!executed) GameManager.EndUnitTurn();
        }

        static void Aram(Unit unit)
        {
            // Todo 더 자세히 구현하기
            BasicMelee(unit);
        }

        static void Zas(Unit unit)
        {
            
            AIFunctionChain chain = new AIFunctionChain(unit);
            chain.AddChain(u => u.FindCardInHand("구울 소환") != null, u => UseCardByName(u, "구울 소환", GetNearestCell(unit.PlacedCell.Position.AdjacentList().Select(coord => GameManager.CurrentRoom.Cells[coord]).ToList(), GameManager.Player.PlacedCell)));
            chain.AddChain(u => u.FindCardInHand("좀비 소환") != null, u => UseCardByName(u, "좀비 소환", GetNearestCell(unit.PlacedCell.Position.AdjacentList().Select(coord => GameManager.CurrentRoom.Cells[coord]).ToList(), GameManager.Player.PlacedCell)));
            chain.AddChain(u => CanUseCard(u, u.FindCardInHand("매우 약한 공격"), GameManager.Player.PlacedCell), u => UseCardByName(u, "매우 약한 공격", GameManager.Player.PlacedCell));
            if (!chain.ExecuteChain()) GameManager.EndUnitTurn();
        }

        //Wrapper Functions

        private static bool MoveTowards(Unit unit, Unit target)
        {
            if (unit.CurrentAP < unit.MovementAP) return false;
            Stack<Node> path = PathManager.GetPath(unit.PlacedCell, target.PlacedCell);
            if (path == null) return false;

            Node destination = path.Pop();

            unit.AddMovementToChain(destination.Cell);
            return true;
        }


        private static bool MoveAway(Unit unit, Unit target)
        {
            if (unit.CurrentAP < unit.MovementAP) return false;
            List<HexCoord> adjCoord = unit.PlacedCell.Position.AdjacentList();
            Cell targetCell = target.PlacedCell;

            //다음 타일과 겹치지 않는 3개의 타일을 구함
            List<HexCoord> moveAwayList = new List<HexCoord>();
            int distance = unit.PlacedCell.GetDistanceFrom(targetCell);
            foreach (HexCoord hex in adjCoord)
            {
                if (!GameManager.CurrentRoom.Cells.ContainsKey(hex)) continue;
                if (GameManager.CurrentRoom.Cells[hex].Occupied) continue;
                if (GameManager.CurrentRoom.Cells[hex].GetDistanceFrom(targetCell) > distance)
                    moveAwayList.Add(hex);
            }

            if (moveAwayList.Count == 0) return false;
            unit.AddMovementToChain(GameManager.CurrentRoom.Cells[moveAwayList[UnityEngine.Random.Range(0, moveAwayList.Count)]]);
            return true;
        }

        private static Cell GetNearestCell(List<Cell> candidates, Cell target, bool includeTarget = false, bool mustempty = true)
        {
            int min = 100;
            Cell result = null;
            foreach(Cell cell in candidates)
            {
                if (!includeTarget && cell == target) continue;
                if (mustempty && cell.PlacedUnit != null) continue;
                int dist = Mathf.Abs(target.Position.X - cell.Position.X) + Mathf.Abs(target.Position.Y - cell.Position.Y);
                if (dist < min)
                {
                    result = cell;
                    min = dist;
                }
            }
            return result;
        }

        private static bool CanUseCard(Unit unit, Card card, Cell target)
        {
            if (card == null) return false;
            return unit.CurrentAP >= card.Cost && card.Range >= unit.PlacedCell.GetDistanceFrom(target) && unit.Hand.Contains(card) &&
                GameManager.CurrentRoom.GetAvailableTargetCells(unit, card.Range, card.BaseCardData.TargetConditions, 
                card.BaseCardData.CustomTargetCheckFunction).Contains(target);
        }

        private static bool UseCardByName(Unit unit, string name, Cell target)
        {
            Card c = unit.FindCardInHand(name);
            if (c == null) return false;

            unit.UseCardInHand(c, target);
            return true;
        }

        private static bool UseHighestCost(Unit unit, Cell target)
        {
            int maxCost = -1;
            Card useCard = null;
            foreach (Card c in unit.Hand)
            {
                if (!CanUseCard(unit, c, target)) continue;
                if (c.Cost > maxCost)
                {
                    maxCost = c.Cost;
                    useCard = c;
                }

            }
            if (useCard == null)
                return false;
            unit.UseCardInHand(useCard, target);
            return true;
        }


    }
}
