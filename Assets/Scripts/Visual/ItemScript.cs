﻿using UnityEngine;
using TMPro;
using Wielder.Item;
using Wielder.NonUnits;
using Wielder.Units;
using Wielder.Cards;
using System.Collections.Generic;

namespace Wielder.Visual
{
    public class ItemScript : Indicator<IItem>
    {
        private SpriteRenderer spriteRenderer;

        [SerializeField]
        private GameObject priceTextArea;

        [SerializeField]
        private TextMeshProUGUI priceText;

        [SerializeField]
        private GameObject selectedIndicator;

        private int quantity;

        [SerializeField]
        private GameObject quantityTextArea;

        [SerializeField]
        private TextMeshProUGUI quantityText;

        private List<IItem> linkedComponents;
        
        //여러 아이템을 하나로 묶고 싶을 때
        public bool DuplicateItemMode;

        public override IItem LinkedComponent
        {
            get
            {

                return linkedComponents.Count != 0 ? linkedComponents[0] : null;
            }

            set
            {
                if (value == null)
                {
                    if (linkedComponents.Count > 0)
                    {
                        linkedComponents.RemoveAt(0);
                    }
                }
                else
                {

                    if (DuplicateItemMode)
                    {
                        if (linkedComponents.Count == 0)
                        {
                            linkedComponents.Add(value);
                        }
                        else if (linkedComponents[0].Name == value.Name && linkedComponents[0].GetType() == value.GetType())
                        {
                            linkedComponents.Add(value);
                        }
                    }
                    else
                    {
                        linkedComponents.Clear();
                        linkedComponents.Add(value);
                    }
                }
                if(linkedComponents.Count > 0)
                    RefreshDisplay();
            }
        }

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            linkedComponents = new List<IItem>();
            DuplicateItemMode = true;
        }

        public override void RefreshDisplay()
        {
            spriteRenderer.sprite = LinkedComponent.Image;

            if(PopupUIScript.Instance.CurrentState != PopupState.Shop)
            {
                priceTextArea.SetActive(false);
            }
            else
            {
                priceTextArea.SetActive(true);
                priceText.text = LinkedComponent.Price.ToString();
            }
            
            if(LinkedComponent is ItemChest)
            {
                quantityTextArea.SetActive(true);
                quantityText.text = "inf";
            }
            else if(linkedComponents.Count <= 1)
            {
                quantityTextArea.SetActive(false);
            }
            else
            {
                quantityTextArea.SetActive(true);
                quantityText.text = linkedComponents.Count.ToString();
            }
        }

        private void OnMouseOver()
        {
            if (LinkedComponent is Card)
                CardInfoManager.Instance.Display((Card)LinkedComponent, transform.position);
            else
                ItemInfoManager.Instance.Display(LinkedComponent, transform.position);
        }

        private void OnMouseExit()
        {
            if (LinkedComponent is Card)
                CardInfoManager.Instance.StopDisplay();
            else
                ItemInfoManager.Instance.StopDisplay();
        }

        private void OnMouseUpAsButton()
        {
            switch(PopupUIScript.Instance.CurrentState)
            {
                case PopupState.Shop:
                    ShopInteractionScript.Instance.InteractItem(this);
                    break;
                case PopupState.Inventory:
                    InventoryScript.Instance.InteractItem(this);
                    break;
                case PopupState.Chest:
                    ChestInteractionScript.Instance.InteractItem(this);
                    break;
            }
            
        }

        public void Select()
        {
            selectedIndicator.SetActive(true);
        }

        public void Unselect()
        {
            selectedIndicator.SetActive(false);
        }

        public void SetPrice(int price)
        {
            priceText.text = price.ToString();
        }

        public void AddLinkedItem(IItem item)
        {
            if(LinkedComponent == null)
            {
                LinkedComponent = item;
            }
            linkedComponents.Add(item);
        }
        
    }
}
