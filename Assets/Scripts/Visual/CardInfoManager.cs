﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Wielder.Units;
using Wielder.Cards;

namespace Wielder.Visual
{
    public class CardInfoManager : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static CardInfoManager instance;

        public static CardInfoManager Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private Card displayingCard;

        public const float XDistance = 5.7f; // TermInfoManager와의 x 거리

        public const float YHeight = 5.5f; // TermInfoManager의 초기 y 높이

        [SerializeField]
        private GameObject displayer;

        [SerializeField]
        private Image image;

        [SerializeField]
        private TextMeshProUGUI nameText;

        [SerializeField]
        private TextMeshProUGUI apText;

        [SerializeField]
        private TextMeshProUGUI rangeText;

        [SerializeField]
        private TextMeshProUGUI descriptionText;

        private List<Card> cardsOfUnit;

        [SerializeField]
        private GameObject leftArrow;

        [SerializeField]
        private GameObject rightArrow;

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
            cardsOfUnit = new List<Card>();
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.A))
                DisplayPrevious();

            if (Input.GetKeyUp(KeyCode.D))
                DisplayNext();
        }

        private void Display(Card card) // CardInfoManager의 위치를 바꿀 필요가 없는 경우
        {
            Debug.Assert(card != null);
            displayingCard = card;
            RefreshDisplay();
            TermInfoManager.Instance.Display(card, new Vector3(transform.position.x + XDistance, transform.position.y + YHeight, 0));
        }

        public void Display(Card card, Vector2 pos)
        {
            if (displayingCard == card)
                return;

            SetPosition(pos);
            displayer.SetActive(true);
            Display(card);
        }

        public void StopDisplay()
        {
            displayer.SetActive(false);
            displayingCard = null;
            cardsOfUnit = new List<Card>();
            TermInfoManager.Instance.StopDisplay();
        }

        private void SetPosition(Vector2 pos)
        {
            float x = Mathf.Min(5, pos.x);
            float y = Mathf.Min(-1.5f, Mathf.Max(-5, pos.y));
            transform.position = new Vector3(x, y, 0);
        }

        private void RefreshDisplay()
        {
            RefreshArrow();
            image.sprite = displayingCard.BaseCardData.CardImage;
            nameText.text = displayingCard.Name;
            apText.text = displayingCard.Cost.ToString();
            rangeText.text = displayingCard.Range.ToString();
            descriptionText.text = " " + displayingCard.Description;
        }

        public void DisplayUnitCards(Unit unit, Vector2 pos) // 몬스터의 카드 풀을 표시해야 할 때
        {
            if (unit.UnitType != UnitType.Monster)
                return;

            cardsOfUnit = new List<Card>();

            foreach (Card card in unit.GetCards())
            {
                if (!cardsOfUnit.Any(element => element.Name == card.Name))
                    cardsOfUnit.Add(card);
            }

            if (cardsOfUnit.Count > 0)
                Display(cardsOfUnit[0], pos);
        }

        private void DisplayPrevious()
        {
            if (cardsOfUnit.Count < 2)
                return;

            int index = cardsOfUnit.IndexOf(displayingCard);
            if (index > 0)
                Display(cardsOfUnit[index - 1]);
        }

        private void DisplayNext()
        {
            if (cardsOfUnit.Count < 2)
                return;

            int index = cardsOfUnit.IndexOf(displayingCard);
            if (index < cardsOfUnit.Count - 1)
                Display(cardsOfUnit[index + 1]);
        }

        private void RefreshArrow()
        {
            if (cardsOfUnit.Count == 0)
            {
                leftArrow.SetActive(false);
                rightArrow.SetActive(false);
                return;
            }

            Debug.Assert(cardsOfUnit.Contains(displayingCard));
            int index = cardsOfUnit.IndexOf(displayingCard);

            if (index < cardsOfUnit.Count - 1)
                rightArrow.SetActive(true);
            else
                rightArrow.SetActive(false);

            if (index > 0)
                leftArrow.SetActive(true);
            else
                leftArrow.SetActive(false);
        }
    }
}
