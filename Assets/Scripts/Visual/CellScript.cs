﻿using UnityEngine;
using Wielder.Maps;

namespace Wielder.Visual
{
    public class CellScript : Indicator<Cell>
    {
        private Color baseColor = Color.white;
        public Color PingPongColor = Color.white;
        public bool PingPong = false;

        SpriteRenderer spriteRenderer;

        public override Cell LinkedComponent
        {
            get
            {
                return base.LinkedComponent;
            }

            set
            {
                if (value != null)
                {
                    value.Indicator = this;
                }
                base.LinkedComponent = value;
            }
        }

        public Color BaseColor
        {
            get
            {
                return baseColor;
            }

            set
            {
                baseColor = spriteRenderer.color = value;
            }
        }

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            transform.localScale *= worldConversionScale;
        }

        private void Update()
        {
            if (PingPong)
                spriteRenderer.color = Color.Lerp(BaseColor, PingPongColor, Mathf.PingPong(Time.time, 1));
            else
                spriteRenderer.color = BaseColor;
        }

        public override void RefreshDisplay()
        {
            transform.localPosition = LinkedComponent.Position.ConvertToWorldPosition(
                worldConversionScale, worldConversionStart);
        }

        private void OnMouseOver()
        {
            UIManagerScript.Instance.CellMouseOvered(this);
            spriteRenderer.color = Color.Lerp(BaseColor, PingPongColor, Mathf.PingPong(Time.time, 1));
            UnitInfoManager.Instance.CellMouseOver(LinkedComponent,
                LinkedComponent.Position.ConvertToWorldPosition(worldConversionScale, worldConversionStart));
        }

        private void OnMouseExit()
        {
            UIManagerScript.Instance.CellMouseExited(this);
            UnitInfoManager.Instance.StopDisplay();
            CardInfoManager.Instance.StopDisplay();
        }

        private void OnMouseUpAsButton()
        {
            UIManagerScript.Instance.CellClicked(this);
        }
    }
}
