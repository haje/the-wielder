﻿using UnityEngine;
using TMPro;
using Wielder.Units;

namespace Wielder.Visual
{
    public class UnitScript : Indicator<Unit>
    {
        private SpriteRenderer spriteRenderer;

        [SerializeField]
        private GameObject hpDisplay;

        [SerializeField]
        private TextMeshProUGUI hpText;

        [SerializeField]
        private GameObject shieldDisplay;

        [SerializeField]
        private TextMeshProUGUI shieldText;

        private const float hpYCalibration = -0.5f;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            transform.localScale *= worldConversionScale;
        }

        public override Unit LinkedComponent
        {
            get
            {
                return base.LinkedComponent;
            }

            set
            {
                if (value != null)
                {
                    value.Indicator = this;
                }
                base.LinkedComponent = value;
            }
        }

        public override void RefreshDisplay()
        {
            spriteRenderer.sprite = LinkedComponent.BaseUnitData.UnitImage;
            hpText.text = LinkedComponent.CurrentRemainingHP.ToString();
            float baseCalibration = LinkedComponent.BaseUnitData.YCalibration;
            transform.position = LinkedComponent.PlacedCell.Indicator.transform.position + new Vector3(0, baseCalibration * worldConversionScale, 0);
            hpDisplay.transform.position = new Vector3(transform.position.x, transform.position.y + hpYCalibration * worldConversionScale, 0);

            if (LinkedComponent.CurrentShield > 0)
            {
                shieldDisplay.SetActive(true);
                shieldDisplay.transform.position = new Vector3(transform.position.x + 0.4f, transform.position.y + hpYCalibration * worldConversionScale, 0);
                shieldText.text = LinkedComponent.CurrentShield.ToString();
            }
            else
                shieldDisplay.SetActive(false);

            spriteRenderer.sortingOrder = LinkedComponent.PlacedCell.Position.Y + 2;
        }
    }
}
