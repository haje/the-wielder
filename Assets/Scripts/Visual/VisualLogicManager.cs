﻿using System.Collections.Generic;
using Wielder.Maps;
using Wielder.NonUnits;
using Wielder.Units;

namespace Wielder.Visual
{
    public static class VisualLogicManager
    {
        /// <summary>
        /// 주어진 Room 및 거기 배치된 모든 물체를 모두 화면에 표시함
        /// </summary>
        /// <param name="room"></param>
        public static void ShowRoom(Room room)
        {
            foreach (Cell cell in room.Cells.Values)
            {
                VisualResourceManagerScript.Instance.FetchCellIndicator().GetComponent<CellScript>().LinkedComponent = cell;

                if (cell.PlacedUnit != null)
                    VisualResourceManagerScript.Instance.FetchUnitIndicator().GetComponent<UnitScript>().LinkedComponent = cell.PlacedUnit;

                foreach (NonUnit nonUnit in cell.PlacedNonUnits)
                    VisualResourceManagerScript.Instance.FetchNonUnitIndicator().GetComponent<NonUnitScript>().LinkedComponent = nonUnit;
            }
        }

        public static void SpawnUnit(Unit unit)
        {
            VisualResourceManagerScript.Instance.FetchUnitIndicator().GetComponent<UnitScript>().LinkedComponent = unit;
        }

        /// <summary>
        /// 현재 Room에 연관된 시각적 자원을 모두 할당 해제함
        /// </summary>
        public static void FreeCurrentRoomResources()
        {
            foreach (Cell cell in GameManager.CurrentRoom.Cells.Values)
            {
                if (cell.PlacedUnit != null)
                    VisualResourceManagerScript.Instance.ReturnUnitIndicatorToPool(cell.PlacedUnit.Indicator.gameObject);

                foreach (NonUnit n in cell.PlacedNonUnits)
                    VisualResourceManagerScript.Instance.ReturnNonUnitIndicatorToPool(n.Indicator.gameObject);

                VisualResourceManagerScript.Instance.ReturnCellIndicatorToPool(cell.Indicator.gameObject);
            }    
        }

        public static void FreeCurrentStageResources()
        {
            FreeCurrentRoomResources();
        }

        /// <summary>
        /// 현재 Room에 있는 모든 장판을 제거함
        /// </summary>
        public static void FreeAllFloors()
        {
            List<NonUnit> floors = new List<NonUnit>();

            foreach (Cell cell in GameManager.CurrentRoom.Cells.Values)
            {
                foreach (NonUnit n in cell.PlacedNonUnits)
                {
                    if (n.NonUnitType == NonUnitType.Floor)
                        floors.Add(n);
                }
            }

            foreach (NonUnit n in floors)
            {
                VisualResourceManagerScript.Instance.ReturnNonUnitIndicatorToPool(n.Indicator.gameObject);
                n.RemoveAllModifiers();
                GameManager.CurrentRoom.RemoveNonUnit(n);
            }
        }
    }
}
