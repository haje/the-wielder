﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Wielder.Cards;

namespace Wielder.Visual
{
    public class TermInfo
    {
        public string TermName;
        public string TermDescription;
        public Sprite Sprite;

        public TermInfo(string name, string description)
        {
            TermName = name;
            TermDescription = description;
        }

        public TermInfo(string name, Sprite sprite)
        {
            TermName = name;
            TermDescription = "";
            Sprite = sprite;
        }
    }

    public class TermInfoManager : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static TermInfoManager instance;

        public static TermInfoManager Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private const float yDistance = -1.8f; // term 정보를 표시하는 오브젝트 사이의 y 거리

        private List<TermScript> displayingTerms;

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;

            displayingTerms = new List<TermScript>();
        }

        private TermInfo GetCardTermInfo(CardTerms cardTerm)
        {
            switch (cardTerm)
            {
                case CardTerms.Burn:
                    return new TermInfo("화상", "턴을 종료할 때마다 피해를 1 입는다");
                case CardTerms.Bind:
                    return new TermInfo("속박", "이동에 필요한 행동력이 999가 된다");
                case CardTerms.Stun:
                    return new TermInfo("기절", "턴 시작 때 얻는 행동력이 0이 된다");
                case CardTerms.Overwhelm:
                    return new TermInfo("압도", "입히는 모든 피해가 0이 된다");
                case CardTerms.Bleed:
                case CardTerms.Penetrate:
                case CardTerms.Silence:
                case CardTerms.Curse:
                    break;
            }

            Debug.LogError(string.Format("Error at TermInfoManager.cs, cardTerm {0}", cardTerm));
            return new TermInfo("Dummy", "Dummy");
        }

        private TermInfo GetCardClassTermInfo(CardClassTerms cardClassTerm)
        {
            switch (cardClassTerm)
            {
                case CardClassTerms.CCT1:
                    return new TermInfo("교전술", FileResourceManager.GetSprite("Icon_Small_Sword_Double"));
                case CardClassTerms.CCT2:
                    return new TermInfo("사격술", FileResourceManager.GetSprite("Icon_Small_Aim"));
                case CardClassTerms.CCT3:
                    return new TermInfo("대지 마법", FileResourceManager.GetSprite("Icon_Small_Rockslide"));
                case CardClassTerms.CCT4:
                    return new TermInfo("가호", FileResourceManager.GetSprite("Icon_Small_Plus"));
                case CardClassTerms.CCT5:
                    Debug.LogError("이 계열은 지금 없음");
                    return new TermInfo("주술", FileResourceManager.GetSprite("Icon_Small_Plus"));
                case CardClassTerms.CCT6:
                    return new TermInfo("지략", FileResourceManager.GetSprite("Icon_Small_Idea"));
                case CardClassTerms.CCT7:
                    return new TermInfo("금전주의", FileResourceManager.GetSprite("Icon_Small_Coin"));
            }

            Debug.LogError(string.Format("Error at TermInfoManager.cs, cardClassTerm {0}", cardClassTerm));
            return new TermInfo("Dummy", FileResourceManager.GetSprite("Icon_Small_Plus"));
        }

        public void Display(Card card)
        {
            foreach (CardTerms cardTerm in Enum.GetValues(typeof(CardTerms)))
                if (card.BaseCardData.CardTerms.HasFlag(cardTerm))
                    ShowNewTerm(GetCardTermInfo(cardTerm));

            foreach (CardClassTerms cardClassTerm in Enum.GetValues(typeof(CardClassTerms)))
                if (card.BaseCardData.CardClassTerms.HasFlag(cardClassTerm))
                    ShowNewTerm(GetCardClassTermInfo(cardClassTerm));
        }

        public void Display(Card card, Vector3 position)
        {
            transform.position = position;
            Display(card);
        }

        public void DisplayTrinketDescription(Vector3 position)
        {
            transform.position = position;
            ShowNewTerm(new TermInfo("장신구", "장신구 아이템은 하나만 가질 수 있다"));
        }

        private void ShowNewTerm(TermInfo termInfo)
        {
            TermScript newTerm = VisualResourceManagerScript.Instance.FetchTermIndicator().GetComponent<TermScript>();
            newTerm.LinkedComponent = termInfo;
            newTerm.transform.localPosition = new Vector3(0, displayingTerms.Count * yDistance, 0);
            displayingTerms.Add(newTerm);
        }

        public void StopDisplay()
        {
            foreach (TermScript termScript in displayingTerms)
                VisualResourceManagerScript.Instance.ReturnTermIndicatorToPool(termScript.gameObject);

            displayingTerms = new List<TermScript>();
        }
    }
}
