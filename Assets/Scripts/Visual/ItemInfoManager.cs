﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Wielder.Item;
using Wielder.Cards;

namespace Wielder.Visual
{
    public class ItemInfoManager : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static ItemInfoManager instance;

        public static ItemInfoManager Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private IItem displayingItem;

        [SerializeField]
        private GameObject displayer;

        [SerializeField]
        private Image image;

        [SerializeField]
        private TextMeshProUGUI nameText;

        [SerializeField]
        private TextMeshProUGUI descriptionText;

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
        }

        public void Display(IItem item, Vector2 pos)
        {
            Debug.Assert(item != null);
            if (displayingItem == item)
                return;

            displayer.SetActive(true);
            displayingItem = item;
            SetPosition(pos);
            RefreshDisplay();

            if (item is Trinket)
                TermInfoManager.Instance.DisplayTrinketDescription(new Vector3(transform.position.x + CardInfoManager.XDistance, transform.position.y + CardInfoManager.YHeight, 0));
        }

        public void StopDisplay()
        {
            displayer.SetActive(false);
            if (displayingItem is Trinket)
                TermInfoManager.Instance.StopDisplay();
            displayingItem = null;
        }

        private void SetPosition(Vector2 pos)
        {
            float x = Mathf.Min(5, pos.x);
            float y = Mathf.Min(-1.5f, Mathf.Max(-5, pos.y));
            transform.position = new Vector3(x, y, 0);
        }

        private void RefreshDisplay()
        {
            image.sprite = displayingItem.Image;
            nameText.text = displayingItem.Name;
            descriptionText.text = " " + displayingItem.Description;
        }
    }
}
