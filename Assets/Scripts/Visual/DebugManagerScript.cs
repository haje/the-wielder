﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using TMPro;
using UnityEngine;
using Wielder.Cards;
using Wielder.Item;
using Wielder.Maps;
using Wielder.Units;

namespace Wielder.Visual
{

    enum DebugMode
    {
        MainMenu,
        Stat,
        Card,
        Unit,
        Item,
        LoadRoom,

    }
    public class DebugManagerScript : MonoBehaviour
    {
        private static DebugManagerScript instance;

        public static DebugManagerScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private string[] cards;
        private string[] units;
        private string[] items;
        private string[] rooms;

        private List<Unit> allUnits;
        private int unitIndex;

        private List<Card> allCards;
        private int cardIndex;

        private List<IItem> allItems;
        private int itemIndex;

        private List<Room> allRooms;
        private int roomIndex;

        private DebugMode currentMode;

        private const string cardDirectory = "Assets/Resources/Data/Cards";
        private const string unitDirectory = "Assets/Resources/Data/Units";
        private const string itemDirectory = "Assets/Resources/Data/Items";
        private const string roomDirectory = "Assets/Resources/Data/Rooms";

        private const int displayingCount = 5;

        private Unit selectedUnit;

        private UIState previousUIState;

        private Room testRoom;

        [SerializeField]
        public List<GameObject> Panels = new List<GameObject>();

        public void Awake()
        {
            transform.localScale = new Vector3(1, 1, 1);
            gameObject.SetActive(false);
            if (!GameStarter.Instance.DebugMode) return;
            Debug.Assert(instance == null);
            transform.localScale = new Vector3(1, 1);
            instance = this;


            currentMode = DebugMode.MainMenu;
            cards = Directory.GetFiles(cardDirectory);
            units = Directory.GetFiles(unitDirectory);
            items = Directory.GetFiles(itemDirectory);
            rooms = Directory.GetFiles(roomDirectory);
            allUnits = new List<Unit>();
            allCards = new List<Card>();
            allItems = new List<IItem>();
            allRooms = new List<Room>();
            if (!GameManager.Initialized) GameManager.Initialize("welldonwarrior"); //야매 코드
            foreach (string card in cards)
            {
                if (card.EndsWith(".meta")) continue;
                string name = card.Substring(cardDirectory.Length + 1, card.Length - cardDirectory.Length - 6);
                try
                {
                    allCards.Add(new Card(card.Substring(cardDirectory.Length + 1, card.Length - cardDirectory.Length - 6)));
                }
                catch (Exception e)
                {
                    continue;
                }

            }

            foreach (string unit in units)
            {
                if (unit.EndsWith(".meta")) continue;
                string name = unit.Substring(unitDirectory.Length + 1, unit.Length - unitDirectory.Length - 6);
                UnitBase unitBase = FileResourceManager.GetUnitBase(name);
                if (unitBase.AI == null) continue;
                try
                {
                    allUnits.Add(new Monster(unitBase));
                }
                catch(Exception e)
                {
                    continue;
                }
                
            }

            foreach (string item in items)
            {
                if (item.EndsWith(".meta")) continue;
                string name = item.Substring(unitDirectory.Length + 1, item.Length - unitDirectory.Length - 6);
                //ItemBase itemBase = FileResourceManager.GetItemBase(name);
                //나중에... itembase 지금 좀 개판이다

            }

            foreach (string room in rooms)
            {
                if (room.EndsWith(".meta")) continue;
                string name = room.Substring(unitDirectory.Length + 1, room.Length - unitDirectory.Length - 6);
                Debug.Log(name);
                try
                {
                    allRooms.Add(new Room(name));
                }
                catch (Exception e)
                {
                    continue;
                }
                
            }
        }

        public void Show()
        {
            gameObject.SetActive(true);
            RefreshDisplay();
            if (testRoom != null)
            {

            }
            else testRoom = GameManager.CurrentRoom;
            UIManagerScript.Instance.SetUIState(UIState.Debug_Waiting);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private void AddUnit(Unit unit, Cell cell)
        {
            testRoom.SpawnUnit(new Monster(unit.BaseUnitData), cell);
            VisualResourceManagerScript.Instance.FetchUnitIndicator()
                        .GetComponent<UnitScript>().LinkedComponent = cell.PlacedUnit;
        }

        public void RemoveUnit(Unit unit)
        {
            GameManager.CurrentRoom.RemoveUnit(unit);
        }

        private void AddPlayerHealth(int amount)
        {
            GameManager.Player.AddHP(amount);
            PlayerInfoManager.Instance.UpdatePlayerInfo();
        }

        private void AddPlayerMana(int amount)
        {
            GameManager.Player.AddMana(amount);
            PlayerInfoManager.Instance.UpdatePlayerInfo();
        }

        private void AddHandSize(int amount)
        {
            GameManager.Player.AddHandSize(amount);
            PlayerInfoManager.Instance.UpdatePlayerInfo();
        }

        private void AddCardToDeck(Card card)
        {
            GameManager.Player.AddCardToDeck(new Card(card.BaseCardData));
        }

        public void ShowStatMenu()
        {
            if (currentMode != DebugMode.Stat)
            {
                currentMode = DebugMode.Stat;
                RefreshDisplay();
            }
        }

        public void ShowCardMenu()
        {
            if (currentMode != DebugMode.Card)
            {
                currentMode = DebugMode.Card;
                RefreshDisplay();
            }
        }

        public void ShowUnitMenu()
        {
            if (currentMode != DebugMode.Unit)
            {
                currentMode = DebugMode.Unit;
                RefreshDisplay();
            }
        }

        public void ButtonClicked(int code)
        {
            if (code == 1 && currentMode != DebugMode.MainMenu)
            {
                currentMode = DebugMode.MainMenu;
                RefreshDisplay();
                return;
            }
            switch (currentMode)
            {
                case DebugMode.MainMenu:
                    currentMode = (DebugMode)((code + 1) / 2);
                    Debug.Log(currentMode);
                    RefreshDisplay();
                    break;
                case DebugMode.Stat:
                    Action<int> func = null;

                    if (code < 4) func = AddPlayerHealth;
                    else if (code < 6) func = AddPlayerMana;
                    else if (code < 8) func = AddHandSize;
                    else
                    {
                        GameManager.Player.GetHealed(500);
                    }
                    func(code % 2 == 0 ? -1 : 1);
                    break;
                case DebugMode.Card:
                    if (code == 3)
                    {
                        cardIndex -= displayingCount;
                        RefreshDisplay();
                        break;
                    }
                    if (code == 15)
                    {
                        cardIndex += displayingCount;
                        RefreshDisplay();
                        break;
                    }
                    if (code % 2 == 1)
                    {
                        //플레이어에게 카드 추가
                        AddCardToDeck(allCards[cardIndex + (code / 2) - 2]);
                    }
                    else
                    {
                        //플레이어가 가진 카드 제거
                        GameManager.Player.RemoveCardInDeck(GameManager.Player.FindCardInDeck(allCards[cardIndex + (code / 2) - 2].Name));
                    }
                    break;
                case DebugMode.Unit:
                    if (code == 3)
                    {
                        unitIndex -= displayingCount;
                        RefreshDisplay();
                        break;
                    }
                    if (code == 15)
                    {
                        unitIndex += displayingCount;
                        RefreshDisplay();
                        break;
                    }
                    selectedUnit = allUnits[unitIndex + (code / 2) - 2];
                    UIManagerScript.Instance.SetUIState(UIState.Debug_Monster);
                    break;
                case DebugMode.LoadRoom:
                    if (code == 3)
                    {
                        roomIndex -= displayingCount;
                        RefreshDisplay();
                        break;
                    }
                    if (code == 15)
                    {
                        roomIndex += displayingCount;
                        RefreshDisplay();
                        break;
                    }
                    testRoom = new Room(allRooms[roomIndex + (code / 2) - 2]);
                    GameManager.ReloadRoom(testRoom);
                    break;
            }
        }

        public void CellClicked(Cell cell)
        {
            AddUnit(selectedUnit, cell);
            selectedUnit = null;
            UIManagerScript.Instance.SetUIState(UIState.Debug_Waiting);
        }

        public void ResetCharacter()
        {
            GameManager.Player.ResetStat();
        }

        public void RestartRoom(Direction dir)
        {
            Hide();
            GameManager.RestartRoom(new Room(testRoom), dir);
        }

        private void DisablePanels()
        {
            foreach (GameObject panel in Panels)
            {
                panel.SetActive(false);
            }
        }

        public void RestoreTestRoom()
        {
            Debug.Assert(testRoom != null);
            GameManager.ReloadRoom(testRoom);
            UIManagerScript.Instance.SetUIState(UIState.Debug_Waiting);
        }

        private void SetPanelTextByMode()
        {
            List<string> items = new List<string>();
            switch (currentMode)
            {
                case DebugMode.Card:
                    for (int i = 0; i < 6 && cardIndex + i < cards.Length; i++)
                    {
                        items.Add(cards[cardIndex + i]);
                    }
                    break;
                case DebugMode.Unit:
                    for (int i = 0; i < 6 && cardIndex + i < cards.Length; i++)
                    {
                        items.Add(units[cardIndex + i]);
                    }
                    break;
            }
            for (int i = 1; i < 7; i++)
            {
                SetPanelText(Panels[i], items[i]);
            }
        }

        private void SetPanelText(GameObject panel, string text)
        {
            Debug.Assert(Panels.Contains(panel));
            panel.transform.Find("Text1").GetComponent<TextMeshProUGUI>().text = text;
        }

        private void RefreshDisplay()
        {
            DisablePanels();
            switch (currentMode)
            {
                case DebugMode.MainMenu:
                    SetPanelText(Panels[0], "스탯 메뉴");
                    SetPanelText(Panels[1], "카드 메뉴");
                    SetPanelText(Panels[2], "몬스터 메뉴");
                    SetPanelText(Panels[3], "아이템 메뉴");
                    SetPanelText(Panels[4], "방 로드");
                    foreach (GameObject panel in Panels)
                    {
                        panel.SetActive(true);
                        panel.transform.Find("Button1").gameObject.SetActive(false);
                    }
                    for (int i = 5; i < 8; i++)
                    {
                        Panels[i].SetActive(false);
                    }
                    break;
                case DebugMode.Stat:
                    SetPanelText(Panels[0], "뒤로");
                    SetPanelText(Panels[1], "체력");
                    SetPanelText(Panels[2], "행동력");
                    SetPanelText(Panels[3], "핸드 크기");
                    Panels[0].SetActive(true);
                    for (int i = 1; i < 4; i++)
                    {
                        Panels[i].SetActive(true);
                        Panels[i].transform.Find("Button1").gameObject.SetActive(true);
                    }

                    break;
                case DebugMode.Card:
                    ReadOnlyCollection<Card> PlayerCard = GameManager.Player.GetCards();
                    SetPanelText(Panels[0], "뒤로");
                    SetPanelText(Panels[1], "이전");
                    SetPanelText(Panels[7], "다음");
                    foreach (GameObject panel in Panels)
                    {
                        panel.SetActive(true);
                    }
                    if (cardIndex == 0)
                    {
                        Panels[1].SetActive(false);
                    }
                    if (cardIndex + displayingCount >= allCards.Count)
                    {
                        Panels[7].SetActive(false);
                    }
                    for (int i = allCards.Count - cardIndex; i < 7; i++)
                    {
                        Panels[i].SetActive(false);
                    }
                    for (int i = 2; i < 7 && cardIndex + i < allCards.Count; i++)
                    {
                        SetPanelText(Panels[i], allCards[cardIndex + i - 2].Name);
                        Panels[i].transform.Find("Button1").gameObject.SetActive(false);
                        foreach (Card c in PlayerCard)
                        {
                            if (c.Name == allCards[cardIndex + i - 2].Name)
                            {
                                Panels[i].transform.Find("Button1").gameObject.SetActive(true);
                                break;
                            }
                        }
                    }
                    break;
                case DebugMode.Unit:
                    SetPanelText(Panels[0], "뒤로");
                    SetPanelText(Panels[1], "이전");
                    SetPanelText(Panels[7], "다음");
                    foreach (GameObject panel in Panels)
                    {
                        panel.SetActive(true);
                    }
                    if (unitIndex == 0)
                    {
                        Panels[1].SetActive(false);
                    }
                    if (unitIndex + displayingCount >= allUnits.Count)
                    {
                        Panels[7].SetActive(false);
                    }
                    for (int i = allUnits.Count - unitIndex; i < 7; i++)
                    {
                        Panels[i].SetActive(false);
                    }
                    for (int i = 2; i < 7 && unitIndex + i < allUnits.Count; i++)
                    {
                        SetPanelText(Panels[i], allUnits[unitIndex + i - 2].Name);
                    }
                    break;
                case DebugMode.Item:
                    SetPanelText(Panels[0], "뒤로");
                    SetPanelText(Panels[1], "이전");
                    SetPanelText(Panels[7], "다음");
                    Panels[0].SetActive(true);
                    break;
                case DebugMode.LoadRoom:
                    SetPanelText(Panels[0], "뒤로");
                    SetPanelText(Panels[1], "이전");
                    SetPanelText(Panels[7], "다음");
                    foreach (GameObject panel in Panels)
                    {
                        panel.SetActive(true);
                    }
                    if (roomIndex == 0)
                    {
                        Panels[1].SetActive(false);
                    }
                    if (roomIndex + displayingCount >= allRooms.Count)
                    {
                        Panels[7].SetActive(false);
                    }
                    for (int i = Mathf.Max(allRooms.Count - roomIndex, 1); i < 7; i++)
                    {
                        Panels[i].SetActive(false);
                    }
                    for (int i = 2; i < 7 && roomIndex + i < allRooms.Count; i++)
                    {
                        SetPanelText(Panels[i], allRooms[roomIndex + i - 2].BaseRoomData.FileName);
                    }
                    break;

            }
        }
    }
}
