﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Wielder.Visual
{
    class TermScript : Indicator<TermInfo>
    {
        [SerializeField]
        private TextMeshProUGUI nameText;

        [SerializeField]
        private TextMeshProUGUI descriptionText;

        [SerializeField]
        private Image Sprite;
        
        public override void RefreshDisplay()
        {
            nameText.text = LinkedComponent.TermName;
            descriptionText.text = LinkedComponent.TermDescription;
            Sprite.sprite = LinkedComponent.Sprite;
            if (Sprite.sprite == null)
                Sprite.color = new Color(255, 255, 255, 0);
            else
                Sprite.color = new Color(255, 255, 255, 255);
        }
    }
}
