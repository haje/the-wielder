﻿using System.Collections;
using UnityEngine;
using Wielder.Cards;

namespace Wielder.Visual
{
    public static class CardVisualEffectContainer
    {
        public static CardVisualEffect GetVisualEffectFromName(string functionName)
        {
            return DelegateHelper.GetDelegateFromName<CardVisualEffect>(typeof(CardVisualEffectContainer), functionName);
        }

        private static IEnumerator DestroyAfter(GameObject effect, float delay)
        {
            yield return new WaitForSeconds(delay);
            GameObject.Destroy(effect);
        }

        static IEnumerator NoVisualEffect(CardEffectParameter param)
        {
            if (param.Caster.Indicator == null)
            {
                yield return new WaitForSeconds(1);
                yield break;
            }

            GameObject effect = GameObject.Instantiate(VisualResourceManagerScript.Instance.TestParticleEffect);

            yield return CoroutineExecuterScript.Instance.StartCoroutine(GeneralVisualEffectContainer.MoveLinearly(
                effect, 0.3f, param.Caster.PlacedCell.Indicator.transform.position, (param.TargetCell ?? param.Caster.PlacedCell).Indicator.transform.position));

            GameObject.Destroy(effect, 1);
        }

        static IEnumerator Slash(CardEffectParameter param)
        {
            GameObject effect = GameObject.Instantiate(FileResourceManager.GetVisualEffect("Slash"));
            effect.GetComponent<TrailRenderer>().sortingLayerName = "VisualEffects";

            yield return CoroutineExecuterScript.Instance.StartCoroutine(GeneralVisualEffectContainer.MoveLinearly(
               effect, 0.6f, param.TargetCell.Indicator.transform.position + new Vector3(-0.7f, 0.7f), param.TargetCell.Indicator.transform.position + new Vector3(0.7f, -0.7f)));
        }

        static IEnumerator ShootRedBall(CardEffectParameter param)
        {
            GameObject redBall = GameObject.Instantiate(FileResourceManager.GetVisualEffect("Red Ball"));
            yield return CoroutineExecuterScript.Instance.StartCoroutine(GeneralVisualEffectContainer.MoveLinearly(
                redBall, 0.4f, (param.Caster.PlacedCell ?? param.TargetCell).Indicator.transform.position, param.TargetCell.Indicator.transform.position));
            yield return new WaitForSeconds(0.05f);
            GameObject.Destroy(redBall);

            GameObject hitEffect = GameObject.Instantiate(FileResourceManager.GetVisualEffect("Hit Particle Effect"), param.TargetCell.Indicator.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.5f);

            CoroutineExecuterScript.Instance.StartCoroutine(DestroyAfter(hitEffect, 2f));
        }

        static IEnumerator MoveLinearly(CardEffectParameter param)
        {
            if (param.Caster.Indicator == null)
            {
                yield return new WaitForSeconds(1);
                yield break;
            }

            yield return CoroutineExecuterScript.Instance.StartCoroutine(GeneralVisualEffectContainer.MoveLinearly(
                param.Caster.Indicator.gameObject, 0.1f, param.Caster.Indicator.transform.position, param.TargetCell.Indicator.transform.position));
        }
    }
}
