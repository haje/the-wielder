﻿using UnityEngine;
using Wielder.Maps;

namespace Wielder
{
    public class RoomMoveButtonScript : MonoBehaviour
    {
        [HideInInspector]
        public Direction Dir;

        [SerializeField]
        private Color32 activeColor;

        [SerializeField]
        private Color32 pingPongColor;

        [SerializeField]
        private Color32 inactiveColor;

        private SpriteRenderer spriteRenderer;

        public bool Active;

        public bool Pingpong;

        private void Awake()
        {
            if (spriteRenderer == null)
                spriteRenderer = GetComponent<SpriteRenderer>();             
        }

        private void Update()
        {
            if (Pingpong)
                spriteRenderer.color = Color.Lerp(activeColor, pingPongColor, Mathf.PingPong(Time.time, 0.7f));
            else if (Active)
                spriteRenderer.color = activeColor;
            else
                spriteRenderer.color = inactiveColor;

        }

        private void OnMouseOver()
        {
            if (Active)
                UIManagerScript.Instance.RoomMoveButtonMouseOver(this);
        }

        private void OnMouseExit()
        {
            UIManagerScript.Instance.RoomMoveButtonMouseExit(this);

            Pingpong = false;
        }

        private void OnMouseUpAsButton()
        {
            if (Active)
                UIManagerScript.Instance.RoomMoveButtonClicked(Dir);
        }
    }
}