﻿using System.Collections;
using UnityEngine;
using Wielder.Units;

namespace Wielder.Visual
{
    public static class GeneralVisualEffectContainer
    {
        public static IEnumerator NoVisualEffect()
        {
            yield return new WaitForSeconds(0.1f);
        }

        public static IEnumerator MoveLinearly(GameObject effect, float duration, Vector3 startingPos, Vector3 endPos)
        {
            effect.transform.position = startingPos;
            float elapsedTime = 0;
            while (elapsedTime < duration)
            {
                yield return new WaitForEndOfFrame();
                effect.transform.position = Vector3.Lerp(startingPos, endPos, elapsedTime / duration);
                elapsedTime += Time.deltaTime;
            }
            effect.transform.position = endPos;
        }

        private static IEnumerator TempFlashEffect(Vector2 position)
        {
            /*
            GameObject flash = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            flash.transform.position = new Vector3(position.x, position.y, -1);

            yield return new WaitForSeconds(0.5f);
            GameObject.Destroy(flash);
            */
            yield return CoroutineExecuterScript.Instance.StartCoroutine(CreateEffectAndWait("Trigger Flash", position, 0.5f, 0));
        }

        public static VisualEffect GenerateTempFlashEffect(Passive.IPassiveOwner passiveOwner)
        {
            VisualEffect vEffect;
            if (passiveOwner is UnitModifier)
            {
                Unit p = (passiveOwner as UnitModifier).Owner;
                vEffect = () => TempFlashEffect(p.PlacedCell.Indicator.transform.position);
            }
            else
                vEffect = NoVisualEffect;

            return vEffect;
        }

        private static IEnumerator DestroyAfter(GameObject effect, float delay)
        {
            yield return new WaitForSeconds(delay);
            GameObject.Destroy(effect);
        }

        private static IEnumerator CreateEffectAndWait(string effectName, Vector3 position, float duration, float destructionDelay)
        {
            GameObject effect = GameObject.Instantiate(FileResourceManager.GetVisualEffect(effectName));
            effect.transform.position = position;

            yield return new WaitForSeconds(duration);
            CoroutineExecuterScript.Instance.StartCoroutine(DestroyAfter(effect, destructionDelay));
        }

        public static IEnumerator GenerateFire(Vector3 position)
        {
            yield return CoroutineExecuterScript.Instance.StartCoroutine(CreateEffectAndWait("Floor Fire Effect", position, 0, 2f));
        }
    }
}
