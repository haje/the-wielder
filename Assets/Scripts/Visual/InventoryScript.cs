﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Wielder.Cards;
using Wielder.Item;
using Wielder.Units;

namespace Wielder.Visual
{
    public class InventoryScript : Indicator<Unit>
    {
        enum InventoryMode
        {
            Inventory,
            Deck,
            FunFunCardPackExtraction,
        }

        private static InventoryScript instance;

        public static InventoryScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        public override Unit LinkedComponent
        {
            get
            {
                //일단 임시 코드:남의 인벤토리를 볼 일이 있을까?
                return GameManager.Player;
            }

        }

        public bool CurrentlyShown { get; private set; }
        public GameObject ItemDisplayArea;
        private List<ItemScript> displayingItems;
        private InventoryMode currentMode;
        private CardPack openingCardPack;
        private ItemScript selectedItem;
        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
            gameObject.SetActive(CurrentlyShown = false);
            displayingItems = new List<ItemScript>();
        }

        public void InteractItem(ItemScript itemScript)
        {
            switch (currentMode)
            {
                case InventoryMode.Inventory:
                    if (itemScript.LinkedComponent is CardPack)
                    {
                        OpenCardPack((CardPack)itemScript.LinkedComponent);
                        ItemInfoManager.Instance.StopDisplay();
                    }
                    break;
                case InventoryMode.FunFunCardPackExtraction:
                    Card c = (Card)itemScript.LinkedComponent;
                    foreach (ItemScript item in displayingItems)
                    {
                        if (item == itemScript)
                            continue;
                        item.LinkedComponent.Owner = null;
                    }
                    c.Owner = GameManager.Player;
                    GameManager.Player.AddCardToDeck(c);
                    GameManager.Player.DiscardItem(openingCardPack);
                    openingCardPack = null;
                    CardInfoManager.Instance.StopDisplay();
                    SetInventoryMode();
                    break;
            }
        }

        public void Show()
        {
            Debug.Assert(!CurrentlyShown);
            CurrentlyShown = true;
            gameObject.SetActive(true);
            currentMode = InventoryMode.Inventory;
            RefreshDisplay();
        }

        public void Hide()
        {
            Debug.Assert(CurrentlyShown);
            CurrentlyShown = false;
            ClearScreen();
            gameObject.SetActive(false);
        }

        public void SetInventoryMode()
        {
            if (currentMode == InventoryMode.Inventory) return;
            openingCardPack = null;
            currentMode = InventoryMode.Inventory;
            RefreshDisplay();
        }

        public void SetDeckMode()
        {
            if (currentMode == InventoryMode.Deck) return;
            openingCardPack = null;
            currentMode = InventoryMode.Deck;
            RefreshDisplay();
        }

        private void OpenCardPack(CardPack pack)
        {
            if (currentMode == InventoryMode.FunFunCardPackExtraction) return;
            openingCardPack = pack;
            currentMode = InventoryMode.FunFunCardPackExtraction;
            RefreshDisplay();
        }

        public override void RefreshDisplay()
        {
            if (!CurrentlyShown) return;
            ClearScreen();
            switch (currentMode)
            {
                case InventoryMode.Inventory:
                    foreach (IItem item in LinkedComponent.Inventory)
                    {
                        CheckItemScript(item);
                    }
                    break;
                case InventoryMode.Deck:
                    foreach (IItem item in LinkedComponent.GetCards())
                    {
                        CheckItemScript(item);
                    }
                    break;
                case InventoryMode.FunFunCardPackExtraction:
                    foreach (IItem item in openingCardPack.Cards)
                    {
                        item.Owner = GameManager.Player;
                        
                        ItemScript script = VisualResourceManagerScript.Instance.FetchItemIndicator(ItemDisplayArea.transform).GetComponent<ItemScript>();
                        script.DuplicateItemMode = false;
                        script.LinkedComponent = item;
                        displayingItems.Add(script);
                        
                    }
                    break;
            }
            int count = 0;
            foreach(ItemScript script in displayingItems)
            {
                script.transform.localPosition += new Vector3(count % 6, (count / 6) * -2f - 0.5f);
                count++;
            }
        }

        private void ClearScreen()
        {
            foreach (ItemScript script in displayingItems)
            {
                VisualResourceManagerScript.Instance.ReturnItemIndicatorToPool(script.gameObject);
            }
            displayingItems.Clear();
        }

        private void CheckItemScript(IItem item)
        {
            bool exists = false;
            foreach (ItemScript i in displayingItems)
            {
                if (item.Name == i.LinkedComponent.Name && item.GetType() == i.LinkedComponent.GetType())
                {
                    i.LinkedComponent = item;
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                ItemScript script = VisualResourceManagerScript.Instance.FetchItemIndicator(ItemDisplayArea.transform).GetComponent<ItemScript>();
                script.LinkedComponent = item;
                displayingItems.Add(script);
            }
        }

    }
}
