﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Wielder.Cards;
using Wielder.Item;
using Wielder.NonUnits;

namespace Wielder.Visual
{
    class ChestInteractionScript : Indicator<Chest>
    {
        enum ChestMode
        {
            Chest,
            FunFunCardPackExtraction,
        }

        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static ChestInteractionScript instance;

        public static ChestInteractionScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        public bool CurrentlyShown { get; private set; }

        public GameObject CardDisplayArea;
        private ChestMode currentMode;
        private CardPack openingCardPack;
        private List<ItemScript> displayingItems;
        private ItemScript selectedItem;

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
            gameObject.SetActive(CurrentlyShown = false);
            displayingItems = new List<ItemScript>();
        }

        public void Show()
        {
            Debug.Assert(!CurrentlyShown);
            CurrentlyShown = true;
            gameObject.SetActive(true);
            currentMode = ChestMode.Chest;
            RefreshDisplay();
        }

        public void Hide()
        {
            Debug.Assert(CurrentlyShown);
            CurrentlyShown = false;
            gameObject.SetActive(false);
            foreach (ItemScript cs in displayingItems)
            {
                VisualResourceManagerScript.Instance.ReturnItemIndicatorToPool(cs.gameObject);
            }
            displayingItems.Clear();
        }

        private void OpenCardPack(CardPack pack)
        {
            if (currentMode == ChestMode.FunFunCardPackExtraction) return;
            openingCardPack = pack;
            currentMode = ChestMode.FunFunCardPackExtraction;

            foreach (Card card in openingCardPack.Cards)
            {
                GameManager.Player.AddCardToDeck(card);
            }

            RefreshDisplay();
        }

        public override void RefreshDisplay()
        {
            if (!CurrentlyShown) return;
            ClearScreen();
            switch (currentMode)
            {
                case ChestMode.Chest:
                    foreach (IItem item in LinkedComponent.DroppingItems)
                    {
                        ItemScript script = VisualResourceManagerScript.Instance.FetchItemIndicator(CardDisplayArea.transform).GetComponent<ItemScript>();
                        script.DuplicateItemMode = false;
                        script.LinkedComponent = item;
                        displayingItems.Add(script);
                    }
                    break;
                case ChestMode.FunFunCardPackExtraction:
                    foreach (IItem item in openingCardPack.Cards)
                    {
                        item.Owner = GameManager.Player;

                        ItemScript script = VisualResourceManagerScript.Instance.FetchItemIndicator(CardDisplayArea.transform).GetComponent<ItemScript>();
                        script.DuplicateItemMode = false;
                        script.LinkedComponent = item;
                        displayingItems.Add(script);

                    }
                    break;
            }
            int count = 0;
            foreach (ItemScript script in displayingItems)
            {
                script.transform.localPosition += new Vector3(count % 6, (count / 6) * -2f - 0.5f);
                count++;
            }
        }

        private void ClearScreen()
        {
            foreach (ItemScript script in displayingItems)
            {
                VisualResourceManagerScript.Instance.ReturnItemIndicatorToPool(script.gameObject);
            }
            displayingItems.Clear();
        }

        public void InteractItem(ItemScript itemScript)
        {
            switch (currentMode)
            {
                case ChestMode.Chest:
                    if (itemScript.LinkedComponent is CardPack)
                    {
                        OpenCardPack((CardPack)itemScript.LinkedComponent);
                        ItemInfoManager.Instance.StopDisplay();
                    }
                    break;
            }
        }

        public void GetItem(ItemScript itemScript)
        {
            bool success = false;

            success = LinkedComponent.GetItem(itemScript.LinkedComponent);

            if (success)
            {
                displayingItems.Remove(itemScript);
                VisualResourceManagerScript.Instance.ReturnItemIndicatorToPool(itemScript.gameObject);
            }

            PopupUIScript.Instance.HideChest();
        }
    }
}
