﻿using UnityEngine;

namespace Wielder.Visual
{
    public abstract class Indicator<T> : MonoBehaviour
    {
        // 나중에 여기 들어갈 값들을 바꿀 예정. 일단 지금은 고정된 값
        protected const float worldConversionScale = 1.0f;
        protected readonly Vector2 worldConversionStart = new Vector2(-3, 3);

        private T linkedComponent;

        public virtual T LinkedComponent
        {
            get
            {
                return linkedComponent;
            }

            set
            {
                linkedComponent = value;

                if (linkedComponent != null)
                    RefreshDisplay();
            }
        }

        public abstract void RefreshDisplay();
    }
}
