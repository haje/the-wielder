﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Wielder.Visual
{
    public class LogScript : Indicator<LogParameter>
    {
        [SerializeField]
        private GameObject display;

        [SerializeField]
        private TextMeshProUGUI mainText;

        [SerializeField]
        private TextMeshProUGUI valueText;

        [SerializeField]
        private SpriteRenderer hexagon;

        [SerializeField]
        private Image mainSprite; // 육각형 안에 들어가는 sprite

        [SerializeField]
        private Image actorSprite;

        [SerializeField]
        private Image middleSprite; // 체력 같은 아이콘 또는 카드

        [SerializeField]
        private Image targetSprite;

        private const int ga = 0;
        private const int notga = 1;

        public void SetPosition(float y)
        {
            transform.localPosition = new Vector2(0, y);
            y = Mathf.Max(-1.5f, Mathf.Min(-0.5f, display.transform.localPosition.y) + transform.position.y);
            display.transform.position = new Vector2(display.transform.position.x, y);
        }

        private void OnMouseOver()
        {
            display.SetActive(true);
            if (LinkedComponent.TargetCard != null)
                CardInfoManager.Instance.Display(LinkedComponent.TargetCard, new Vector2(transform.position.x + 4.5f, transform.position.y));
        }

        private void OnMouseExit()
        {
            display.SetActive(false);
            CardInfoManager.Instance.StopDisplay();
        }

        public override void RefreshDisplay()
        {
            if (LinkedComponent.Type == LogType.Die)
                hexagon.color = Color.red;
            else
                hexagon.color = Color.white;

            SetMainText();
            SetValueText();
            SetSprite();
        }

        private void SetMainText()
        {
            string logText = "";

            string actorName = LinkedComponent.Actor == null ? null : LinkedComponent.Actor.Name;

            switch (LinkedComponent.Type)
            {
                case LogType.Deal:
                    logText += string.Format("{0} 피해를 {1} 입었습니다.", AttachSpecialCase(actorName, ga), LinkedComponent.Value);
                    break;
                case LogType.Heal:
                    logText += string.Format("{0} 체력을 {1} 회복합니다.", AttachSpecialCase(actorName, ga), LinkedComponent.Value);
                    break;
                case LogType.Die:
                    logText += string.Format("{0} 죽었습니다.", AttachSpecialCase(actorName, ga));
                    break;
                case LogType.Cast:
                    if (LinkedComponent.TargetUnit != null)
                        logText += string.Format("{0} {1}에게 {2} 사용합니다.", AttachSpecialCase(actorName, ga), LinkedComponent.TargetUnit.Name, AttachSpecialCase(LinkedComponent.TargetCard.Name, notga));
                    else
                        logText += string.Format("{0} {1} 사용합니다", AttachSpecialCase(actorName, ga), AttachSpecialCase(LinkedComponent.TargetCard.Name, notga));
                    break;
                case LogType.Absorb:
                    logText += string.Format("{0} {1}데미지를 흡수합니다.", AttachSpecialCase(actorName, ga), LinkedComponent.Value);
                    break;
                case LogType.EndRoom:
                    logText += "방을 클리어했습니다.";
                    break;
                default:
                    throw new NotImplementedException();
            }

            mainText.text = " " + logText;
        }

        private void SetValueText()
        {
            switch(LinkedComponent.Type)
            {
                case LogType.Deal:
                case LogType.Absorb:
                    valueText.text = "-" + LinkedComponent.Value.ToString();
                    break;
                case LogType.Heal:
                    valueText.text = "+" + LinkedComponent.Value.ToString();
                    break;
                default:
                    valueText.text = "";
                    break;
            }
        }

        private void SetSprite()
        {
            middleSprite.color = targetSprite.color = new Color32(255, 255, 255, 0); // 안 보이게 됨
            mainSprite.sprite = actorSprite.sprite = LinkedComponent.Actor.BaseUnitData.UnitImage;

            switch(LinkedComponent.Type)
            {
                case LogType.Deal:
                case LogType.Heal:
                    middleSprite.sprite = FileResourceManager.GetSprite("Heart");
                    middleSprite.color = Color.white;
                    break;
                case LogType.Die:
                    middleSprite.sprite = FileResourceManager.GetSprite("Skull");
                    middleSprite.color = Color.white;
                    break;
                case LogType.Absorb:
                    middleSprite.sprite = FileResourceManager.GetSprite("Shield");
                    middleSprite.color = Color.white;
                    break;
                case LogType.Cast:
                    middleSprite.sprite = LinkedComponent.TargetCard.BaseCardData.CardImage;
                    middleSprite.color = Color.white;
                    if (LinkedComponent.TargetUnit != null)
                    {
                        targetSprite.sprite = LinkedComponent.TargetUnit.BaseUnitData.UnitImage;
                        targetSprite.color = Color.white;
                    }
                    break;
                case LogType.EndRoom:
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private static string AttachSpecialCase(string context, int value)
        {
            return context + GetSpecialCase(context, value);
        }

        private static string GetSpecialCase(string context, int value)
        {

            int lastIndex = context[context.Length - 1];
            if (lastIndex >= 0xAC00 && lastIndex <= 0xD7A3)
            {
                switch (value)
                {
                    case notga:
                        return (lastIndex - 0xAC00) % 28 == 0 ? "를" : "을";
                    case ga:
                        return (lastIndex - 0xAC00) % 28 == 0 ? "가" : "이";
                }

            }

            return value == notga ? "을/를" : "이/가";
        }
    }
}
