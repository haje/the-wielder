﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Wielder.Units;
using Wielder.Cards;

namespace Wielder.Visual
{
    public class PlayerInfoManager : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static PlayerInfoManager instance;

        public static PlayerInfoManager Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        public List<CardScript> DisplayingCards
        {
            get
            {
                return displayingCards;
            }
        }

        private List<CardScript> displayingCards;

        [SerializeField]
        private TextMeshProUGUI hpText;

        [SerializeField]
        private Image hpBar;

        [SerializeField]
        private TextMeshProUGUI apText;

        [SerializeField]
        private Image apBar;

        [SerializeField]
        private TextMeshProUGUI moneyText;

        [SerializeField]
        private TextMeshProUGUI deckText;

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;

            displayingCards = new List<CardScript>();
        }

        public void UpdatePlayerInfo()
        {
            Unit player = GameManager.Player;

            hpText.text = player.CurrentRemainingHP.ToString() + " / " + player.CurrentMaxHP.ToString();
            apText.text = player.CurrentAP.ToString() + " / " + player.APPerTurn.ToString();
            moneyText.text = player.CurrentMoney.ToString();
            deckText.text = player.Deck.Count.ToString() + " / " + player.GetCards().Count.ToString();

            hpBar.fillAmount = Mathf.Min(1, (float)player.CurrentRemainingHP / player.CurrentMaxHP);
            apBar.fillAmount = Mathf.Min(1, (float)player.CurrentAP / player.APPerTurn);

            foreach (CardScript cardScript in displayingCards)
                cardScript.RefreshDisplay();
        }

        public void DisplayPlayerHand()
        {
            if (GameManager.CurrentRoom == null // 게임이 처음 시작했을 때
                || GameManager.CurrentRoom.IsCombatFinished) // 또는 핸드를 표시할 필요가 없을 때
                return;

            IList<Card> hand = GameManager.Player.Hand;

            FreeDisplayingCard();

            for (int i = 0; i < hand.Count; i++)
            {
                CardScript cardScript;
                cardScript = VisualResourceManagerScript.Instance.FetchCardIndicator().GetComponent<CardScript>();
                cardScript.transform.localPosition = new Vector3(i, 0, 0);
                displayingCards.Add(cardScript);

                cardScript.LinkedComponent = hand[i];
            }
        }

        /// <summary>
        /// 표시하고 있는 카드를 모두 제거
        /// </summary>
        public void FreeDisplayingCard()
        {
            foreach (CardScript cardScript in displayingCards)
                VisualResourceManagerScript.Instance.ReturnCardIndicatorToPool(cardScript.gameObject);

            displayingCards = new List<CardScript>();
        }
    }
}
