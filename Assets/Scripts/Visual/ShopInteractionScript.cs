﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Wielder.Cards;
using Wielder.Item;
using Wielder.NonUnits;

namespace Wielder.Visual
{
    class ShopInteractionScript : Indicator<Shopkeeper>
    {
        public enum ShopMode
        {
            Shop,
            TrashCan,
        }

        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static ShopInteractionScript instance;

        public static ShopInteractionScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        public bool CurrentlyShown { get; private set; }

        public GameObject ItemDisplayArea;
        private ShopMode currentMode;
        private List<ItemScript> displayingItems;
        private ItemScript selectedItem;

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
            gameObject.SetActive(CurrentlyShown = false);
            displayingItems = new List<ItemScript>();
        }

        public void Show()
        {
            Debug.Assert(!CurrentlyShown);
            CurrentlyShown = true;
            gameObject.SetActive(true);
            currentMode = ShopMode.Shop;
            RefreshDisplay();
        }

        public void Hide()
        {
            Debug.Assert(CurrentlyShown);
            CurrentlyShown = false;
            gameObject.SetActive(false);
            foreach (ItemScript cs in displayingItems)
            {
                VisualResourceManagerScript.Instance.ReturnItemIndicatorToPool(cs.gameObject);
            }
            displayingItems.Clear();
        }

        public void SetShopMode()
        {
            if (currentMode != ShopMode.Shop)
            {
                currentMode = ShopMode.Shop;
                RefreshDisplay();
            }
        }

        public void SetTrashMode()
        {
            if (currentMode != ShopMode.TrashCan)
            {
                currentMode = ShopMode.TrashCan;
                RefreshDisplay();
            }
        }

        public override void RefreshDisplay()
        {

            if (!CurrentlyShown) return;

            foreach (ItemScript cs in displayingItems)
            {
                VisualResourceManagerScript.Instance.ReturnItemIndicatorToPool(cs.gameObject);
            }
            displayingItems.Clear();

            switch (currentMode)
            {
                case ShopMode.Shop:
                    foreach (IItem item in LinkedComponent.SellingItems)
                    {
                        CheckItemScript(item);
                    }
                    break;
                case ShopMode.TrashCan:
                    foreach (IItem item in GameManager.Player.GetCards())
                    {
                        CheckItemScript(item);
                    }
                    foreach (ItemScript script in displayingItems)
                    {
                        script.SetPrice(LinkedComponent.DiscardPrice);
                    }
                    break;
            }
            int count = 0;
            foreach (ItemScript script in displayingItems)
            {
                script.transform.localPosition += new Vector3(count % 6, (count / 6) * -2f - 0.5f);
                count++;
            }
        }

        public void InteractItem(ItemScript itemScript)
        {
            if (selectedItem != null)
            {
                if (selectedItem == itemScript)   // 같은 아이템을 다시 클릭했을 때 구매
                {
                    itemScript.Unselect();
                    BuyItem(selectedItem);
                    selectedItem = null;
                    CardInfoManager.Instance.StopDisplay();
                    ItemInfoManager.Instance.StopDisplay();
                    return;
                }
                selectedItem.Unselect();
            }

            selectedItem = itemScript;
            itemScript.Select();
        }

        public void BuyItem(ItemScript itemScript)
        {
            bool success = false;
            switch (currentMode)
            {
                case ShopMode.Shop:
                    success = LinkedComponent.BuyItem(itemScript.LinkedComponent);
                    break;
                case ShopMode.TrashCan:
                    success = LinkedComponent.DiscardItem(itemScript.LinkedComponent);
                    break;
            }
            if (success)
            {
                itemScript.LinkedComponent = null;
                RefreshDisplay();
                /*
                if (itemScript.LinkedComponent == null) // 이해 안되는 코드일 수 있는데, itemScript에 한해서 중복 아이템 존재시 null이 아닐 수도 있음
                {
                    displayingItems.Remove(itemScript);
                    VisualResourceManagerScript.Instance.ReturnItemIndicatorToPool(itemScript.gameObject);
                }
                
                //후처리
                switch (currentMode)
                {
                    case ShopMode.Shop:
                        break;
                    case ShopMode.TrashCan:
                        foreach (ItemScript script in displayingItems)
                        {
                            script.SetPrice(LinkedComponent.DiscardPrice);
                        }
                        break;
                }
                */
            }
        }

        private void CheckItemScript(IItem item)
        {
            bool exists = false;
            foreach (ItemScript i in displayingItems)
            {
                if (item.Name == i.LinkedComponent.Name && item.GetType() == i.LinkedComponent.GetType())
                {
                    i.LinkedComponent = item;
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                ItemScript script = VisualResourceManagerScript.Instance.FetchItemIndicator(ItemDisplayArea.transform).GetComponent<ItemScript>();
                script.LinkedComponent = item;
                displayingItems.Add(script);
            }
        }
    }
}
