﻿using UnityEngine;
using Wielder.NonUnits;

namespace Wielder.Visual
{
    public class NonUnitScript : Indicator<NonUnit>
    {
        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            transform.localScale *= worldConversionScale;
        }

        public override NonUnit LinkedComponent
        {
            get
            {
                return base.LinkedComponent;
            }

            set
            {
                if (value != null)
                {
                    value.Indicator = this;
                }
                base.LinkedComponent = value;
            }
        }

        public override void RefreshDisplay()
        {
            spriteRenderer.sprite = LinkedComponent.NonUnitImage;
            transform.position = LinkedComponent.PlacedCell.Indicator.transform.position
                + new Vector3(0, LinkedComponent.YCalibration * worldConversionScale, 0);
        }
    }
}
