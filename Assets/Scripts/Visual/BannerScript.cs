﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Wielder.Visual
{
    class BannerScript : MonoBehaviour
    {
        private static BannerScript instance;

        public static BannerScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        public bool CurrentlyShown { get; private set; }

        [SerializeField]
        private TextMeshProUGUI bannerText;

        [SerializeField]
        private Image bannerImage;

        public void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
            gameObject.SetActive(CurrentlyShown = false);
        }

        public enum MessageType
        {
            GameOver,
            YouWin
        }

        private MessageType message = MessageType.GameOver;
        public MessageType Message
        {
            get { return message; }
            set
            {
                message = value;
                switch (message)
                {
                    case MessageType.GameOver:
                        bannerText.text = "GAME OVER";
                        bannerImage.color = Color.red;
                        break;
                    case MessageType.YouWin:
                        bannerText.text = "YOU WIN";
                        bannerImage.color = Color.green;
                        break;
                }
                bannerImage.color = new Color(bannerImage.color.r, bannerImage.color.g, bannerImage.color.b, 0.5f);
            }
        }

        public void Show()
        {
            gameObject.SetActive(true);
            CurrentlyShown = true;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            CurrentlyShown = false;
        }
    }
}
