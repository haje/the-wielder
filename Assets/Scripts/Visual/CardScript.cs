﻿using UnityEngine;
using TMPro;
using Wielder.Cards;

namespace Wielder.Visual
{
    public class CardScript : Indicator<Card>
    {
        private SpriteRenderer spriteRenderer;

        [SerializeField]
        private TextMeshProUGUI apText;

        [SerializeField]
        private GameObject manaIcon;

        [SerializeField]
        private GameObject selectedIndicator;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public override Card LinkedComponent
        {
            get
            {
                return base.LinkedComponent;
            }

            set
            {
                if (value != null)
                    base.LinkedComponent = value;
            }
        }

        public override void RefreshDisplay()
        {
            spriteRenderer.sprite = LinkedComponent.BaseCardData.CardImage;
            apText.text = LinkedComponent.Cost.ToString();
        }

        private void OnMouseOver()
        {
            UIManagerScript.Instance.CardMouseOvered(this);
            CardInfoManager.Instance.Display(LinkedComponent, gameObject.transform.position);
        }

        private void OnMouseExit()
        {
            UIManagerScript.Instance.CardMouseExited(this);
            CardInfoManager.Instance.StopDisplay();
        }

        private void OnMouseUpAsButton()
        {
            UIManagerScript.Instance.CardClicked(this);
        }

        public void Select()
        {
            manaIcon.SetActive(false);
            selectedIndicator.SetActive(true);
        }

        public void Unselect()
        {
            manaIcon.SetActive(true);
            selectedIndicator.SetActive(false);
        }
    }
}
