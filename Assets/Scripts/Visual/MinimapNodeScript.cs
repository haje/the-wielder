﻿using System;
using UnityEngine;
using Wielder.Maps;

namespace Wielder.Visual
{
    public class MinimapNodeScript : Indicator<GraphNode>
    {
        public override GraphNode LinkedComponent
        {
            get
            {
                return base.LinkedComponent;
            }

            set
            {
                if (value != null)
                {
                    value.Indicator = this;
                }
                base.LinkedComponent = value;
            }
        }

        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            transform.localScale *= worldConversionScale;
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private new const float worldConversionScale = 0.6f;
        private new readonly Vector2 worldConversionStart = Vector2.zero;

        public override void RefreshDisplay()
        {
            gameObject.SetActive(LinkedComponent.Discovered);
            transform.localPosition = LinkedComponent.Pos.ConvertToWorldPosition(worldConversionScale, worldConversionStart)
                - GameManager.CurrentStage.GetCurrentRoomLocation().ConvertToWorldPosition(worldConversionScale, worldConversionStart);

            if (LinkedComponent.Room.Type == RoomType.BossRoom)
            {
                if (GameManager.CurrentRoom == LinkedComponent.Room)
                    spriteRenderer.color = Color.red;
                else
                    spriteRenderer.color = new Color(1, 0.6f, 0);
                return;
            }

            if (LinkedComponent.Visited)
            {
                if (GameManager.CurrentRoom == LinkedComponent.Room)
                    spriteRenderer.color = Color.yellow;
                else
                    spriteRenderer.color = Color.white;
            }
            else
                spriteRenderer.color = Color.gray;

        }
    }
}
