﻿using System.Collections.Generic;
using UnityEngine;
using Wielder.Maps;

namespace Wielder.Visual
{
    public class MinimapContainerScript : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static MinimapContainerScript instance;

        public static MinimapContainerScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private List<MinimapNodeScript> nodes;

        [SerializeField]
        private GameObject minimapDisplayLocation;

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
            MeshRenderer renderer = minimapDisplayLocation.GetComponent<MeshRenderer>();
            renderer.sortingLayerName = "UI";
        }

        public void Show() // GameManager가 Map을 가지고 게임을 시작할 때 이 함수를 부름
        {
            nodes = new List<MinimapNodeScript>();
            minimapDisplayLocation.SetActive(true);

            foreach (GraphNode gN in GameManager.CurrentStage.Node.ToList())
            {
                MinimapNodeScript mN = VisualResourceManagerScript.Instance.FetchMinimapCellIndicator().GetComponent<MinimapNodeScript>();
                mN.LinkedComponent = gN;
                nodes.Add(mN);
            }
        }

        public void Hide() // 맵 이동할 때 필요
        {
            if (!minimapDisplayLocation.activeSelf) // 첫 월드이면 아무것도 안함
                return;

            foreach (MinimapNodeScript ns in nodes)
            {
                VisualResourceManagerScript.Instance.ReturnMinimapCellIndicatorToPool(ns.gameObject);
            }
            minimapDisplayLocation.SetActive(false);
        }

        public void Refresh() // 변화가 생길 때마다 이 함수가 불림
        {
            if (!minimapDisplayLocation.activeSelf)
                return;

            foreach (MinimapNodeScript mN in nodes)
                mN.RefreshDisplay();
        }
    }
}
