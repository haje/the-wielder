﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Wielder.Units;
using Wielder.Maps;
using Wielder.NonUnits;

namespace Wielder.Visual
{
    public class UnitInfoManager : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static UnitInfoManager instance;

        public static UnitInfoManager Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private Unit displayingUnit;

        private NonUnit displayingNonUnit;

        [SerializeField]
        private GameObject commonCanvas;

        [SerializeField]
        private GameObject unitDisplayer;

        [SerializeField]
        private GameObject nonUnitDisplayer;

        [SerializeField]
        private GameObject shieldDisplayer;

        [SerializeField]
        private Image image;

        [SerializeField]
        private TextMeshProUGUI nameText;

        [SerializeField]
        private TextMeshProUGUI hpText;

        [SerializeField]
        private TextMeshProUGUI apText;

        [SerializeField]
        private TextMeshProUGUI handText;

        [SerializeField]
        private TextMeshProUGUI shieldText;

        [SerializeField]
        private TextMeshProUGUI unitDescriptionText;

        [SerializeField]
        private TextMeshProUGUI nonUnitDescriptionText;

        [SerializeField]
        private GameObject leftArrow;

        [SerializeField]
        private GameObject rightArrow;

        private Cell recentCell;

        private const float xDistance = 3.5f; // 유닛의 카드 풀 정보를 표시할 때 x좌표 거리

        private const float yHeight = -0.06f; // 유닛의 카드 풀 정보를 표시할 때 y 높이 보정

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Q))
                DisplayPrevious();

            if (Input.GetKeyUp(KeyCode.E))
                DisplayNext();
        }

        public void CellMouseOver(Cell cell, Vector2 pos)
        {
            if (recentCell == cell && !(displayingUnit == null && displayingNonUnit == null))
                return;

            if (PopupUIScript.Instance.GetState() != PopupState.None)
                return;

            recentCell = cell;

            if (cell.PlacedUnit != null) // 유닛이 있으면 우선적으로 정보 표시
            {
                Display(cell.PlacedUnit, pos);
                RefreshArrow();
            }
            else if (cell.PlacedNonUnits.Count > 0)  // 논유닛이 있으면 정보 표시
            {
                Display(cell.PlacedNonUnits[0], pos);
                RefreshArrow();
            }
        }

        private void Display(Unit unit, Vector2 pos)
        {
            Debug.Assert(unit != null);
            if (displayingUnit == unit)
                return;

            unitDisplayer.SetActive(true);
            nonUnitDisplayer.SetActive(false);
            displayingUnit = unit;
            displayingNonUnit = null;
            SetPosition(pos);
            RefreshDisplay();
            CardInfoManager.Instance.DisplayUnitCards(unit, new Vector2(transform.position.x + xDistance, transform.position.y + yHeight));
        }

        private void Display(NonUnit nonUnit, Vector2 pos)
        {
            Debug.Assert(nonUnit != null);
            if (displayingNonUnit == nonUnit)
                return;

            nonUnitDisplayer.SetActive(true);
            unitDisplayer.SetActive(false);
            displayingNonUnit = nonUnit;
            displayingUnit = null;
            SetPosition(pos);
            RefreshDisplay();
            CardInfoManager.Instance.StopDisplay();
        }

        public void StopDisplay()
        {
            commonCanvas.SetActive(false);
            unitDisplayer.SetActive(false);
            nonUnitDisplayer.SetActive(false);
            if (displayingUnit != null)
                CardInfoManager.Instance.StopDisplay();
            displayingUnit = null;
            displayingNonUnit = null;
        }

        private void SetPosition(Vector2 pos)
        {
            float x = Mathf.Min(5, pos.x);
            float y = Mathf.Min(-1.5f, Mathf.Max(-5, pos.y));
            transform.position = new Vector3(x, y, 0);
        }

        private void DisplayPrevious()
        {
            if (displayingNonUnit == null)
                return;

            List<NonUnit> nonUnits = displayingNonUnit.PlacedCell.PlacedNonUnits;
            if (nonUnits.IndexOf(displayingNonUnit) != 0) // 첫 NonUnit이 아닐 경우
                Display(nonUnits[nonUnits.IndexOf(displayingNonUnit) - 1], transform.position); // 이전 NonUnit 표시
            else if (displayingNonUnit.PlacedCell.PlacedUnit != null) // 첫 NonUnit이면서 Unit이 있을 경우
            {
                Display(displayingNonUnit.PlacedCell.PlacedUnit, transform.position); // Unit이 있으면 표시
            }
        }

        private void DisplayNext()
        {
            if (displayingUnit != null)
            {
                List<NonUnit> nonUnits = displayingUnit.PlacedCell.PlacedNonUnits;
                if (nonUnits.Count > 0)
                    Display(nonUnits[0], transform.position); // 첫 NonUnit 표시
            }
            else if (displayingNonUnit != null)
            {
                List<NonUnit> nonUnits = displayingNonUnit.PlacedCell.PlacedNonUnits;
                if (nonUnits.IndexOf(displayingNonUnit) + 1 < nonUnits.Count) // 마지막 NonUnit이 아닐 경우
                    Display(nonUnits[nonUnits.IndexOf(displayingNonUnit) + 1], transform.position); // 다음 NonUnit 표시
            }
        }

        public void Removed(Unit unit)
        {
            if (displayingUnit == unit)
                StopDisplay();
        }

        public void Removed(NonUnit nonUnit)
        {
            if (displayingNonUnit == nonUnit)
                StopDisplay();
        }

        public void RefreshDisplay()
        {
            if (displayingUnit == null && displayingNonUnit == null)
                return;

            commonCanvas.SetActive(true);
            RefreshArrow();

            if (displayingUnit != null)
            {
                image.sprite = displayingUnit.BaseUnitData.UnitImage;
                nameText.text = displayingUnit.Name;
                hpText.text = displayingUnit.CurrentRemainingHP.ToString() + " / " + displayingUnit.CurrentMaxHP.ToString();
                apText.text = displayingUnit.CurrentAP.ToString() + " / " + displayingUnit.APPerTurn.ToString();
                handText.text = displayingUnit.Hand.Count.ToString() + " / " + displayingUnit.HandSize.ToString();

                if (displayingUnit.CurrentShield > 0)
                {
                    shieldText.text = displayingUnit.CurrentShield.ToString();
                    shieldDisplayer.SetActive(true);
                }
                else
                    shieldDisplayer.SetActive(false);

                string descText = "";

                foreach (UnitModifier modifier in displayingUnit.Modifiers)
                {
                    descText += modifier.Name + " - " + modifier.Description;
                    if (modifier.RemainingTurn >= 0)
                        descText += " 남은 턴: " + modifier.RemainingTurn;
                    descText += "\n\n";
                }
                unitDescriptionText.text = descText;
            }
            else if (displayingNonUnit != null)
            {
                image.sprite = displayingNonUnit.NonUnitImage;
                nameText.text = displayingNonUnit.Name;
                nonUnitDescriptionText.text = displayingNonUnit.Description;
            }           
        }

        private void RefreshArrow()
        {
            if (displayingUnit != null)
            {
                leftArrow.SetActive(false);
                if (displayingUnit.PlacedCell.PlacedNonUnits.Count > 0)
                    rightArrow.SetActive(true);
                else
                    rightArrow.SetActive(false);
            }
            else if (displayingNonUnit != null)
            {
                List<NonUnit> nonUnits = displayingNonUnit.PlacedCell.PlacedNonUnits;
                int index = nonUnits.IndexOf(displayingNonUnit);

                if (index == 0 && displayingNonUnit.PlacedCell.PlacedUnit == null)
                    leftArrow.SetActive(false);
                else
                    leftArrow.SetActive(true);

                if (index == nonUnits.Count - 1)
                    rightArrow.SetActive(false);
                else
                    rightArrow.SetActive(true);
            }
            else
            {
                leftArrow.SetActive(false);
                rightArrow.SetActive(false);
            }
        }
    }
}
