﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Wielder.Visual
{
    public class VisualResourceManagerScript : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static VisualResourceManagerScript instance;

        public static VisualResourceManagerScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;
        }

        [SerializeField]
        private Transform gameBoardTransform;

        [SerializeField]
        private Transform handCardDisplayTransform;

        [SerializeField]
        private GameObject cellIndicator;

        [SerializeField]
        private GameObject unitIndicator;

        [SerializeField]
        private GameObject nonUnitIndicator;

        [SerializeField]
        private GameObject handCardIndicator;

        [SerializeField]
        private GameObject miniMapCellIndicator;

        [SerializeField]
        private GameObject itemIndicator;

        [SerializeField]
        private GameObject logIndicator;

        [SerializeField]
        private GameObject termIndicator;

        public GameObject TestParticleEffect;

        private List<GameObject> cellIndicatorPool = new List<GameObject>();
        private List<GameObject> unitIndicatorPool = new List<GameObject>();
        private List<GameObject> nonUnitIndicatorPool = new List<GameObject>();
        private List<GameObject> handCardIndicatorPool = new List<GameObject>();
        private List<GameObject> minimapCellIndicatorPool = new List<GameObject>();
        private List<GameObject> itemIndicatorPool = new List<GameObject>();
        private List<GameObject> logIndicatorPool = new List<GameObject>();
        private List<GameObject> termIndicatorPool = new List<GameObject>();

        private GameObject FetchFromPool(List<GameObject> pool, Func<GameObject> instantiationFunction)
        {
            GameObject toReturn;
            if (pool.Count > 0)
            {
                toReturn = pool[0];
                pool.Remove(toReturn);
                toReturn.SetActive(true);
            }
            else
            {
                toReturn = instantiationFunction();
            }
            return toReturn;
        }

        private void PoolOrDestroy(List<GameObject> pool, GameObject gameObject, int poolcapacity)
        {
            gameObject.SetActive(false);
            if (poolcapacity >= pool.Count)
                Destroy(gameObject);
            else
                pool.Add(gameObject);
        }

        public GameObject FetchCellIndicator()
        {
            return FetchFromPool(cellIndicatorPool, () =>
                Instantiate(cellIndicator, gameBoardTransform) as GameObject);
        }

        public GameObject FetchUnitIndicator()
        {
            return FetchFromPool(unitIndicatorPool, () =>
                Instantiate(unitIndicator, gameBoardTransform) as GameObject);
        }

        public GameObject FetchNonUnitIndicator()
        {
            return FetchFromPool(nonUnitIndicatorPool, () =>
                Instantiate(nonUnitIndicator, gameBoardTransform) as GameObject);
        }

        public GameObject FetchCardIndicator()
        {
            return FetchFromPool(handCardIndicatorPool, () =>
                Instantiate(handCardIndicator, handCardDisplayTransform) as GameObject);
        }

        public GameObject FetchMinimapCellIndicator()
        {
            return FetchFromPool(minimapCellIndicatorPool, () =>
                Instantiate(miniMapCellIndicator, MinimapContainerScript.Instance.transform) as GameObject);
        }

        public GameObject FetchItemIndicator(Transform parent)
        {
            return FetchFromPool(itemIndicatorPool, () =>
                Instantiate(itemIndicator, parent) as GameObject);
        }

        public GameObject FetchLogIndicator()
        {
            return FetchFromPool(logIndicatorPool, () =>
            Instantiate(logIndicator, LogManager.Instance.transform) as GameObject);
        }

        public GameObject FetchTermIndicator()
        {
            return FetchFromPool(termIndicatorPool, () =>
            Instantiate(termIndicator, TermInfoManager.Instance.transform) as GameObject);
        }

        public void ReturnCellIndicatorToPool(GameObject indicator)
        {
            Debug.Assert(indicator.GetComponent<CellScript>() != null);
            indicator.GetComponent<CellScript>().LinkedComponent = null;
            PoolOrDestroy(cellIndicatorPool, indicator, 98);
        }

        public void ReturnUnitIndicatorToPool(GameObject indicator)
        {
            Debug.Assert(indicator.GetComponent<UnitScript>() != null);
            indicator.GetComponent<UnitScript>().LinkedComponent = null;
            PoolOrDestroy(unitIndicatorPool, indicator, 7);
        }

        public void ReturnNonUnitIndicatorToPool(GameObject indicator)
        {
            Debug.Assert(indicator.GetComponent<NonUnitScript>() != null);
            indicator.GetComponent<NonUnitScript>().LinkedComponent = null;
            PoolOrDestroy(nonUnitIndicatorPool, indicator, 7);
        }

        public void ReturnCardIndicatorToPool(GameObject indicator)
        {
            Debug.Assert(indicator.GetComponent<CardScript>() != null);
            indicator.GetComponent<CardScript>().LinkedComponent = null;
            PoolOrDestroy(handCardIndicatorPool, indicator, 6);
        }

        public void ReturnMinimapCellIndicatorToPool(GameObject indicator)
        {
            Debug.Assert(indicator.GetComponent<MinimapNodeScript>() != null);
            indicator.GetComponent<MinimapNodeScript>().LinkedComponent = null;
            PoolOrDestroy(minimapCellIndicatorPool, indicator, 20);
        }
        
        public void ReturnItemIndicatorToPool(GameObject indicator)
        {
            Debug.Assert(indicator.GetComponent<ItemScript>() != null);
            indicator.GetComponent<ItemScript>().LinkedComponent = null;
            PoolOrDestroy(itemIndicatorPool, indicator, 20);
        }

        public void ReturnLogIndicatorToPool(GameObject indicator)
        {
            Debug.Assert(indicator.GetComponent<LogScript>() != null);
            indicator.GetComponent<LogScript>().LinkedComponent = null;
            PoolOrDestroy(logIndicatorPool, indicator, 10);
        }

        public void ReturnTermIndicatorToPool(GameObject indicator)
        {
            Debug.Assert(indicator.GetComponent<TermScript>() != null);
            indicator.GetComponent<TermScript>().LinkedComponent = null;
            PoolOrDestroy(termIndicatorPool, indicator, 10);
        }
    }
}
