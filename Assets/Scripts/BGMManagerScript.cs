﻿using UnityEngine;

namespace Wielder
{
    public class BGMManagerScript : MonoBehaviour
    {
        private static BGMManagerScript instance;

        private static AudioSource audioSource;

        public static BGMManagerScript Instance
        {
            get
            {
                if (!instance)
                {
                    GameObject manager = new GameObject("BGM Manager");
                    instance = manager.AddComponent<BGMManagerScript>();
                    audioSource = manager.AddComponent<AudioSource>();
                    audioSource.loop = true;
                    DontDestroyOnLoad(instance.gameObject);
                }
                return instance;
            }
        }

        public const string MainMenuBGMname = "Dark Castle_slow";
        public const string Floor1BGMName = "floor1";
        public const string Floor2BGMName = "floor2";
        public const string Boss1BGMName = "Boss1_Final";
        public const string Boss2BGMName = "Boss2_Final_slow";
        public const string Boss3BGMName = "Boss3";

        private bool fadingOut = false;
        private AudioClip nextClip;

        private void Update()
        {
            float fadespeed = 0.3f;
            if (fadingOut)
            {
                audioSource.volume -= fadespeed * Time.deltaTime;
                if (audioSource.volume < fadespeed)
                {
                    fadingOut = false;
                    Debug.Assert(nextClip != null);
                    audioSource.clip = nextClip;
                    nextClip = null;
                    audioSource.Play();
                }
            }
            else if (audioSource != null && audioSource.volume <= 0.99999f)
            {
                audioSource.volume += fadespeed * Time.deltaTime;
            }
        }

        public void PlayBGM(AudioClip clip)
        {
            if (audioSource.clip == null) audioSource.clip = clip;
            else if (audioSource.clip.name == clip.name) return;
            else
            {
                nextClip = clip;
                fadingOut = true;
            }
            if (!audioSource.isPlaying) audioSource.Play();
        }

        public void PlayBGM(string fileName)
        {
            PlayBGM(FileResourceManager.GetBGM(fileName));
        }

        private void Start()
        {
            Debug.Assert(this == instance);
        }
    }
}