﻿using Wielder.Maps;

namespace Wielder
{
    public interface IPlacable
    {
        Cell PlacedCell { get; }
        void PlaceOnCell(Cell cell);
    }
}
