﻿namespace Wielder
{
    public interface IDamageDealer
    {
        string Name { get; }
    }
}