﻿using System.Collections.Generic;
using UnityEngine;
using Wielder.Maps;

namespace Wielder.NonUnits
{
    class Floor : NonUnit
    {
        public override NonUnitType NonUnitType
        {
            get
            {
                return NonUnitType.Floor;
            }
        }

        public Floor(string name, Sprite floorImage, string description)
        {
            Name = name;
            NonUnitImage = floorImage;
            YCalibration = 0.15f;
            OccupiesCell = false;
            Description = " " + description;
            modifiers = new List<CellModifier>();
        }
    }
}
