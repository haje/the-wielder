﻿using System.Collections.Generic;
using Wielder.Units;
using Wielder.Cards;
using Wielder.Visual;
using Wielder.Item;
using UnityEngine;

namespace Wielder.NonUnits
{
    class Chest : NonUnit
    {
        public override NonUnitType NonUnitType
        {
            get
            {
                return NonUnitType.Chest;
            }
        }

        public List<IItem> DroppingItems { get; private set; }

        public Chest(string grade = "normal")
        {
            switch (grade)
            {
                case "normal":
                    Name = "일반 상자";
                    NonUnitImage = FileResourceManager.GetSprite("Chest_Brown");
                    break;
                case "hero":
                    Name = "영웅 상자";
                    NonUnitImage = FileResourceManager.GetSprite("Chest_Silver");
                    break;
                case "legend":
                    Name = "전설 상자";
                    NonUnitImage = FileResourceManager.GetSprite("Chest_Gold");
                    break;
                default:
                    Debug.LogError("Unknown Chest");
                    break;
            }
            YCalibration = 0.05f;
            OccupiesCell = false;
            DroppingItems = new List<IItem>();
        }

        public override bool Faced(Unit unit)
        {
            if (unit.UnitType != UnitType.Player)
                return false;

            if (PopupUIScript.Instance.GetState() != PopupState.None)
                return false;
            
            ChestInteractionScript.Instance.LinkedComponent = this;
            PopupUIScript.Instance.ShowChest();

            Remove();
            return true;
        }

        public bool GetItem(IItem item)
        {
            if (item is Card)
            {
                GameManager.Player.AddCardToDeck((Card)item);
            }
            else
            {
                GameManager.Player.GetItem(item);
            }
            item.Owner = GameManager.Player;
            DroppingItems.Remove(item);
            return true;
        }

        public static Chest GenerateChest(string grade = "normal")
        {
            Chest ch = new Chest(grade);
            List<CardClass> classPool = new List<CardClass>();
            int[] classWeight;

            CardClass cardClass;
            Rarity rarity;

            switch (grade)
            {
                case "normal":
                    rarity = Rarity.Common;
                    classWeight = new int[] { 1, 2, 2, 1, 1, 0, 1, 0, 0 };
                    break;
                case "hero":
                    rarity = Rarity.Epic;
                    classWeight = new int[] { 0, 2, 2, 1, 1, 0, 0, 0, 0 };
                    break;
                case "legend":
                    rarity = Rarity.Legendary;
                    classWeight = new int[] { 0, 2, 2, 1, 1, 0, 0, 0, 0 };
                    break;
                default:
                    Debug.LogAssertion("상자 생성, 등급 에러");
                    classWeight = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 1 };
                    rarity = Rarity.Banned;
                    break;
            }

            for(int i = 0; i < classWeight.Length; i++)
            {
                for (int j = 0; j < classWeight[i]; j++)
                {
                    classPool.Add((CardClass)i);
                }
            }

            int count = 3;
            while (count > 0)
            {
                int val = Random.Range(0, classPool.Count);

                cardClass = classPool[val];
                IItem pack = CardPack.GenerateCardPack(cardClass, rarity);
                pack.Owner = ch;
                ch.DroppingItems.Add(pack);

                while (classPool.Remove(cardClass)) { }
                
                count--;
            }

            return ch;
        }
    }
}
