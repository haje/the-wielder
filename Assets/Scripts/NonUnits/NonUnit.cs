﻿using System.Collections.Generic;
using UnityEngine;
using Wielder.Units;
using Wielder.Maps;
using Wielder.Visual;
using Wielder.Item;

namespace Wielder.NonUnits
{
    public enum NonUnitType
    {
        Obstacle,
        TrashCan,
        Shopkeeper, // Npc로 분류하는 게 나을수도
        Chest,
        Gold,
        RecoveryMarble,
        Floor,
        Heart,
        ManaCrystal
    }

    public abstract class NonUnit : IPlacable, IItemOwner
    {
        public abstract NonUnitType NonUnitType { get; }
        public bool OccupiesCell { get; protected set; }
        public Sprite NonUnitImage { get; protected set; }
        public Cell PlacedCell { get; private set; }
        public NonUnitScript Indicator;
        public string Name { get; protected set; }
        public string Description { get; protected set; }
        public float YCalibration { get; protected set; }
        protected List<CellModifier> modifiers;
        public List<CellModifier> Modifiers
        {
            get
            {
                return modifiers;
            }
        }

        public void PlaceOnCell(Cell cell)
        {
            if (PlacedCell != null)
                PlacedCell.PlacedNonUnits.Remove(this);
            Debug.Assert(!cell.PlacedNonUnits.Contains(this));
            Debug.Assert(!OccupiesCell || !cell.Occupied);
            cell.PlacedNonUnits.Add(this);
            PlacedCell = cell;
        }

        protected void Remove()
        {
            GameManager.CurrentRoom.RemoveNonUnit(this);
        }

        /// <summary>
        /// 논유닛이 있는 셀을 클릭했을 때 불리는 함수
        /// </summary>
        public virtual bool Clicked()
        {
            return false; // 논유닛이 없어지면 true를 반환, 그 외엔 false를 반환
        }

        /// <summary>
        /// 논유닛이 있는 셀에 들어갔을 때 불리는 함수
        /// </summary>
        public virtual bool Faced(Unit unit)
        {
            return false; // 논유닛이 없어지면 true를 반환, 그 외엔 false를 반환
        }

        public void AddCellModifier(CellModifier modifier)
        {
            modifier.Owner = this;
            modifiers.Add(modifier);
            GameManager.AddPassiveOwner(modifier);
        }

        public void RemoveModifier(CellModifier modifier)
        {
            modifiers.Remove(modifier);
            GameManager.RemovePassiveOwner(modifier);
        }

        public void RemoveAllModifiers()
        {
            while (modifiers.Count > 0)
            {
                GameManager.RemovePassiveOwner(modifiers[0]);
                modifiers.RemoveAt(0);
            }
        }
    }
}
