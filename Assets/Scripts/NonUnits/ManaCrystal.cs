﻿using Wielder.Units;
using Wielder.Visual;
using Wielder.Item;

namespace Wielder.NonUnits
{
    class ManaCrystal : NonUnit
    {
        public int ManaPrize { get; protected set; }

        public override NonUnitType NonUnitType
        {
            get
            {
                return NonUnitType.ManaCrystal;
            }
        }

        public ManaCrystal(int amount = 1)
        {
            Name = "마나 정수";
            NonUnitImage = FileResourceManager.GetSprite("ManaCrystal");
            YCalibration = 0.15f;
            OccupiesCell = false;
            ManaPrize = amount;
            Description = string.Format("최대 행동력이 {0} 증가한다.", amount);
        }

        public override bool Faced(Unit unit)
        {
            if (unit.UnitType != UnitType.Player)
                return false;

            IItem mana = new PassiveItem("manacrystal");
            unit.GetItem(mana);
            
            Remove();

            return true;
        }
    }
}
