﻿using UnityEngine;

namespace Wielder.NonUnits
{
    public class NonUnitBase : SerializableBase
    {
        public readonly Sprite ObjectImage;
        public readonly string Type;
        public readonly string Name;
        public readonly bool OccupiesCell;
        public readonly int MoneyPrize;
        public readonly float YCalibration;
        public readonly string Description;

        [SerializeField]
        private string type;

        [SerializeField]
        private string name;

        [SerializeField]
        private string imageFileName;

        [SerializeField]
        private bool occupiesCell;

        [SerializeField]
        private int moneyPrize;

        [SerializeField]
        private float yCalibration;

        [SerializeField]
        private string description;

        public NonUnitBase(string fileName) 
            : base(fileName)
        {
            Type = type;
            Name = name;
            OccupiesCell = occupiesCell;
            YCalibration = yCalibration;
            MoneyPrize = moneyPrize;
            Description = description;
            ObjectImage = FileResourceManager.GetSprite(imageFileName);
        }

        protected override string FileDirectory
        {
            get
            {
                return "NonUnits/";
            }
        }
    }
}
