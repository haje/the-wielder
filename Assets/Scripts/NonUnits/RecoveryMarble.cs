﻿using Wielder.Units;
using Wielder.Visual;

namespace Wielder.NonUnits
{
    class RecoveryMarble : NonUnit
    {
        public int Recovery { get; protected set; }

        public override NonUnitType NonUnitType
        {
            get
            {
                return NonUnitType.RecoveryMarble;
            }
        }

        public RecoveryMarble(int amount = 1)
        {
            Name = "재생의 구슬";
            NonUnitImage = FileResourceManager.GetSprite("Recovery_Marble");
            YCalibration = 0.15f;
            OccupiesCell = false;
            Recovery = amount;
            Description = string.Format(" {0} 회복", amount);
        }

        public override bool Faced(Unit unit)
        {
            if (unit.UnitType != UnitType.Player)
                return false;

            GameManager.Player.GetHealed(Recovery);
            Remove();

            return true;
        }
    }
}
