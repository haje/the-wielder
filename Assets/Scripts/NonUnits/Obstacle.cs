﻿namespace Wielder.NonUnits
{
    class Obstacle : NonUnit
    {
        public override NonUnitType NonUnitType
        {
            get
            {
                return NonUnitType.Obstacle;
            }
        }

        public Obstacle()
        {
            Name = "바위";
            NonUnitImage = FileResourceManager.GetSprite("Rock");
            YCalibration = 0.15f;
            OccupiesCell = true;
        }
    }
}
