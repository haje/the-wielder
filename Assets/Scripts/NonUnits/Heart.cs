﻿using Wielder.Units;
using Wielder.Visual;
using Wielder.Item;

namespace Wielder.NonUnits
{
    class Heart : NonUnit
    {
        public int HeartPrize { get; protected set; }

        public override NonUnitType NonUnitType
        {
            get
            {
                return NonUnitType.Heart;
            }
        }

        public Heart(int amount = 1)
        {
            Name = "활력활력열매";
            NonUnitImage = FileResourceManager.GetSprite("Heart");
            YCalibration = 0.15f;
            OccupiesCell = false;
            HeartPrize = amount;
            Description = string.Format("최대 체력이 {0} 증가한다.", amount);

        }

        public override bool Faced(Unit unit)
        {
            if (unit.UnitType != UnitType.Player)
                return false;

            IItem heart = new PassiveItem("heartcrystal");
            unit.GetItem(heart);
            Remove();

            return true;
        }
    }
}
