﻿using System.Collections.Generic;
using Wielder.Cards;
using Wielder.Visual;
using Wielder.Item;
using UnityEngine;

namespace Wielder.NonUnits
{
    class Shopkeeper : NonUnit
    {
        public int DiscardPrice { get; private set; }
        public override NonUnitType NonUnitType
        {
            get
            {
                return NonUnitType.Shopkeeper;
            }
        }

        public List<IItem> SellingItems { get; private set; }

        public Shopkeeper()
        {
            Name = "상인";
            NonUnitImage = FileResourceManager.GetSprite("Shopkeeper");
            YCalibration = 0.15f;
            OccupiesCell = true;
            SellingItems = new List<IItem>();
        }

        public override bool Clicked()
        {
            //shop algorithm
            //UIScript에서 shop을 띄우고 아이템 선택 시 BuyItem을 부르면 됨
            if (PopupUIScript.Instance.GetState() != PopupState.None)
            {
                return false;
            }
            ShopInteractionScript.Instance.LinkedComponent = this;
            PopupUIScript.Instance.ShowShop();

            return false;
        }

        public bool BuyItem(IItem item)
        {
            if (GameManager.Player.CurrentMoney < item.Price) return false;
            if (item is Card)
            {
                GameManager.Player.AddCardToDeck((Card)item);
            }
            else if (item is ItemChest)
            {
                GameManager.Player.GetMoney(-item.Price);
                PopupUIScript.Instance.HideShop();
                Chest chest = Chest.GenerateChest();
                GameManager.CurrentRoom.SpawnNonUnit(chest, GameManager.Player.PlacedCell);
                chest.Faced(GameManager.Player);
                return false;// 후처리 안함
            }
            else if (item is Trinket)
            {
                GameManager.Player.GetMoney(-item.Price);
                GameManager.Player.GetItem(item);
                SellingItems.RemoveAll(iitem => iitem is Trinket);
                return true;
            }
            else
            {
                GameManager.Player.GetItem(item);
            }
            GameManager.Player.GetMoney(-item.Price);
            SellingItems.Remove(item);
            return true;
        }

        public bool DiscardItem(IItem item)
        {
            if (GameManager.Player.CurrentMoney < DiscardPrice) return false;
            Debug.Assert(item is Card);
            GameManager.Player.RemoveCardInDeck((Card)item);
            GameManager.Player.GetMoney(-DiscardPrice);
            DiscardPrice += 2;
            return true;
        }

        public static Shopkeeper GenerateShopkeeper()
        {
            Shopkeeper sk = new Shopkeeper();

            IItem chest = new ItemChest();
            chest.Owner = sk;
            sk.SellingItems.Add(chest);

            ItemSet shopItemSet = new ItemSet("shopitemset");

            foreach (PassiveItem item in shopItemSet.PassiveItems)
            {
                item.Owner = sk;
                sk.SellingItems.Add(item);
            }

            for (int i = 0; i < 3;)
            {
                Trinket trinket = shopItemSet.Trinkets[Random.Range(0, shopItemSet.Trinkets.Count)];
                if (sk.SellingItems.Contains(trinket)) continue;
                trinket.Owner = sk;
                sk.SellingItems.Add(trinket);
                i++;
            }
            

            return sk;
        }

    }
}
