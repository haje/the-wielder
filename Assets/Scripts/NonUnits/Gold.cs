﻿using Wielder.Units;
using Wielder.Visual;

namespace Wielder.NonUnits
{
    class Gold : NonUnit
    {
        public int MoneyPrize { get; protected set; }

        public override NonUnitType NonUnitType
        {
            get
            {
                return NonUnitType.Gold;
            }
        }

        public Gold(int amount = 0)
        {
            Name = "동전";
            NonUnitImage = FileResourceManager.GetSprite("Coin");
            YCalibration = 0.15f;
            OccupiesCell = false;
            MoneyPrize = amount;
            Description = string.Format(" {0}골드", amount);
        }

        public override bool Faced(Unit unit)
        {
            if (unit.UnitType != UnitType.Player)
                return false;

            GameManager.Player.GetMoney(MoneyPrize);
            Remove();

            return true;
        }
    }
}
