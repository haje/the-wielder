﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Wielder.Cards;
using Wielder.Maps;
using Wielder.NonUnits;
using Wielder.Passive;
using Wielder.Passive.Auras;
using Wielder.Passive.Triggers;
using Wielder.Units;
using Wielder.Visual;
using Wielder.Item;

namespace Wielder
{
    /// <summary>
    /// 게임 전체 핵심 로직을 담당하는 클래스
    /// </summary>
    public static class GameManager
    {
        public enum TurnPhase
        {
            NotInCombat,
            TurnStart,
            Main,
            TurnEnd,
        }

        public static TurnPhase CurrentPhase = TurnPhase.NotInCombat;

        public static Map CurrentStage { get; private set; }
        public static Room CurrentRoom { get; private set; }
        public static Unit Player { get; private set; }
        public static Unit CurrentTurnOwner { get; private set; }
        public static bool Initialized { get; private set; }
        private static Unit nextTurnOwner;
        private static bool currentTurnOwnerRemoved = false;

        private static List<IPassiveOwner> passiveOwners;
        public static ReadOnlyCollection<IPassiveOwner> PassiveOwners
        {
            get
            {
                return passiveOwners.AsReadOnly();
            }
        }

        private static List<ChainObject> theChain;

        private static List<TriggerQueueObject> triggerQueue;

        public static string PlayerName { get; private set; }

        public static void Initialize(string playername)
        {
            passiveOwners = new List<IPassiveOwner>();
            theChain = new List<ChainObject>();
            triggerQueue = new List<TriggerQueueObject>();
            Player = new Player(playername);
            PlayerName = playername;
            Initialized = true;
        }

        public static void AddPassiveOwner(IPassiveOwner passiveOwner)
        {
            passiveOwners.Add(passiveOwner);
        }

        public static void RemovePassiveOwner(IPassiveOwner passiveOwner)
        {
            passiveOwners.Remove(passiveOwner);
        }

        public static ReadOnlyCollection<ChainObject> TheChain
        {
            get { return theChain.AsReadOnly(); }
        }

        public static void PushBackToChain(ChainObject chainObject)
        {
            theChain.Add(chainObject);
            if (theChain.Count == 1)
                CoroutineExecuterScript.Instance.StartCoroutine(CheckChainAndExecute());
        }

        public static void InsertToChain(ChainObject chainObject, int index)
        {
            theChain.Insert(index, chainObject);
            if (theChain.Count == 1)
                CoroutineExecuterScript.Instance.StartCoroutine(CheckChainAndExecute());
        }

        private static IEnumerator CheckChainAndExecute()
        {
            yield return null; // 남은 로직 다 처리될 때까지 1프레임 기다리기
            yield return new WaitUntil(() => triggerQueue.Count == 0); // 트리거가 있으면 무조건 트리거에 실행 양보

            if (CurrentTurnOwner == Player)
                UIManagerScript.Instance.SetUIState(UIState.Disabled);
            while (theChain.Count > 0)
            {
                ChainObject cObj = theChain[0];

                // 이 시점에 타겟이 이미 전장에 없으면 모든 효과를 무효화함
                if (cObj.CParam == null || cObj.CParam.TargetUnit == null || CurrentRoom.Units.Contains(cObj.CParam.TargetUnit))
                {
                    yield return CoroutineExecuterScript.Instance.StartCoroutine(cObj.VisualEffect());
                    cObj.GameEffect();
                }

                CurrentRoom.RefreshAllIndicators();
                PlayerInfoManager.Instance.DisplayPlayerHand();
                PlayerInfoManager.Instance.UpdatePlayerInfo();
                UnitInfoManager.Instance.RefreshDisplay();

                // 지금 닫는 중괄호 직전에 있던 theChain.Remove를 StateCheck 위로 올렸는데 이거 때문에 문제 생기지 않을지 좀 걱정됨. 해 봐야 알 것 같음.
                theChain.Remove(cObj);
                StateCheck();
                if (Player.CurrentRemainingHP <= 0)
                    yield break;
                yield return new WaitUntil(() => triggerQueue.Count == 0);
                // 원래 theChain.Remove가 있던 자리
            }

            if (currentTurnOwnerRemoved)
            {
                currentTurnOwnerRemoved = false;
                CurrentTurnOwner = nextTurnOwner;
                nextTurnOwner = GetNextUnit(CurrentTurnOwner);
                BeginUnitTurn();
            }
            else if (PopupUIScript.Instance.CurrentState != PopupState.Chest) // chest를 열고 있는 상태로 다른 행동을 할 수 없게 함
                ContinuePhase();
        }

        public static void AddTriggerToQueue(TriggerQueueObject trigger)
        {
            triggerQueue.Add(trigger);
            if (triggerQueue.Count == 1)
                CoroutineExecuterScript.Instance.StartCoroutine(CheckTriggerQueueAndActivate());
        }

        private static IEnumerator CheckTriggerQueueAndActivate()
        {
            yield return null; // 남은 로직 다 처리될 때까지 1프레임 기다리기

            if (CurrentTurnOwner == Player)
                UIManagerScript.Instance.SetUIState(UIState.Disabled);

            while (triggerQueue.Count > 0)
            {
                TriggerQueueObject trigger = triggerQueue[0];
                yield return CoroutineExecuterScript.Instance.StartCoroutine(trigger.VisualEffect());
                if (trigger.PushBefore == null)
                {
                    PushBackToChain(trigger.ChainObjectToPush);
                }
                else
                {
                    InsertToChain(trigger.ChainObjectToPush, theChain.FindIndex(cObj => cObj == trigger.PushBefore));
                }
                triggerQueue.Remove(trigger);
            }

            // 플레이어 UI는 체인 쪽에서 처리해 줌
        }

        private static Unit GetNextUnit(Unit unit)
        {
            int index = CurrentRoom.Units.IndexOf(unit);
            Debug.Assert(index != -1, "Unit of GetNextUnit not in current room");
            if (index < CurrentRoom.Units.Count - 1)
                return CurrentRoom.Units[index + 1];
            else
                return CurrentRoom.Units[0];
        }

        public static void StartStage(Map stage)
        {
            CurrentStage = stage;
            if (CurrentRoom != null)
                VisualLogicManager.FreeCurrentRoomResources();
            CurrentRoom = CurrentStage.GetCurrentRoom();
            MinimapContainerScript.Instance.Hide(); // 첫 월드일 땐 아무 동작 안함
            MinimapContainerScript.Instance.Show();
            StartRoom(CurrentRoom, null);
        }

        private static void StartRoom(Room room, Direction? startDirection)
        {
            string bGMToPlay = BGMManagerScript.Floor1BGMName;
            if (room.Type == RoomType.BossRoom)
                switch (room.BaseRoomData.FileName)
                {
                    case "boss1":
                        bGMToPlay = BGMManagerScript.Boss1BGMName;
                        break;
                    case "boss2":
                        bGMToPlay = BGMManagerScript.Boss2BGMName;
                        break;
                    default:
                        bGMToPlay = BGMManagerScript.Boss3BGMName;
                        break;
                }
            else
                switch (GameManager.CurrentStage.CurrentLevel)
                {
                    case 1:
                        break;
                    case 2:
                        bGMToPlay = BGMManagerScript.Floor2BGMName;
                        break;
                    default:
                        Debug.LogError("Not Defined Case");
                        break;
                }

            BGMManagerScript.Instance.PlayBGM(bGMToPlay);

            CurrentRoom = room;
            HexCoord initialPos = new HexCoord();
            int leftX = 0;
            int centerX = CurrentRoom.RoomWidth / 2;
            int rightX = CurrentRoom.RoomWidth - 1;
            int topY = 0;
            int centerY = CurrentRoom.RoomHeight / 2;
            int bottomY = CurrentRoom.RoomHeight - 1;
            if (startDirection == null)
                initialPos = new HexCoord(centerX, centerY);
            else
            {
                switch (startDirection)
                {
                    case Direction.UP:
                        initialPos = new HexCoord(centerX, topY);
                        break;
                    case Direction.UPPER_RIGHT:
                        initialPos = new HexCoord(rightX, topY);
                        break;
                    case Direction.LOWER_RIGHT:
                        initialPos = new HexCoord(rightX, bottomY);
                        break;
                    case Direction.DOWN:
                        initialPos = new HexCoord(centerX, bottomY);
                        break;
                    case Direction.LOWER_LEFT:
                        initialPos = new HexCoord(leftX, bottomY);
                        break;
                    case Direction.UPPER_LEFT:
                        initialPos = new HexCoord(leftX, topY);
                        break;

                }
            }
            CurrentRoom.SpawnUnit(Player, CurrentRoom.Cells[initialPos]);
            CurrentTurnOwner = Player;

            Debug.Assert(Player != null);
            VisualLogicManager.ShowRoom(CurrentRoom);
            UIManagerScript.Instance.SetUIState(UIState.PlayerTurnBasic);

            if (room.IsCombatFinished)  // room cleared before
            {
                UIManagerScript.Instance.SetUIState(UIState.RoomEnded);
                return;
            }
            else if (room.CheckCombatFinished())    // non-battle room visited first time
            {
                EndCombat();
                return;
            }

            nextTurnOwner = GetNextUnit(Player);
            StateCheck();
            foreach (Unit unit in CurrentRoom.Units)
                unit.DiscardAndDraw();

            BeginUnitTurn();
        }

        public static void MoveRoom(Direction dir)
        {
            VisualLogicManager.FreeCurrentRoomResources();

            CurrentStage.MoveToRoom(dir);
            CurrentRoom = CurrentStage.GetCurrentRoom();

            StartRoom(CurrentRoom, DirectionUtil.GetOppositeDirection(dir));

            MinimapContainerScript.Instance.Refresh();
        }

        /// <summary>
        /// 전투가 끝났을 때 부르는 함수. 
        /// </summary>
        private static void EndCombat()
        {
            triggerQueue.Clear();
            theChain.Clear();

            // 전투 끝나면 안 영구적인 상태이상은 다 풂
            foreach (UnitModifier modifier in new List<UnitModifier>(Player.Modifiers))
                if (modifier.RemoveOnEndCombat)
                    Player.RemoveModifier(modifier);

            AuraCheck();

            CurrentPhase = TurnPhase.NotInCombat;

            Player.GetShield(-Player.CurrentShield);
            CurrentRoom.RefreshAllIndicators();
            UnitInfoManager.Instance.RefreshDisplay();

            PlayerInfoManager.Instance.FreeDisplayingCard();
            UIManagerScript.Instance.SetUIState(UIState.RoomEnded);
        }

        private static void AuraCheck()
        {
            foreach (Unit unit in CurrentRoom.Units)
            {
                foreach (UnitModifier modifier in new List<UnitModifier>(unit.Modifiers))
                    if (modifier.Source != null)
                        unit.RemoveModifier(modifier);

                foreach (Card card in unit.GetCards())
                    foreach (CardModifier modifier in new List<CardModifier>(card.Modifiers))
                        if (modifier.Source != null)
                            card.RemoveModifier(modifier);
            }

            foreach (IPassiveOwner passiveOwner in new List<IPassiveOwner>(PassiveOwners))
                foreach (Unit unit in CurrentRoom.Units)
                {
                    foreach (UnitAura aura in passiveOwner.PassiveHandler.UnitAura)
                        if (aura.Condition(passiveOwner, unit))
                            unit.AddModifier(aura.GenerateModifier(passiveOwner));

                    foreach (Card card in unit.GetCards())
                        foreach (CardAura aura in passiveOwner.PassiveHandler.CardAura)
                            if (aura.Condition(passiveOwner, card))
                                card.AddModifier(aura.GenerateModifier(passiveOwner));
                }
        }

        private static void StateCheck()
        {
            for (int i = CurrentRoom.Units.Count - 1; i >= 0; i--)
            {
                Unit unit = CurrentRoom.Units[i];
                if (unit.CurrentRemainingHP <= 0)
                {
                    if (unit != Player)
                    {
                        GiveDrop(unit);
                    }
                    unit.Die();

                    if (unit == Player)
                    {
                        EndCombat();
                        UIManagerScript.Instance.SetUIState(UIState.GameOver);
                    }
                }
            }

            AuraCheck();

            if (CurrentRoom.IsCombatFinished) // 이미 전투가 끝나 있었을 경우
            {
                if (PopupUIScript.Instance.CurrentState != PopupState.Chest) // chest를 열고 있는 상태로 다른 행동을 할 수 없게 함
                    UIManagerScript.Instance.SetUIState(UIState.RoomEnded);
            }
            else if (triggerQueue.Count == 0 && TheChain.Count == 0 && CurrentRoom.CheckCombatFinished()) // 이번 체인 처리에서 전투가 끝난 경우
                EndCombat();
        }

        private static void GiveDrop(Unit unit)
        {
            if (unit.CurrentMoney > 0)//코인 드랍
            {
                NonUnit gold = new Gold(unit.CurrentMoney);
                CurrentRoom.SpawnNonUnit(gold, unit.PlacedCell);
            }
        }

        public static void RemoveUnit(Unit unit)
        {
            if (nextTurnOwner == unit)
                nextTurnOwner = GetNextUnit(unit);

            CurrentRoom.RemoveUnit(unit);

            if (CurrentTurnOwner == unit)
            {
                currentTurnOwnerRemoved = true;
                // 자기 턴에 전장을 뜨게 되면 그 즉시 턴 종료 트리거도 부름. 단 자기 자신 건 안 불림.
                if (CurrentPhase != TurnPhase.TurnEnd)
                    foreach (IPassiveOwner listener in PassiveOwners)
                        foreach (var trigger in listener.PassiveHandler.TurnEnd)
                        {
                            if (trigger.Condition(listener, CurrentTurnOwner))
                                AddTriggerToQueue(new TriggerQueueObject(trigger.GenerateChainObject(listener, CurrentTurnOwner),
                                GeneralVisualEffectContainer.GenerateTempFlashEffect(listener)));
                        }
            }
        }

        public static void EndUnitTurn()
        {
            if (CurrentRoom.CheckCombatFinished())
            {
                EndCombat();
                return;
            }
            CurrentPhase = TurnPhase.TurnEnd;

            foreach (IPassiveOwner listener in PassiveOwners)
                foreach (var trigger in listener.PassiveHandler.TurnEnd)
                {
                    if (trigger.Condition(listener, CurrentTurnOwner))
                        AddTriggerToQueue(new TriggerQueueObject(trigger.GenerateChainObject(listener, CurrentTurnOwner),
                        GeneralVisualEffectContainer.GenerateTempFlashEffect(listener)));
                }

            if (triggerQueue.Count == 0)
                ContinuePhase();
        }

        private static void BeginUnitTurn()
        {
            CurrentPhase = TurnPhase.TurnStart;

            foreach (IPassiveOwner listener in PassiveOwners)
                foreach (var trigger in listener.PassiveHandler.TurnStart)
                {
                    if (trigger.Condition(listener, CurrentTurnOwner))
                        AddTriggerToQueue(new TriggerQueueObject(trigger.GenerateChainObject(listener, CurrentTurnOwner),
                        GeneralVisualEffectContainer.GenerateTempFlashEffect(listener)));
                }

            if (triggerQueue.Count == 0)
                ContinuePhase();
        }

        private static void ContinuePhase()
        {
            switch (CurrentPhase)
            {
                case TurnPhase.TurnStart:
                    CurrentPhase = TurnPhase.Main;
                    CurrentTurnOwner.TakeTurn();
                    break;
                case TurnPhase.NotInCombat:
                case TurnPhase.Main:
                    CurrentTurnOwner.PerformAction();
                    break;
                case TurnPhase.TurnEnd:
                    if (!currentTurnOwnerRemoved)
                        CurrentTurnOwner.DiscardAndDraw();
                    CurrentTurnOwner = nextTurnOwner;
                    nextTurnOwner = GetNextUnit(CurrentTurnOwner);
                    BeginUnitTurn();
                    break;
                default:
                    Debug.LogError("We're not allowed to reach here");
                    break;
            }
        }

        public static void StartGame(string playername)
        {
            Initialize(playername);
            ItemSet cardload = new ItemSet("fullcardset");
            if (LogManager.Instance != null)
                LogManager.Instance.ResetLog();
            Map stage = new Map(1);
            StartStage(stage);
        }

        public static void EndGame()
        {
            VisualLogicManager.FreeCurrentRoomResources();
            CurrentRoom = null;
        }

        public static void StartDebugMode()
        {
            Initialize("newbie");
            Map stage = new Map();
            StartStage(stage);
            UIManagerScript.Instance.DebugMode = true;
            DebugManagerScript.Instance.Show();

        }

        public static void ReloadRoom(Room room)
        {
            VisualLogicManager.FreeCurrentRoomResources();
            CurrentRoom = room;
            VisualLogicManager.ShowRoom(CurrentRoom);
        }

        public static void RestartRoom(Room room, Direction? startDirection)
        {
            VisualLogicManager.FreeCurrentRoomResources();
            CurrentRoom.IsCombatFinished = false;
            StartRoom(room, startDirection);
        }
    }
}
