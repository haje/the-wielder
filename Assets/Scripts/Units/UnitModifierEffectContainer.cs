﻿using System;
using Wielder.Visual;

namespace Wielder.Units
{
    public partial class Unit
    {
        public static class UnitModifierEffectContainer
        {
            public static UnitModifierEffect GetUnitModifierEffectFromName(string functionName)
            {
                return DelegateHelper.GetDelegateFromName<UnitModifierEffect>
                    (typeof(UnitModifierEffectContainer), functionName);
            }

            public static void NoModifierEffect(UnitModifier modifier)
            { }

            public static void ZentaPassive(UnitModifier modifier)
            {
                throw new NotImplementedException();
                /*
                UnitEffect passive = new UnitEffect(target,
                    effectFunc: unit => unit.AddCardToDeck(new Card("multishot")),
                    conditionCheckFunction: unit => unit.CurrentRemainingHP <= 8);
                modifier.owner.AddUnitEffect(passive);
                */
            }

            public static void ArtilleryPassive(UnitModifier modifier)
            {
                throw new NotImplementedException();
                /*
                UnitEffect passive = new UnitEffect(target,
                    effectFunc: unit => { },
                    conditionCheckFunction: unit => unit.CurrentMaxHP <= 0);
                modifier.owner.AddUnitEffect(passive);
                */
            }

            static void AddHeart(UnitModifier modifier)
            {
                modifier.Owner.CurrentMaxHP++;
                modifier.Owner.CurrentRemainingHP++;
            }

            static void AddMana(UnitModifier modifier)
            {
                modifier.Owner.APPerTurn++;
            }

            static void AddHandSize(UnitModifier modifier)
            {
                modifier.Owner.HandSize++;
            }

            //TODO
            static void AddMoneyRatio(UnitModifier modifier)
            {
                modifier.Owner.Die(); //빅-엿
            }

            static void PreventDeath(UnitModifier modifier)
            {
                modifier.PassiveHandler.UnitDeath.Add(new Passive.Triggers.UnitDeathTrigger(
                    (passiveOwner, deadUnit) => 
                    {
                        if(((UnitModifier)passiveOwner).Owner == deadUnit)
                        {
                            ((UnitModifier)passiveOwner).Owner.CurrentRemainingHP = 1;
                            ((UnitModifier)passiveOwner).Owner.RemoveModifier(modifier);
                        }
                    },
                    (passiveOwner, deadUnit) => GeneralVisualEffectContainer.NoVisualEffect(),
                    (passiveOwner, deadUnit) => ((UnitModifier)passiveOwner).Owner.CurrentRemainingHP <= 0
                    ));
            }
        }
    }
}
