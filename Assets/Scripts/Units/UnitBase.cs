﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Wielder.AI;

namespace Wielder.Units
{
    [Serializable]
    public class UnitBase : SerializableBase
    {
        public readonly int MaxHP;
        public readonly int APPerTurn;
        public readonly int StartingHandSize;
        public readonly int Money;
        public readonly string Name;
        public readonly Sprite UnitImage;
        public readonly float YCalibration;
        public readonly ReadOnlyCollection<string> CardsInDeckNames;
        public readonly AIFunction AI;
        public readonly UnitInitializerEffect Initializer;

        [SerializeField]
        private int maxHP;

        [SerializeField]
        private int aPPerTurn;

        [SerializeField]
        private int startingHandSize;

        [SerializeField]
        private int money;

        [SerializeField]
        private string name;

        [SerializeField]
        private string unitImageFileName;

        [SerializeField]
        private float yCalibration;

        [SerializeField]
        private List<string> cardsInDeckNames;

        [SerializeField]
        private string aiFunctionName;

        [SerializeField]
        private string initializerFunctionName;

        protected override string FileDirectory
        {
            get
            {
                return "Units/";
            }
        }

        public UnitBase(string fileName)
            : base(fileName)
        {
            MaxHP = maxHP;
            APPerTurn = aPPerTurn;
            StartingHandSize = startingHandSize;
            Money = money;
            Name = name;
            YCalibration = yCalibration;
            CardsInDeckNames = cardsInDeckNames.AsReadOnly();
            UnitImage = FileResourceManager.GetSprite(unitImageFileName);
            if (aiFunctionName != null)
                AI = AIFunctionContainer.GetFunctionFromName(aiFunctionName);
            if (initializerFunctionName != null)
                Initializer = Unit.UnitInitializerEffectContainer.GetUnitInitializerEffectFromName(initializerFunctionName);
        }
    }

}