﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Wielder.Cards;
using Wielder.Item;
using Wielder.Maps;
using Wielder.Passive;
using Wielder.Passive.Replacements;
using Wielder.Passive.Triggers;
using Wielder.Visual;

namespace Wielder.Units
{
    public abstract partial class Unit : IPlacable, IDamageDealer, IItemOwner
    {
        public UnitBase BaseUnitData;

        public Cell PlacedCell { get; private set; }
        public Visual.UnitScript Indicator;

        protected List<Card> deck;
        protected List<Card> hand;
        protected List<IItem> inventory;
        protected List<Card> graveyard;
        protected List<Card> specialZone;
        private List<UnitModifier> modifiers;
        public ReadOnlyCollection<Card> Deck
        {
            get { return deck.AsReadOnly(); }
        }

        public ReadOnlyCollection<Card> Hand
        {
            get { return hand.AsReadOnly(); }
        }

        public ReadOnlyCollection<IItem> Inventory
        {
            get { return inventory.AsReadOnly(); }
        }

        public ReadOnlyCollection<Card> Graveyard
        {
            get { return graveyard.AsReadOnly(); }
        }
        public ReadOnlyCollection<UnitModifier> Modifiers
        {
            get { return modifiers.AsReadOnly(); }
        }

        public string Name { get; private set; }
        public int CurrentMaxHP { get; private set; }
        public int APPerTurn { get; private set; }
        public int CurrentMoney { get; protected set; }
        public int HandSize { get; private set; }

        public int CurrentRemainingHP { get; private set; }
        public int CurrentAP { get; set; }
        public int CurrentShield { get; private set; }
        public int MovementAP { get; protected set; }
        public abstract UnitType UnitType { get; }

        public int UsedCardInTurn { get; private set; }

        public Unit(UnitBase unitBase)
        {
            BaseUnitData = unitBase;

            deck = new List<Card>();
            hand = new List<Card>();
            inventory = new List<IItem>();
            graveyard = new List<Card>();
            specialZone = new List<Card>();
            modifiers = new List<UnitModifier>();

            if (BaseUnitData.Initializer != null)
                BaseUnitData.Initializer(this);

            foreach (string fileName in unitBase.CardsInDeckNames)
            {
                Card newCard = new Card(fileName);
                newCard.Owner = this;
                deck.Add(newCard);
            }

            CurrentMoney = BaseUnitData.Money;
            CurrentRemainingHP = CurrentMaxHP = BaseUnitData.MaxHP;

            ShuffleCards();
            RefreshModifiers();
            // Drawing part of monster is moved to Monster.Monster(), player draws first hand at GameManager.StartRoom()
        }

        private void RefreshModifiers()
        {
            Name = BaseUnitData.Name;
            CurrentMaxHP = BaseUnitData.MaxHP;
            APPerTurn = BaseUnitData.APPerTurn;
            HandSize = BaseUnitData.StartingHandSize;
            MovementAP = 2;
            ParticleSystem pSys = null;
            if (Indicator != null) pSys = Indicator.GetComponent<ParticleSystem>();
            bool stopPSys = true;
            foreach (UnitModifier modifier in modifiers)
            {
                modifier.Effect(modifier);
                if (modifier.RemoveOnEndCombat) stopPSys = false;
            }
            if (pSys != null)
                if (stopPSys && pSys.isPlaying) pSys.Stop();
                else if (!stopPSys && !pSys.isPlaying) pSys.Play();

            CurrentRemainingHP = Mathf.Min(CurrentRemainingHP, CurrentMaxHP);
        }

        public void AddModifier(UnitModifier modifier)
        {
            modifier.Owner = this;

            if (modifier.Source != null)
            {
                int i;
                for (i = 0; i < modifiers.Count; i++)
                {
                    if (modifiers[i].Source == null)
                        break;
                }
                modifiers.Insert(i, modifier);
            }
            else
                modifiers.Add(modifier);

            GameManager.AddPassiveOwner(modifier);
            RefreshModifiers();
        }

        public void RemoveModifier(UnitModifier modifier)
        {
            modifiers.Remove(modifier);
            GameManager.RemovePassiveOwner(modifier);
            RefreshModifiers();
        }

        public void RemoveDuplicateModifier(string modifierName)
        {
            foreach (UnitModifier modifier in modifiers)
                if (modifier.Name == modifierName)
                {
                    RemoveModifier(modifier);
                    return;
                }
        }

        public void PlaceOnCell(Cell cell)
        {
            if (PlacedCell != null)
                PlacedCell.PlacedUnit = null;
            if (cell.Occupied)
            {
                string error = string.Format("해당 위치에 {0} 유닛을 놓을 수 없음", Name);
                if (cell.PlacedUnit != null)
                    error = error + string.Format(" : 이미 {0} 유닛이 존재함", cell.PlacedUnit.Name);
                throw new System.Exception(error);
            }
            cell.PlacedUnit = this;
            PlacedCell = cell;
        }

        public Card FindCardInHand(string name)
        {
            foreach (Card c in hand)
            {
                if (c.Name == name) return c;
            }
            return null;
        }

        public Card FindCardInDeck(string name)
        {
            foreach (Card c in deck)
            {
                if (c.Name == name) return c;
            }
            return null;
        }

        public void UseCardInHand(Card card, Cell targetCell)
        {
            Debug.Assert(hand.Contains(card));
            Debug.Assert(card.Cost <= CurrentAP);
            Debug.Assert(card.BaseCardData.MoneyCost <= CurrentMoney);

            hand.Remove(card);
            graveyard.Add(card);
            CurrentAP -= card.Cost;
            CurrentMoney -= card.BaseCardData.MoneyCost;
            card.Activate(targetCell);
            UsedCardInTurn++;
        }

        public void SealCard(Card card)
        {
            Debug.Assert(graveyard.Contains(card));
            graveyard.Remove(card);
            specialZone.Add(card);
        }

        public void UnsealCard(Card card)
        {
            Debug.Assert(specialZone.Contains(card));
            specialZone.Remove(card);
            graveyard.Add(card);
        }

        public virtual void DiscardAndDraw()
        {
            Discard();
            Draw(HandSize);
        }

        public void AddCardToDeck(Card card)
        {
            deck.Add(card);
            card.Owner = this;
            ShuffleDeck();
        }

        public void RemoveCardInDeck(Card card)
        {
            Debug.Assert(deck.Contains(card) || hand.Contains(card) || specialZone.Contains(card));
            deck.Remove(card);
        }

        private void Discard() // 현재는 카드를 모두 버림
        {
            for (int i = hand.Count - 1; i >= 0; i--)
            {
                graveyard.Add(hand[i]);
                hand.RemoveAt(i);
            }
        }

        public void Draw(int num)
        {
            if (deck.Count == 0 && graveyard.Count == 0)
                return;
            for (int i = 0; i < num; i++)
            {
                if (deck.Count == 0 && graveyard.Count > 0)
                    ShuffleCards();

                if (deck.Count > 0)
                {
                    hand.Add(deck[0]);
                    deck.RemoveAt(0);
                }
            }
        }

        private void ShuffleDeck() // 덱의 카드를 섞음
        {
            for (int i = 0; i < deck.Count; i++)
            {
                Card temp = deck[i];
                int randomIndex = Random.Range(i, deck.Count);
                deck[i] = deck[randomIndex];
                deck[randomIndex] = temp;
            }
        }

        private void ShuffleCards() // 묘지와 덱의 카드를 섞음, 핸드는 섞지 않음
        {
            for (int i = graveyard.Count - 1; i >= 0; i--)
            {
                deck.Add(graveyard[i]);
                graveyard.RemoveAt(i);
            }

            ShuffleDeck();
        }

        public abstract void PerformAction();

        public void TakeTurn()
        {
            CurrentAP = APPerTurn;
            UsedCardInTurn = 0;
            UnitInfoManager.Instance.RefreshDisplay();
            PerformAction();
        }

        public void AddMovementToChain(Cell destination)
        {
            if (UIManagerScript.Instance.GetUIState() == UIState.RoomEnded)
            {
                GameManager.PushBackToChain(new ChainObject(
                    () => PlaceOnCell(destination), GeneralVisualEffectContainer.NoVisualEffect));
            }
            else
            {
                Debug.Assert(PlacedCell.GetDistanceFrom(destination) == 1);
                Debug.Assert(CurrentAP >= MovementAP);
                Cell previousCell = PlacedCell;
                GameManager.PushBackToChain(new ChainObject(
                    () =>
                    {
                        CurrentAP = Mathf.Min(CurrentAP, CurrentAP - MovementAP); // 이동할 수록 행동력 오르는 경우 방지
                        PlaceOnCell(destination);
                        foreach (IPassiveOwner listener in GameManager.PassiveOwners)
                            foreach (var trigger in listener.PassiveHandler.UnitMove)
                            {
                                if (trigger.Condition(listener, this, previousCell, PlacedCell))
                                    GameManager.AddTriggerToQueue(new TriggerQueueObject(
                                        trigger.GenerateChainObject(listener, this, previousCell, PlacedCell),
                                        GeneralVisualEffectContainer.GenerateTempFlashEffect(listener)));
                            }
                    }, () => GeneralVisualEffectContainer.MoveLinearly(
                        Indicator.gameObject, 0.2f, Indicator.transform.position, destination.Indicator.transform.position)));
            }

            int count = destination.PlacedNonUnits.Count;
            for (int i = 0; i < count; i++)
            {
                if (destination.PlacedNonUnits[i].Faced(this))
                {
                    i--; // NonUnit이 Faced했을 때 사라질 경우
                    count--;
                }
            }

            Indicator.GetComponent<SpriteRenderer>().sortingOrder = PlacedCell.Position.Y + 2;
        }

        public void TakeDamage(int amount, IDamageDealer source)
        {
            foreach (IPassiveOwner trigger in GameManager.PassiveOwners)
                foreach (DamageReplacement replacement in trigger.PassiveHandler.DamageReplacement)
                    if (replacement.Condition(amount, source, this))
                        amount = replacement.Effect(amount, source, this);

            amount = Mathf.Max(0, amount); // 최소 0 피해

            if (CurrentShield > 0)
            {
                int temp = amount;
                amount = Mathf.Max(0, amount - CurrentShield);
                CurrentShield = Mathf.Max(0, CurrentShield - temp);
                LogManager.Instance.Log(new LogParameter(this, temp - amount, LogType.Absorb));
            }
            CurrentRemainingHP = Mathf.Max(0, CurrentRemainingHP - amount);
            LogManager.Instance.Log(new LogParameter(this, amount, LogType.Deal));

            foreach (IPassiveOwner listener in GameManager.PassiveOwners)
                foreach (var trigger in listener.PassiveHandler.DealDamage)
                {
                    if (trigger.Condition(listener, source, this, amount))
                        GameManager.AddTriggerToQueue(new TriggerQueueObject(trigger.GenerateChainObject(listener, source, this, amount),
                            GeneralVisualEffectContainer.GenerateTempFlashEffect(listener)));
                }
        }

        public virtual void GetMoney(int amount)
        {
            CurrentMoney = Mathf.Max(0, CurrentMoney + amount);
        }

        public void GetItem(IItem item)
        {
            item.Owner = this;
            inventory.Add(item);
        }

        public void DiscardItem(IItem item)
        {
            Debug.Assert(inventory.Contains(item));
            inventory.Remove(item);
            item.Owner = null;
        }

        public void GetHealed(int amount)
        {
            foreach (IPassiveOwner trigger in GameManager.PassiveOwners)
                foreach (HealReplacement replacement in trigger.PassiveHandler.HealReplacement)
                    if (replacement.Condition(amount, this))
                        amount = replacement.Effect(amount, this);

            int beforeHeal = CurrentRemainingHP;
            CurrentRemainingHP = Mathf.Min(CurrentMaxHP, CurrentRemainingHP + amount);
            LogManager.Instance.Log(new LogParameter(this, CurrentRemainingHP - beforeHeal, LogType.Heal));
        }

        public void GetShield(int amount)
        {
            CurrentShield += amount;
        }

        public void Die()
        {
            foreach (IPassiveOwner listener in GameManager.PassiveOwners)
                foreach (var trigger in listener.PassiveHandler.UnitDeath)
                    if (trigger.Condition(listener, this))
                        GameManager.AddTriggerToQueue(new TriggerQueueObject(trigger.GenerateChainObject(listener, this),
                            GeneralVisualEffectContainer.GenerateTempFlashEffect(listener)));

            GameManager.RemoveUnit(this);
            LogManager.Instance.Log(new LogParameter(this, LogType.Die));

        }

        public ReadOnlyCollection<Card> GetCards()
        {
            List<Card> cards = new List<Card>();
            cards.AddRange(deck);
            cards.AddRange(graveyard);
            cards.AddRange(specialZone);
            cards.AddRange(hand);
            return cards.AsReadOnly();
        }
        /// <summary>
        /// Balancing환경에서만 부를 것
        /// </summary>
        /// <param name="amount"></param>
        public void AddHP(int amount)
        {
            CurrentMaxHP += amount;
            CurrentRemainingHP += amount;
        }

        public void AddMana(int amount)
        {
            APPerTurn += amount;
        }

        public void AddHandSize(int amount)
        {
            HandSize += amount;
        }

        public void ResetStat()
        {
            CurrentMaxHP = BaseUnitData.MaxHP;
            CurrentRemainingHP = CurrentMaxHP;
            APPerTurn = BaseUnitData.APPerTurn;
            HandSize = BaseUnitData.StartingHandSize;
        }
    }
}