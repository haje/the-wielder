﻿namespace Wielder.Units
{
    public partial class Unit
    {
        public static class UnitInitializerEffectContainer
        {
            public static UnitInitializerEffect GetUnitInitializerEffectFromName(string functionName)
            {
                return DelegateHelper.GetDelegateFromName<UnitInitializerEffect>
                    (typeof(UnitInitializerEffectContainer), functionName);
            }

            static void CheatInit(Unit unit)
            {
                unit.AddModifier(UnitModifierFactory.GetCheatModifier());
            }

            static void TauntInit(Unit unit)
            {
                unit.AddModifier(UnitModifierFactory.GetTauntModifier());
            }

            static void ZombieInit(Unit unit)
            {
                
                unit.AddModifier(UnitModifierFactory.GetRottingModifier(1));
            }

            static void GhoulInit(Unit unit)
            {
                unit.AddModifier(UnitModifierFactory.GetRottingModifier(2));
            }
        }
    }
}
