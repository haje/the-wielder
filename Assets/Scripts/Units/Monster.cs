﻿namespace Wielder.Units
{
    public class Monster : Unit
    {

        public override UnitType UnitType
        {
            get
            {
                return UnitType.Monster;
            }
        }

        public Monster(UnitBase unitBase)
            : base(unitBase)
        {
            // monsters no longer draw cards first when they are spawned.
            // Instead they draw when the player draws.
        }
        
        public override void PerformAction()
        {
            BaseUnitData.AI(this);
        }

    }
}