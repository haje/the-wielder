﻿namespace Wielder.Units
{
    public enum UnitType
    {
        Player,
        Monster,
        // 이 밑으로는 쓰일지 확실치 않음
        PlayerAlly,
        Neutral,
    }

    public static class UnitTypeHelper
    {
        public static bool IsAlliedWith(this UnitType thisUnitType, UnitType otherUnitType)
        {
            switch (thisUnitType)
            {
                case UnitType.Player:
                case UnitType.PlayerAlly:
                    return otherUnitType == UnitType.Player || otherUnitType == UnitType.PlayerAlly;
                case UnitType.Monster:
                    return otherUnitType == UnitType.Monster;
                default:
                    return false;
            }
        }

        public static bool IsHostileAgainst(this UnitType thisUnitType, UnitType otherUnitType)
        {
            switch (thisUnitType)
            {
                case UnitType.Player:
                case UnitType.PlayerAlly:
                    return otherUnitType.IsAlliedWith(UnitType.Monster);
                case UnitType.Monster:
                    return otherUnitType.IsAlliedWith(UnitType.Player);
                default:
                    return false;
            }
        }
    }
}