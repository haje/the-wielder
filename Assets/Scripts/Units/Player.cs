﻿using Wielder.Visual;

namespace Wielder.Units
{
    public class Player : Unit
    {
        public override UnitType UnitType
        {
            get
            {
                return UnitType.Player;
            }
        }

        public Player(string fileName)
            : base(FileResourceManager.GetUnitBase(fileName))
        { }

        public override void GetMoney(int amount)
        {
            base.GetMoney(amount);
            PlayerInfoManager.Instance.UpdatePlayerInfo();
        }

        public override void DiscardAndDraw()
        {
            base.DiscardAndDraw();
            PlayerInfoManager.Instance.DisplayPlayerHand();
        }

        public override void PerformAction()
        {
            if (UIManagerScript.Instance.GetUIState() != UIState.RoomEnded)
                UIManagerScript.Instance.SetUIState(UIState.PlayerTurnBasic);
        }
    }
}