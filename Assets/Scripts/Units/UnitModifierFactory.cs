﻿using System;
using Wielder.Cards;
using Wielder.Passive.Auras;
using Wielder.Passive.Replacements;
using Wielder.Passive.Triggers;
using Wielder.Visual;

namespace Wielder.Units
{
    public abstract partial class Unit
    {
        public static class UnitModifierFactory
        {
            public static UnitModifier GetIdolOfVitatlityOwnerModifier()
            {
                return new UnitModifier("활력의 우상 효과", "턴 당 행동력 +3", (modifier) => modifier.Owner.APPerTurn += 3);
            }

            public static UnitModifier GetTauntModifier()
            {
                UnitModifier taunt = new UnitModifier("도발", "적이 카드를 썼을 때 이 유닛이 대상이 될 수 있으면 그 카드의 대상을 이 유닛으로 바꾼다.",
                    UnitModifierEffectContainer.NoModifierEffect, false);

                taunt.PassiveHandler.BeforeCast.Add(new BeforeCastTrigger(
                    (passiveOwner, cParam) =>
                    {
                        cParam.TargetCell = taunt.Owner.PlacedCell;
                        cParam.TargetUnit = taunt.Owner;
                    },
                    (passiveOwner, cParam) => GeneralVisualEffectContainer.NoVisualEffect(),
                    (passiveOwner, cParam) => taunt.Owner.UnitType.IsHostileAgainst(cParam.Caster.UnitType) &&
                        cParam.Card.BaseCardData.HasTarget && GameManager.CurrentRoom.GetAvailableTargetCells(
                        cParam.Caster, cParam.Card.Range, cParam.Card.BaseCardData.TargetConditions,
                        cParam.Card.BaseCardData.CustomTargetCheckFunction).Contains(taunt.Owner.PlacedCell)));

                return taunt;
            }

            public static UnitModifier GetCheatModifier()
            {
                UnitModifier sagi = new UnitModifier("사기", "이 플레이어는 너무 사기라 딜을 제곱으로 받는다. 그리고 모든 적의 턴당 행동력과 손 크기가 3 늘어난다.",
                    UnitModifierEffectContainer.NoModifierEffect, false);

                sagi.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => amount * amount,
                    (amount, source, sufferer) => sufferer == GameManager.Player));

                sagi.PassiveHandler.UnitAura.Add(new UnitAura(
                    () => new UnitModifier("사기 플레이어의 가호", "플레이어가 너무 사기라 각 적은 손 크기와 턴 당 행동력에 +3 버프를 받는다.",
                        (_modifier) =>
                        {
                            _modifier.Owner.APPerTurn += 3;
                            _modifier.Owner.HandSize += 3;
                        }),
                    (source, targetUnit) => targetUnit != GameManager.Player));

                return sagi;
            }

            public static UnitModifier GetArtilleryModifier(CardEffectParameter param)
            {
                UnitModifier artillery = new UnitModifier("대포 발사 준비", string.Format(
                    "{0}턴 뒤의 턴 끝에 사용자가 살아 있으면 대상({2})에게 피해를 {1} 주고 포격 카드를 무덤에 넣는다.", param.Param1, param.Param2, param.TargetUnit.Name),
                    UnitModifierEffectContainer.NoModifierEffect, param.Param1);

                artillery.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger(
                    (passiveOwner, turnOwner) =>
                    {
                        param.Caster.UnsealCard(param.Card);
                        param.TargetUnit.TakeDamage(param.Param2, param.Caster);
                    }));

                return artillery;
            }

            public static UnitModifier GetBindModifier(CardEffectParameter param) // 속박
            {
                UnitModifier bind = new UnitModifier("속박", string.Format("{0}턴 동안 이동할 수 없다.", param.Param1),
                    (modifier) => modifier.Owner.MovementAP = 999, param.Param1);
                bind.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());
                return bind;
            }

            public static UnitModifier GetBleedModifier(CardEffectParameter param) // 출혈
            {
                UnitModifier bleed = new UnitModifier("출혈", string.Format("{1}턴 동안 턴 시작마다 피해를 {0} 입는다.", param.Param1, param.Param2),
                    UnitModifierEffectContainer.NoModifierEffect, param.Param2);

                bleed.PassiveHandler.TurnStart.Add(new TurnStartTrigger(
                    (passiveOwner, turnOwner) => bleed.Owner.TakeDamage(param.Param1, param.Card),
                    (passiveOwner, turnOwner) => GeneralVisualEffectContainer.NoVisualEffect(), TriggerHelper.SelfTurn));

                bleed.PassiveHandler.TurnStart.Add(TurnStartTrigger.GetModifierDurationTickTrigger());

                return bleed;
            }

            public static UnitModifier GetBulletSlugModifier(CardEffectParameter param) // 슬러그탄
            {
                param.Caster.RemoveDuplicateModifier("탄환 강화");

                UnitModifier bulletSlug = new UnitModifier("탄환 강화", param.Card.Description,
                    UnitModifierEffectContainer.NoModifierEffect, 2);

                bulletSlug.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                bulletSlug.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => amount + param.Param1,
                    (amount, source, sufferer) => DamageReplacement.DamageDealerBelongsToUnit(source, param.Caster) &&
                    source is Card && (source as Card).BaseCardData.CardClass == (CardClass) param.Param2));

                return bulletSlug;
            }

            public static UnitModifier GetBurnModifier(CardEffectParameter param) // 화상
            {
                UnitModifier burn = new UnitModifier("화상", string.Format("{1}턴 동안 턴 종료마다 피해를 {0} 입는다.", param.Param1, param.Param2),
                    UnitModifierEffectContainer.NoModifierEffect, param.Param2);

                burn.PassiveHandler.TurnEnd.Add(new TurnEndTrigger(
                    (passiveOwner, turnOwner) => burn.Owner.TakeDamage(param.Param1, param.Card),
                    (passiveOwner, turnOwner) => GeneralVisualEffectContainer.NoVisualEffect(), TriggerHelper.SelfTurn));

                burn.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                return burn;
            }

            public static UnitModifier GetClassAdditionModifier(CardEffectParameter param) // Param2 클래스의 카드 데미지를 Param1 만큼 더함
            {
                UnitModifier classAddition = new UnitModifier(param.Card.Name, param.Card.Description,
                    UnitModifierEffectContainer.NoModifierEffect, 1);

                classAddition.RemainingCast = 1;

                classAddition.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                classAddition.PassiveHandler.AfterCast.Add(AfterCastTrigger.GetModifierDurationTickTrigger(
                    (passiveOwner, cParam) => cParam.Caster.Modifiers.Contains(passiveOwner as UnitModifier) && cParam != param));

                classAddition.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => amount + param.Param1,
                    (amount, source, sufferer) => DamageReplacement.DamageDealerBelongsToUnit(source, param.Caster) &&
                    source is Card && (source as Card).BaseCardData.CardClass == (CardClass) param.Param2));

                return classAddition;
            }

            public static UnitModifier GetClassAdditionTwoTurnModifier(CardEffectParameter param)
            {
                UnitModifier classMultiply = new UnitModifier(param.Card.Name, param.Card.Description,
                    UnitModifierEffectContainer.NoModifierEffect, 2);

                classMultiply.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                classMultiply.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => amount + param.Param1,
                    (amount, source, sufferer) => DamageReplacement.DamageDealerBelongsToUnit(source, param.Caster) &&
                    source is Card && (source as Card).BaseCardData.CardClass == (CardClass) param.Param2));

                return classMultiply;
            }

            public static UnitModifier GetClassCostModifier(CardEffectParameter param) // Param2 클래스의 카드 코스트를 Param1 만큼 더함
            {
                UnitModifier classCost = new UnitModifier(param.Card.Name, param.Card.Description,
                    UnitModifierEffectContainer.NoModifierEffect, 1);

                classCost.RemainingCast = 1;

                classCost.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                classCost.PassiveHandler.AfterCast.Add(AfterCastTrigger.GetModifierDurationTickTrigger(
                    (passiveOwner, cParam) => cParam.Caster.Modifiers.Contains(passiveOwner as UnitModifier) && cParam != param));

                classCost.PassiveHandler.CostReplacement.Add(new CostReplacement(
                    (amount, card) => amount + param.Param1,
                    (amount, card) => card.Owner == param.Caster && card.BaseCardData.CardClass == (CardClass) param.Param2));

                return classCost;
            }

            public static UnitModifier GetClassMultiplyModifier(CardEffectParameter param) // Param2 클래스의 카드 데미지를 Param1 만큼 곱함
            {
                UnitModifier classMultiply = new UnitModifier(param.Card.Name, param.Card.Description,
                    UnitModifierEffectContainer.NoModifierEffect, 1);

                classMultiply.RemainingCast = 1;

                classMultiply.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                classMultiply.PassiveHandler.AfterCast.Add(AfterCastTrigger.GetModifierDurationTickTrigger(
                    (passiveOwner, cParam) => cParam.Caster.Modifiers.Contains(passiveOwner as UnitModifier) && cParam != param));

                classMultiply.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => amount * param.Param1,
                    (amount, source, sufferer) => DamageReplacement.DamageDealerBelongsToUnit(source, param.Caster) &&
                    source is Card && (source as Card).BaseCardData.CardClass == (CardClass) param.Param2));

                return classMultiply;
            }

            public static UnitModifier GetCleanseModifier(CardEffectParameter param) // 정화
            {
                UnitModifier cleanse = new UnitModifier(param.Card.Name, string.Format("{0}턴 동안 입히는 모든 피해가 0이 되고 이동할 수 없다.", param.Param1),
                    (modifier) => modifier.Owner.MovementAP = 999, param.Param1);

                cleanse.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                cleanse.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => -999,
                    (amount, source, sufferer) => DamageReplacement.DamageDealerBelongsToUnit(source, param.TargetUnit)));

                return cleanse;
            }

            public static UnitModifier GetCrouchModifier(CardEffectParameter param) // 웅크리기
            {

                UnitModifier crouching = new UnitModifier(param.Card.Name, param.Card.BaseCardData.Description,
                    UnitModifierEffectContainer.NoModifierEffect, 1);

                crouching.PassiveHandler.TurnStart.Add(TurnStartTrigger.GetModifierDurationTickTrigger(
                    (passiveOwner, turnOwner) => crouching.Owner.CurrentShield = 0));

                return crouching;
            }

            public static UnitModifier GetDodgeModifier(CardEffectParameter param)
            {
                UnitModifier dodge = new UnitModifier("회피", "다음 턴 시작까지 입혀질 사거리 2 이상의 카드로 인한 피해를 모두 0으로 만든다.",
                    UnitModifierEffectContainer.NoModifierEffect, 1);

                dodge.PassiveHandler.TurnStart.Add(TurnStartTrigger.GetModifierDurationTickTrigger());

                dodge.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => -999,
                    (amount, source, sufferer) => sufferer == param.Caster &&
                        source is Card && (source as Card).Range >= 2));

                return dodge;
            }


            public static UnitModifier GetEarthquakeModifier(CardEffectParameter param)
            {
                param.Caster.RemoveDuplicateModifier(param.Card.Name);

                UnitModifier earthquake = new UnitModifier(param.Card.Name, string.Format("{1}턴 동안 턴 종료시 방 안의 모든 적에게 피해를 {0} 준다.", param.Param1, param.Param2),
                    UnitModifierEffectContainer.NoModifierEffect, param.Param2);

                earthquake.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                earthquake.PassiveHandler.TurnEnd.Add(new TurnEndTrigger(
                    (passiveOwner, turnOwner) =>
                    {
                        foreach (Unit unit in GameManager.CurrentRoom.Units)
                            if (unit.UnitType == UnitType.Monster)
                                unit.TakeDamage(param.Param1, param.Card);
                    },
                    (passiveOwner, turnOwner) => GeneralVisualEffectContainer.NoVisualEffect(), TriggerHelper.SelfTurn));

                return earthquake;
            }

            public static UnitModifier GetEnrageModifier(CardEffectParameter param) // 광분
            {
                param.Caster.RemoveDuplicateModifier(param.Card.Name);

                UnitModifier enrage = new UnitModifier(param.Card.Name, String.Format("{1}턴 동안 카드 피해가 {0} 증가한다.", param.Param1, param.Param2),
                    UnitModifierEffectContainer.NoModifierEffect, param.Param2);

                enrage.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                enrage.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => amount + param.Param1,
                    (amount, source, sufferer) => DamageReplacement.DamageDealerBelongsToUnit(source, param.Caster)));

                return enrage;
            }

            public static UnitModifier GetFuryModifier(CardEffectParameter param)
            {
                param.Caster.RemoveDuplicateModifier(param.Card.Name);

                UnitModifier fury = new UnitModifier(param.Card.Name, string.Format("{1}턴 동안 이동 및 근접 계열 카드 사용에 필요한 행동력이 {0} 감소한다", -param.Param1, param.Param2),
                    (modifier) => modifier.Owner.MovementAP -= 1, param.Param2);

                fury.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                fury.PassiveHandler.CostReplacement.Add(new CostReplacement(
                    (amount, card) => amount + param.Param1,
                    (amount, card) => card.Owner == param.Caster && card.BaseCardData.CardClass == (CardClass) 1));

                return fury;
            }

            public static UnitModifier GetIncreaseAPModifier(CardEffectParameter param)
            {
                param.Caster.RemoveDuplicateModifier(param.Card.Name);

                UnitModifier increaseAP = new UnitModifier(param.Card.Name, String.Format("{1}턴 동안 최대 행동력이 {0} 증가한다.", param.Param1, param.Param2),
                    (modifier) => modifier.Owner.APPerTurn += param.Param1, param.Param2 + 1);
                increaseAP.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                return increaseAP;
            }
            
            public static UnitModifier GetIncreaseHandModifier(CardEffectParameter param)
            {
                UnitModifier increaseHand = new UnitModifier(param.Card.Name, String.Format("{1}턴 동안 손패 크기가 {0} 증가한다.", param.Param1, param.Param2),
                    (modifier) => modifier.Owner.HandSize += param.Param1, param.Param2 + 1);
                increaseHand.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                return increaseHand;
            }

            public static UnitModifier GetMultiplyAllDamageModifier(CardEffectParameter param)
            {
                param.Caster.RemoveDuplicateModifier(param.Card.Name);

                UnitModifier multiplyAll = new UnitModifier(param.Card.Name, param.Card.Description,
                    UnitModifierEffectContainer.NoModifierEffect, param.Param2);

                multiplyAll.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                multiplyAll.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => amount * param.Param1,
                    (amount, source, sufferer) => DamageReplacement.DamageDealerBelongsToUnit(source, param.Caster)));

                return multiplyAll;
            }

            public static UnitModifier GetReduceAllDamageFromModifier(CardEffectParameter param) // 압도
            {
                UnitModifier reduceAllDamageFrom = new UnitModifier(param.Card.Name, string.Format("{0}턴 동안 입히는 모든 피해가 0이 된다.", param.Param1),
                    UnitModifierEffectContainer.NoModifierEffect, param.Param1);

                reduceAllDamageFrom.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                reduceAllDamageFrom.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, source, sufferer) => -999,
                    (amount, source, sufferer) => source == param.TargetUnit || param.TargetUnit.Hand.Contains(source as Card)));

                return reduceAllDamageFrom;
            }

            public static UnitModifier GetReduceAPModifier(CardEffectParameter param)
            {
                param.Caster.RemoveDuplicateModifier(param.Card.Name);

                UnitModifier reduceAP = new UnitModifier(param.Card.Name, String.Format("{1}턴 동안 최대 행동력이 {0} 감소한다.", param.Param1, param.Param2),
                    (modifier) => modifier.Owner.APPerTurn -= param.Param1, param.Param2);
                reduceAP.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());
                return reduceAP;
            }

            public static UnitModifier GetReduceDamageToModifier(CardEffectParameter param)
            {
                UnitModifier reduceDamageTo = new UnitModifier(param.Card.Name, string.Format("{1}턴 동안 입는 피해가 {0}만큼 감소한다.", param.Param1, param.Param2),
                    UnitModifierEffectContainer.NoModifierEffect, param.Param2);

                reduceDamageTo.PassiveHandler.TurnStart.Add(TurnStartTrigger.GetModifierDurationTickTrigger());

                reduceDamageTo.PassiveHandler.DamageReplacement.Add(new DamageReplacement(
                    (amount, damageDealer, sufferer) => amount - 1,
                    (amount, damageDealer, sufferer) => sufferer == param.Caster));

                return reduceDamageTo;
            }

            public static UnitModifier GetSadismModifier(CardEffectParameter param) // 가학성
            {
                UnitModifier sadism = new UnitModifier(param.Card.Name, "이번 턴 동안 적이 죽을 때마다 내 체력을 1 회복시킨다.",
                    UnitModifierEffectContainer.NoModifierEffect, 1);

                sadism.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                sadism.PassiveHandler.UnitDeath.Add(new UnitDeathTrigger(
                    (passiveOwner, deadUnit) => sadism.Owner.GetHealed(1),
                    (passiveOwner, deadUnit) => GeneralVisualEffectContainer.NoVisualEffect(),
                    (passiveOwner, deadUnit) => sadism.Owner.UnitType.IsHostileAgainst(deadUnit.UnitType)));

                return sadism;
            }

            public static UnitModifier GetSpiritModifier(CardEffectParameter param)
            {
                param.Caster.RemoveDuplicateModifier(param.Card.Name);

                UnitModifier spirit = new UnitModifier(param.Card.Name, string.Format("{1}턴 동안 카드 사용 시 행동력을 {0} 얻는다.", param.Param1, param.Param2),
                    UnitModifierEffectContainer.NoModifierEffect, param.Param2);

                spirit.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                spirit.PassiveHandler.AfterCast.Add(new AfterCastTrigger(
                    (passiveOwner, cParam) => cParam.Caster.CurrentAP += param.Param1,
                    (passiveOwner, cParam) => GeneralVisualEffectContainer.NoVisualEffect(),
                    (passiveOwner, cParam) => cParam.Caster == param.Caster && cParam != param // 이 카드를 발동하면서 바로 행동력을 얻는 것을 방지
                    ));

                return spirit;
            }

            public static UnitModifier GetStunModifier(CardEffectParameter param)
            {
                UnitModifier stun = new UnitModifier("기절", string.Format("{0}턴 동안 최대 행동력이 0이 된다.", param.Param1),
                    (modifier) => modifier.Owner.APPerTurn = -999, param.Param1);

                stun.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());

                return stun;
            }

            public static UnitModifier GetTurnEndShieldModifier(CardEffectParameter param)
            {
                param.Caster.RemoveDuplicateModifier(param.Card.Name);

                UnitModifier turnShield = new UnitModifier(param.Card.Name, string.Format("{1}턴 동안 턴 종료 시 방어도를 {0} 얻는다.", param.Param1, param.Param2),
                    UnitModifierEffectContainer.NoModifierEffect, param.Param2);

                turnShield.PassiveHandler.TurnEnd.Add(TurnEndTrigger.GetModifierDurationTickTrigger());
                turnShield.PassiveHandler.TurnEnd.Add(new TurnEndTrigger(
                    (passiveOwner, turnOwner) => turnShield.Owner.GetShield(param.Param1),
                    (passiveOwner, turnOwner) => GeneralVisualEffectContainer.NoVisualEffect(), TriggerHelper.SelfTurn));

                return turnShield;
            }

            public static UnitModifier GetRottingModifier(int amount)
            {
                UnitModifier mod = new UnitModifier("썩어가는 살점", string.Format("매 턴 시작 시 {0}데미지를 입습니다.", amount), 
                    UnitModifierEffectContainer.NoModifierEffect);
                mod.PassiveHandler.TurnStart.Add(new TurnStartTrigger(
                    (passiveOwner, turnOwner) => ((UnitModifier)passiveOwner).Owner.CurrentRemainingHP -= amount,
                    (passiveOwner, turnOwner) => GeneralVisualEffectContainer.NoVisualEffect(),
                    TriggerHelper.SelfTurn
                    ));
                return mod;
            }
        }
    }
}
