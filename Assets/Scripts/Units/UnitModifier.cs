﻿using Wielder.Passive;

namespace Wielder.Units
{
    public class UnitModifier : IPassiveOwner
    {
        public UnitModifierEffect Effect;

        public string Name;
        public string Description;

        public int RemainingTurn;
        public int RemainingCast;
        public bool RemoveOnEndCombat;

        private Unit owner;
        public Unit Owner
        {
            get
            {
                return owner;
            }
            set
            {
                UnityEngine.Debug.Assert(owner == null);
                owner = value;
            }
        }

        public PassiveHandler PassiveHandler
        {
            get; set;
        }

        private IPassiveOwner source;
        public IPassiveOwner Source
        {
            get
            {
                return source;
            }
            set
            {
                UnityEngine.Debug.Assert(source == null);
                source = value;
            }
        }

        private UnitModifier(string name, string description, UnitModifierEffect effect,
            int remainingTurn = -1, bool removeOnEndCombat = true)
        {
            Name = name;
            Description = description;
            Effect = effect;
            RemainingTurn = remainingTurn;
            RemoveOnEndCombat = removeOnEndCombat;
            PassiveHandler = new PassiveHandler();
        }
        
        public UnitModifier(string name, string description, UnitModifierEffect effect)
            : this(name, description, effect, -1, true)
        { }

        public UnitModifier(string name, string description, UnitModifierEffect effect, int remainingTurn = -1)
            : this(name, description, effect, remainingTurn, true)
        { }

        public UnitModifier(string name, string description, UnitModifierEffect effect, bool removeOnEndCombat = true)
            : this(name, description, effect, -1, removeOnEndCombat)
        { }
    }
}
