﻿using System.Collections.Generic;
using UnityEngine;
using Wielder.Maps;

namespace Wielder.Path
{
    public static class PathManager
    {
        private static Node BFS(HexCoord start, HexCoord goal)
        {
            Room room = GameManager.CurrentRoom;
            Debug.Assert(start != goal && room.Cells.ContainsKey(start) && room.Cells.ContainsKey(goal));
            List<HexCoord> searched = new List<HexCoord>();
            Queue<Node> toSearch = new Queue<Node>();

            searched.Add(start);
            toSearch.Enqueue(new Node(room.Cells[start]));

            while (toSearch.Count != 0)
            {
                Node current = toSearch.Dequeue();
                if (current.Cell.Position == goal)
                    return current;

                foreach (HexCoord hex in current.Cell.Position.AdjacentList())
                {
                    if (!room.Cells.ContainsKey(hex) || (room.Cells[hex].Occupied && hex != goal) || searched.Contains(hex))
                        continue;

                    searched.Add(hex);
                    toSearch.Enqueue(new Node(room.Cells[hex], current));
                }
            }

            return null;
        }

        public static Stack<Node> GetPath(Cell start, Cell goal)
        {
            Node result = BFS(start.Position, goal.Position);
            if (result == null)
                result = BestApproach(start, goal);

            return NodeToStack(result);
        }

        private static Stack<Node> NodeToStack(Node node)
        {
            if (node == null)
                return null;

            Stack<Node> path = new Stack<Node>();
            do
            {
                path.Push(node);
                node = node.Parent;
            }
            while (node.Parent != null);

            return path;
        }
        
        private static Node BestApproach(Cell start, Cell goal) // 완벽한 경로가 없는 상황일 때 그나마 가까운 자리를 향해 이동
        {
            int maxDistance = start.GetDistanceFrom(goal);
            int distance = 1;
            Dictionary<HexCoord, Cell> allCells = GameManager.CurrentRoom.Cells;
            while (distance < maxDistance)
            {
                List<Cell> toSearch = new List<Cell>();

                foreach (Cell cell in allCells.Values)
                {
                    if (cell.GetDistanceFrom(goal) == distance && !cell.Occupied)
                    {
                        toSearch.Add(cell);
                    }
                }

                toSearch.Sort(delegate (Cell lhs, Cell rhs)
                {
                    int l = lhs.GetDistanceFrom(start);
                    int r = rhs.GetDistanceFrom(start);
                    if (l == r) return 0;
                    else if (l < r) return -1;
                    else return 1;
                });
                
                foreach (Cell cell in toSearch)
                {
                    Node result = BFS(start.Position, cell.Position);
                    if (result != null)
                        return result;
                }

                distance++;
            }

            return null;
        }
    }
}
