﻿using Wielder.Maps;

namespace Wielder.Path
{
    public class Node
    {
        public Node Parent;
        public Cell Cell;

        public Node(Cell cell, Node parent = null)
        {
            this.Cell = cell;
            this.Parent = parent;
        }
    }
}
