﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Wielder
{
    public class TestScript : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static TestScript instance;

        public static TestScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private void Start()
        {
            Debug.Assert(instance == null);
            instance = this;

            if(GameStarter.Instance.DebugMode)
            {
                GameManager.StartDebugMode();
            }
            else
            {
                //GameManager.StartGame("cheaterplayer");
                GameManager.StartGame("newbie");
                //GameManager.StartGame("welldonwarrior");
            }
            SceneManager.UnloadSceneAsync("Main Menu");
            //GameManager.StartGame(playername);
            //GameManager.Player.GetMoney(StartMoney);
            //VisualLogicManager.ShowRoom(GameManager.CurrentStage.GetCurrentRoom());  
        }
    }
}
