﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Wielder.Visual;
using Wielder.Maps;

namespace Wielder
{
    public enum PopupState
    {
        None,
        Shop,
        Inventory,
        Chest,
        Banner,
    }
    public class PopupUIScript : MonoBehaviour
    {
        private static PopupUIScript instance;

        public static PopupUIScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private ItemScript selectedItem;
        private UIState previousUIState;

        public PopupState CurrentState { get; private set; }
        private void Awake()
        {
            transform.localScale = new Vector3(1, 1);
            Debug.Assert(instance == null);
            instance = this;
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                if (ShopInteractionScript.Instance.CurrentlyShown)
                {
                    HideShop();
                }
                if (InventoryScript.Instance.CurrentlyShown)
                {
                    HideInventory();
                }
                if (ChestInteractionScript.Instance.CurrentlyShown)
                {
                    HideChest();
                }
            }
            if (Input.GetKeyUp(KeyCode.I))
            {
                if (CurrentState == PopupState.None && !InventoryScript.Instance.CurrentlyShown) ShowInventory();
                else if (CurrentState == PopupState.Inventory && InventoryScript.Instance.CurrentlyShown) HideInventory();
            }
            if (CurrentState == PopupState.Banner && Input.anyKeyDown)
            {
                HideBanner();
                GameManager.EndGame();
                StartCoroutine(LoadMainMenu());
            }
        }

        private void CellColliderActive(bool condition)
        {
            foreach (Cell cell in GameManager.CurrentRoom.Cells.Values)
            {
                cell.Indicator.GetComponent<PolygonCollider2D>().enabled = condition;
            }
        }

        public void ShowShop()
        {
            CellColliderActive(false);
            CurrentState = PopupState.Shop;
            previousUIState = UIManagerScript.Instance.GetUIState();
            UIManagerScript.Instance.SetUIState(UIState.Disabled);
            ShopInteractionScript.Instance.Show();
        }

        public void HideShop()
        {
            CellColliderActive(true);
            UIManagerScript.Instance.SetUIState(previousUIState);
            CurrentState = PopupState.None;
            ShopInteractionScript.Instance.Hide();
        }

        public void ShowInventory()
        {
            
            CellColliderActive(false);
            previousUIState = UIManagerScript.Instance.GetUIState();
            UIManagerScript.Instance.SetUIState(UIState.Disabled);
            CurrentState = PopupState.Inventory;
            InventoryScript.Instance.Show();
        }

        public void HideInventory()
        {
            CellColliderActive(true);
            UIManagerScript.Instance.SetUIState(previousUIState);
            CurrentState = PopupState.None;
            InventoryScript.Instance.Hide();
        }

        public void ShowChest()
        {
            CellColliderActive(false);
            CurrentState = PopupState.Chest;
            previousUIState = UIManagerScript.Instance.GetUIState();
            UIManagerScript.Instance.SetUIState(UIState.Disabled);
            ChestInteractionScript.Instance.Show();
        }

        public void HideChest()
        {
            CellColliderActive(true);
            UIManagerScript.Instance.SetUIState(previousUIState);
            CurrentState = PopupState.None;
            ChestInteractionScript.Instance.Hide();
        }

        public void ShowBanner()
        {
            CellColliderActive(false);
            previousUIState = UIManagerScript.Instance.GetUIState();
            CurrentState = PopupState.Banner;
            BannerScript.Instance.Show();
        }

        public void HideBanner()
        {
            CellColliderActive(true);
            CurrentState = PopupState.None;
            BannerScript.Instance.Hide();
        }


        public PopupState GetState()
        {
            return CurrentState;
        }

        private IEnumerator LoadMainMenu()
        {
            AsyncOperation task = SceneManager.LoadSceneAsync("Main Menu");
            while (!task.isDone) yield return null;
        }
    }
}
