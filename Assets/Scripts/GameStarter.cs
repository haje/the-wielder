﻿using UnityEngine;

namespace Wielder
{
    class GameStarter : MonoBehaviour
    {
        private static GameStarter instance;

        public static GameStarter Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        public bool DebugMode;

        [SerializeField]
        public GameObject EventSystem;

        private void Awake()
        {
            EventSystem.SetActive(true);
            instance = this;
        }

        private void Start()
        {
            BGMManagerScript.Instance.PlayBGM(BGMManagerScript.MainMenuBGMname);
        }
    }
}
