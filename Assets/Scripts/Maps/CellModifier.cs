﻿using Wielder.NonUnits;
using Wielder.Passive;
using Wielder.Passive.Triggers;

namespace Wielder.Maps
{
    public class CellModifier : IPassiveOwner
    {
        public PassiveHandler PassiveHandler
        {
            get
            {
                return passiveHandler;
            }
        }

        public string Name;
        public string Description;
        public int RemainingTurn;

        public IDamageDealer DealerSource;
        
        private PassiveHandler passiveHandler;
        private NonUnit owner;
        public NonUnit Owner
        {
            get
            {
                return owner;
            }
            set
            {
                UnityEngine.Debug.Assert(owner == null);
                owner = value;
            }
        }

        public CellModifier(string name, string description, int remainingTurn = -1)
        {
            Name = name;
            Description = description;
            RemainingTurn = remainingTurn;
            passiveHandler = new PassiveHandler();
        }
    }
}
