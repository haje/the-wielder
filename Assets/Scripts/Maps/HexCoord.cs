﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Wielder.Maps
{
    // Coordinate System
    //    1,0   3,0
    // 0,0   2,0   4,0 ...
    //    1,1   3,1
    // 0,1   2,1   4,1 ...
    //    1,2   3,2
    //       ...

    public struct HexCoord
    {
        public int X, Y;
        public HexCoord(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            if (obj is HexCoord)
                return (HexCoord)obj == this;
            else return false;
        }

        public static bool operator ==(HexCoord a, HexCoord b)
        {
            return (a.X == b.X) && (a.Y == b.Y);
        }

        public static bool operator !=(HexCoord a, HexCoord b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }

        public override string ToString()
        {
            return X.ToString() + ", " + Y.ToString();
        }

        public List<HexCoord> AdjacentList()
        {
            List<HexCoord> result = new List<HexCoord>();

            result.Add(new HexCoord(X, Y - 1));
            result.Add(new HexCoord(X, Y + 1));

            int bias = Y;
            if (X % 2 != 0)
                bias--;

            result.Add(new HexCoord(X - 1, bias));
            result.Add(new HexCoord(X + 1, bias));
            result.Add(new HexCoord(X - 1, bias + 1));
            result.Add(new HexCoord(X + 1, bias + 1));

            return result;
        }

        /// <summary>
        /// 현재 HexCoord에서 특정 방향으로 이동하면 나오는 HexCoord를 반환한다.
        /// </summary>
        /// <param name="dir">이동 방향</param>
        /// <returns>이동 후의 좌표</returns>
        public HexCoord Translate(Direction dir)
        {
            int bias = Y;
            if (X % 2 != 0)
                bias--;
            switch (dir)
            {
                case Direction.UPPER_LEFT:
                    return new HexCoord(X - 1, bias);
                case Direction.UPPER_RIGHT:
                    return new HexCoord(X + 1, bias);
                case Direction.UP:
                    return new HexCoord(X, Y - 1);
                case Direction.LOWER_LEFT:
                    return new HexCoord(X - 1, bias + 1);
                case Direction.LOWER_RIGHT:
                    return new HexCoord(X + 1, bias + 1);
                case Direction.DOWN:
                    return new HexCoord(X, Y + 1);
                default://cannot reach
                    return new HexCoord();
            }
        }


        /// <summary>
        /// 현재 좌표에서 어느 방향으로 이동해야 해당 좌표가 나오는지를 구한다.
        /// 반드시 AdjacentQueue의 원소여야 한다.
        /// </summary>
        /// <param name="hex">방향을 구하고 싶은 좌표</param>
        /// <returns>방향</returns>
        public Direction GetDirection(HexCoord hex)
        {
            Debug.Assert(AdjacentList().Contains(hex));
            foreach (Direction dir in Enum.GetValues(typeof(Direction)))
            {
                if (Translate(dir) == hex) return dir;
            }
            return 0;
        }

        // Non-orth Coordinate System
        //    1,0   3,1
        // 0,0   2,1   4,2 ...
        //    1,1   3,2
        // 0,1   2,2   4,3 ...
        //    1,2   3,3
        //       ...

        private static HexCoord ConvertToNonorthogonalCoords(HexCoord coord)
        {
            return new HexCoord(coord.X, coord.Y + (coord.X < 0 ? coord.X - 1 : coord.X) / 2);
        }

        private static HexCoord ConvertToOrthogonalCoords(HexCoord coord)
        {
            return new HexCoord(coord.X, coord.Y - (coord.X < 0 ? coord.X - 1 : coord.X) / 2);
        }

        public static int HexCoordDistance(HexCoord a, HexCoord b)
        {
            HexCoord nonorth_a = ConvertToNonorthogonalCoords(a);
            HexCoord nonorth_b = ConvertToNonorthogonalCoords(b);

            return Mathf.Max(Mathf.Abs(nonorth_a.X - nonorth_b.X), Mathf.Abs(nonorth_a.Y - nonorth_b.Y),
                Mathf.Abs(nonorth_a.X - nonorth_a.Y - nonorth_b.X + nonorth_b.Y));
        }

        static readonly float sin60 = Mathf.Sin(2 * Mathf.PI * 60 / 360);

        public Vector2 ConvertToWorldPosition(float scale, Vector2 start)
        {
            HexCoord nonorth = ConvertToNonorthogonalCoords(this);
            return start + new Vector2(sin60 * nonorth.X, -nonorth.Y + nonorth.X * 0.5f) * scale;
        }

        //         3, 1
        //     2, 1    4, 2
        // 1, 1    3, 2    5, 3
        //     2, 2    4, 3
        // 1, 2    3, 3    5, 4
        //     2, 3    4, 4
        // 1, 3    3, 4    5, 5
        //     2, 4    4, 5
        //         3, 5
        public static HexCoord GetClockwise(HexCoord axis, HexCoord from) // axis가 3, 3이고 from이 4, 2이면 5, 3을 반환
        {
            int distance = HexCoordDistance(axis, from);
            Debug.Assert(distance > 0);

            axis = ConvertToNonorthogonalCoords(axis);
            from = ConvertToNonorthogonalCoords(from);
            int x = from.X;
            int y = from.Y;
            if (x < axis.X && y == axis.Y - distance) // 왼쪽 위에서 회전
                x++;
            else if (x == axis.X + distance && y < axis.Y + distance) // 오른쪽 위에서 회전
                y++;
            else if (x > axis.X && y == axis.Y + distance) // 오른쪽 아래에서 회전
                x--;
            else if (x == axis.X - distance && y > axis.Y - distance) // 왼쪽 아래에서 회전
                y--;
            else if (x < axis.X + distance && y < axis.Y) // 위에서 회전
            {
                x++;
                y++;
            }
            else if (x > axis.X - distance && y > axis.Y) // 아래에서 회전
            {
                x--;
                y--;
            }
            else
                Debug.Log("코드 잘못 짬");

            return ConvertToOrthogonalCoords(new HexCoord(x, y));
        }

        public static HexCoord GetCounterClockwise(HexCoord axis, HexCoord from)
        {
            int distance = HexCoordDistance(axis, from);
            Debug.Assert(distance > 0);

            axis = ConvertToNonorthogonalCoords(axis);
            from = ConvertToNonorthogonalCoords(from);
            int x = from.X;
            int y = from.Y;
            if (x == axis.X - distance && y < axis.Y) // 왼쪽 위에서 회전
                y++;
            else if (x > axis.X - distance && y == axis.Y - distance) // 위에서 회전
                x--;
            else if (x == axis.X + distance && y > axis.Y) // 오른쪽 아래에서 회전
                y--;
            else if (x < axis.X + distance && y == axis.Y + distance) // 아래에서 회전
                x++;
            else if (x > axis.X && y > axis.Y - distance) // 오른쪽 위에서 회전
            {
                x--;
                y--;
            }
            else if (x < axis.X && y < axis.Y + distance) // 왼쪽 아래에서 회전
            {
                x++;
                y++;
            }
            else
                Debug.Log("코드 잘못 짬");

            return ConvertToOrthogonalCoords(new HexCoord(x, y));
        }
    }
}
