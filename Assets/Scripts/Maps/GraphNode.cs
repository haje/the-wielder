﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Wielder.Maps
{
    public class GraphNode
    {
        public HexCoord Pos { get; private set; }
        public Room Room { get; set; }
        public List<GraphNode> AdjacentNode { get; private set; }
        public List<HexCoord> AllowedCoord { get; private set; }
        public bool Visited;
        public bool Discovered;
        public Visual.MinimapNodeScript Indicator;
        private Dictionary<HexCoord, GraphNode> nodeSet;
        private GraphNode rootNode;

        /// <summary>
        /// Root Node 생성
        /// </summary>
        public GraphNode()
        {
            Pos = new HexCoord(0, 0);
            AdjacentNode = new List<GraphNode>();
            AllowedCoord = Pos.AdjacentList();
            nodeSet = new Dictionary<HexCoord, GraphNode> { { Pos, this } };
            rootNode = this;
        }

        /// <summary>
        /// 새 노드 생성. Root Node와 AdjacentNode, AllowedCoord를 자동 계산한다.
        /// 반드시 Parent의 AllowedCoord 중 하나를 불러야 한다.
        /// </summary>
        /// <param name="pos">Parent의 AllowedCoordinate의 원소</param>
        /// <param name="from">Parent</param>
        public GraphNode(HexCoord pos, GraphNode from)
        {
            Debug.Assert(from.AllowedCoord.Contains(pos));
            Pos = pos;
            rootNode = from.rootNode;

            AdjacentNode = new List<GraphNode>();
            foreach (HexCoord hex in pos.AdjacentList())
            {
                if (rootNode.nodeSet.ContainsKey(hex))
                {
                    GraphNode n = rootNode.nodeSet[hex];
                    n.AdjacentNode.Add(this);
                    AdjacentNode.Add(n);
                }
            }
            AllowedCoord = pos.AdjacentList();
            CalculateAllowedCoord();
            foreach (GraphNode adjNode in AdjacentNode)
            {
                adjNode.CalculateAllowedCoord();
            }
            rootNode.nodeSet.Add(pos, this);
        }

        /// <summary>
        /// Node가 업데이트 되었을때 부르는 함수. Adjacent node를 분석하여 Allowed Coordinate를 계산한다.
        /// </summary>
        private void CalculateAllowedCoord()
        {
            //동시에 2개 이상의 인접한 Node가 존재하면 해당 좌표는 AllowedCoordinate가 아니다
            List<HexCoord> removeList = new List<HexCoord>();
            foreach (GraphNode adj in AdjacentNode)
            {
                if (AllowedCoord.Contains(adj.Pos))
                {
                    AllowedCoord.Remove(adj.Pos);
                }
                List<HexCoord> adjOfAdj = adj.Pos.AdjacentList();

                foreach (HexCoord hex in AllowedCoord)
                {
                    if (removeList.Contains(hex)) continue;
                    if (adjOfAdj.Contains(hex))
                    {
                        removeList.Add(hex);
                    }
                }
            }
            AllowedCoord.RemoveAll(hex => removeList.Contains(hex));
        }

        public List<HexCoord> ToHexCoordList()
        {
            return rootNode.nodeSet.Keys.ToList();
        }

        /// <summary>
        /// 해당 좌표가 Map에서 순환을 만드는지 검사
        /// </summary>
        /// <param name="target">테스트할 좌표</param>
        /// <returns></returns>
        public bool WillGenerateCycle(HexCoord target)
        {
            //조건1: 현재 이미 존재하는 Node가 있으면 cycle 테스트가 무의미
            if (rootNode.nodeSet.ContainsKey(target))
            {
                return false;
            }
            //조건2: Allowed Coordinate에 없으면 Cycle - triangle을 만듬
            //조건3: adjacent한 Node가 2개 이상이면 cycle
            int adjacentCount = 0;
            foreach (HexCoord adj in target.AdjacentList())
            {
                if (rootNode.nodeSet.ContainsKey(adj))
                {
                    adjacentCount += 1;
                    if (!rootNode.nodeSet[adj].AllowedCoord.Contains(target))
                        return true;
                }
            }
            return adjacentCount >= 2;
        }

        public List<GraphNode> ToList()
        {
            return rootNode.nodeSet.Values.ToList();
        }

        public List<GraphNode> GetEndRooms()
        {
            List<GraphNode> endroom = new List<GraphNode>();
            foreach (GraphNode n in ToList())
            {
                if (n.AdjacentNode.Count == 1) endroom.Add(n);
            }
            return endroom;
        }

        public static int GetDistance(GraphNode g1, GraphNode g2)
        {
            Debug.Assert(g1 != null);
            Debug.Assert(g2 != null);
            Debug.Assert(g1.rootNode == g2.rootNode);
            Dictionary<GraphNode, bool> check = new Dictionary<GraphNode, bool>();
            foreach (GraphNode node in g1.rootNode.nodeSet.Values)
            {
                check.Add(node, false);
            }
            Queue<GraphNode> nextSearch = new Queue<GraphNode>();
            check.Add(g1, true);
            bool found = false;
            int distance = 0;
            GraphNode next = g1;
            while (!found)
            {
                if (next.Pos == g2.Pos) found = true;
                foreach (GraphNode node in next.AdjacentNode)
                {
                    if (!check[node])
                        nextSearch.Enqueue(node);
                }
                next = nextSearch.Dequeue();
            }
            return distance;
        }

        public static GraphNode GetRandomNode(GraphNode node)
        {
            return node.rootNode.nodeSet.Values.ToList()[UnityEngine.Random.Range(0, node.rootNode.nodeSet.Count)];
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object o)
        {
            if (o is GraphNode)
            {
                return (GraphNode)o == this;
            }
            return false;
        }

        public static bool operator ==(GraphNode g1, GraphNode g2)
        {
            bool firstNull = ReferenceEquals(g1, null);
            bool secondNull = ReferenceEquals(g2, null);

            if (firstNull != secondNull)
                return false;

            if (firstNull && secondNull)
                return true;

            return g1.Pos == g2.Pos && ReferenceEquals(g1.rootNode, g2.rootNode);
        }

        public static bool operator !=(GraphNode g1, GraphNode g2)
        {
            return !(g1 == g2);
        }

        // 이거 무슨 의도로 만든 건지 잘 모르겠는데 위험하게 생겨서 주석처리했습니다 - 정호
        /*
        public GraphNode this[HexCoord hex]
        {
            get
            {
                return RootNode.NodeSet.ContainsKey(hex) ? RootNode.NodeSet[hex] : null;
            }
        }
        */

    };
}
