﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wielder.Visual;

namespace Wielder.Maps
{
    public class Map
    {
        private GraphNode currentRoom;
        private Dictionary<Direction, GraphNode> adjacentRooms;
        public GraphNode Node { get; private set; }
        public int CurrentLevel { get; private set; }

        public Map()
        {
            CurrentLevel = 1;
            Node = MapGenerator.GenerateEmptyMap();
            adjacentRooms = new Dictionary<Direction, GraphNode>();
            currentRoom = Node;
            currentRoom.Visited = currentRoom.Discovered = true;
            CalculateAdjacentRooms();
        }

        public Map(int level)
        {
            CurrentLevel = level;
            Node = MapGenerator.GenerateLevel(15, 15, level, 2);
            adjacentRooms = new Dictionary<Direction, GraphNode>();
            currentRoom = Node;
            currentRoom.Visited = currentRoom.Discovered = true;
            CalculateAdjacentRooms();
        }

        

        public Room GetCurrentRoom()
        {
            return currentRoom.Room;
        }

        /// <summary>
        /// 현재 방에서 갈 수 있는 방의 방향의 리스트를 얻는다
        /// </summary>
        /// <returns>방향의 리스트</returns>
        public List<Direction> NextRoomDirections()
        {
            return adjacentRooms.Keys.ToList();
        }

        /// <summary>
        /// Direction방향으로 방 이동함. 반드시 NextRoomDirections()를 먼저 부르고 그 리스트 안에 있는 방향이어야 함
        /// </summary>
        /// <param name="direction">이동할 방의 방향</param>
        public void MoveToRoom(Direction direction)
        {
            Debug.Assert(NextRoomDirections().Contains(direction));
            currentRoom = adjacentRooms[direction];
            currentRoom.Visited = currentRoom.Discovered = true;
            CalculateAdjacentRooms();
        }
        /// <summary>
        /// Minimap을 위한 방문 구역 탐색기
        /// </summary>
        /// <returns></returns>
        public List<GraphNode> GetVisitedRooms()
        {
            List<GraphNode> visited = Node.ToList();
            visited.RemoveAll(node => !node.Visited);
            return visited;
        }

        /// <summary>
        /// 새 방으로 이동했을 때 인접한 방을 Dictionary에 등록한다
        /// </summary>
        private void CalculateAdjacentRooms()
        {
            adjacentRooms.Clear();
            foreach(GraphNode node in currentRoom.AdjacentNode)
            {
                adjacentRooms.Add(currentRoom.Pos.GetDirection(node.Pos), node);
                node.Discovered = true;
            }

            MinimapContainerScript.Instance.Refresh();
        }
        /// <summary>
        /// 현재 방의 좌표를 얻어옴
        /// </summary>
        /// <returns></returns>
        public HexCoord GetCurrentRoomLocation()
        {
            return currentRoom.Pos;
        }
    }
}
