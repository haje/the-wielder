﻿using System;

namespace Wielder.Maps
{
    [Flags]
    public enum CellConditions
    {
        NoCondition = 0,
        EmptyCell = 0x1,
        PlayerPresent = 0x2,
        MonsterPresent = 0x4,
        AllyOfCasterPresent = 0x8,
        EnemyOfCasterPresent = 0x10,
        PassableTerrain = 0x20,
        UnitPresent = 0x40,
        Custom = 0x200,
    }

    public static class CellConditionsHelper
    {
        public static bool HasFlag(this CellConditions cellConditions, CellConditions flagToCheck)
        {
            return (cellConditions & flagToCheck) != 0;
        }
    }
}
