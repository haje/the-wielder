﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using Wielder.NonUnits;
using Wielder.Units;

namespace Wielder.Maps
{
    public class Cell
    {
        public readonly HexCoord Position;
        public readonly Room ContainingRoom;
        public List<NonUnit> PlacedNonUnits;
        public Unit PlacedUnit;
        public Visual.CellScript Indicator;
        public bool Occupied
        {
            get
            {
                return PlacedUnit != null || PlacedNonUnits.Any(obj => obj.OccupiesCell);
            }
        }

        public Cell(Room contaitingRoom, HexCoord position)
        {
            ContainingRoom = contaitingRoom;
            Position = position;
            PlacedNonUnits = new List<NonUnit>();
        }

        public int GetDistanceFrom(Cell other)
        {
            Debug.Assert(other.ContainingRoom == ContainingRoom);
            return HexCoord.HexCoordDistance(Position, other.Position);
        }
    }
}
