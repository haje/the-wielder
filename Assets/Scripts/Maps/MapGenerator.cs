﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Wielder.Maps
{
    public static class MapGenerator
    {
        private static Dictionary<int, int> difficulties;

        public static void Init()
        {
            difficulties = new Dictionary<int, int>();
            for (int i = 1; i < 10; i++)
            {
                RoomBase test;
                int j = 1;
                while(true)
                {
                    try
                    {
                        test = new RoomBase(string.Format("room{0}-{1}", i, j++));
                    }
                    catch(FileNotFoundException)
                    {
                        if(--j != 0)
                            difficulties.Add(i, j);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Map Generator 메인 함수. Size만큼의 Map을
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static GraphNode GenerateLevel(int battleRoomCount, int emptyRoomCount, int baseDifficulty, int difficultyRange)
        {
            int size = battleRoomCount + emptyRoomCount + 2;
            if (difficulties == null) Init();
            
            GraphNode init = new GraphNode();
            size--;
            float randomVal = Random.Range(0f, 1f);
            int i = size * 3 / 4;
            //맵의 기본 뼈대, 스타팅은 랜덤으로 
            if (randomVal < 0.33f || size < 3)
            {
                //단방향

                AddLongRoad(init, i);
            }
            else if (randomVal < 0.66f || size < 4)
            {
                //양방향
                for (int x = 0; x < 2; x++)
                {
                    GraphNode n = new GraphNode(init.AllowedCoord[Random.Range(0, init.AllowedCoord.Count)], init);

                }
                size -= 2;
                foreach (GraphNode node in init.AdjacentNode)
                {
                    AddLongRoad(node, i / 2);
                }
            }
            else
            {
                //3방향
                GraphNode node1 = new GraphNode(init.AllowedCoord[Random.Range(0, init.AllowedCoord.Count)], init);
                AddSelection(init);
                Debug.Assert(init.AdjacentNode.Count == 3);
                foreach (GraphNode node in init.AdjacentNode)
                {
                    AddLongRoad(node, i / 3);
                }
            }
            size -= i;

            //나머지 빈 공간 채움. endroom이 최소 두 개는 필요하다(상점, 보스방)

            while (size > 0 || init.GetEndRooms().Count < 4)
            {
                GraphNode randomNode = GraphNode.GetRandomNode(init);
                //map이 커지면 굉장히 느려질 여지가 있음. TODO
                if (randomNode.AllowedCoord.Count > 0)
                {
                    GraphNode newNode = new GraphNode(randomNode.AllowedCoord[Random.Range(0, randomNode.AllowedCoord.Count)], randomNode);
                    size--;
                }
            }

            List<GraphNode> emptyRooms = init.ToList();

            Debug.Assert(emptyRooms[0] == init);
            List<GraphNode> endRooms = init.GetEndRooms();
            List<GraphNode> emptyNonEndRooms = init.ToList();
            List<GraphNode> emptyEndRooms = init.GetEndRooms();
            emptyNonEndRooms.RemoveAll(room => endRooms.Contains(room));
            if (emptyNonEndRooms.Contains(init))
                emptyNonEndRooms.Remove(init);
            emptyRooms.Remove(init);
            //전투방 배치 - 일단 5개
            int battlerooms = 0;
            int index;
            //preprocess: non-end room부터 채운다
            int diff;
            if (battleRoomCount > 0)
            {
                if (emptyNonEndRooms.Count < battleRoomCount)
                {

                    foreach (GraphNode n in emptyNonEndRooms)
                    {
                        diff = baseDifficulty + Random.Range(0, difficultyRange);
                        n.Room = GenerateBattleRoom(diff);
                        battlerooms++;
                        emptyRooms.Remove(n);
                    }
                    emptyNonEndRooms.Clear();
                }
                else
                {
                    index = Random.Range(1, emptyNonEndRooms.Count);
                    GraphNode node = emptyNonEndRooms[index];
                    diff = baseDifficulty + Random.Range(0, difficultyRange);
                    node.Room = GenerateBattleRoom(diff);
                    emptyNonEndRooms.Remove(node);
                    emptyRooms.Remove(node);
                    battlerooms++;
                }
            }
            //남은 방 중에서 전투방을 채운다
            while (battlerooms < battleRoomCount)
            {
                
                index = Random.Range(0, emptyRooms.Count);
                GraphNode node = emptyRooms[index];
                if (node.Pos == new HexCoord(0, 0)) continue;
                if (endRooms.Contains(node) && emptyRooms.Count > endRooms.Count) continue;

                diff = baseDifficulty + Random.Range(0, difficultyRange);
                node.Room = GenerateBattleRoom(diff);
                emptyRooms.Remove(node);
                emptyEndRooms.Remove(node);
                battlerooms++;
            }

            //상점 배치 - endroom에만 -> 시작방에 상점 배치니까 거른다
            /*
            if (endRooms.Count != 0)
            {
                index = Random.Range(0, emptyEndRooms.Count);
                GraphNode node = emptyEndRooms[index];
                node.Room = new Room("testshop");
                emptyRooms.Remove(node);
                emptyEndRooms.Remove(node);
            }
            */
            //보스방 배치 - endroom에만
            if (endRooms.Count != 0)
            {
                index = Random.Range(0, emptyEndRooms.Count);
                GraphNode node = emptyEndRooms[index];

                node.Room = GenerateBossRoom(baseDifficulty); //temp code
                emptyRooms.Remove(node);
                emptyEndRooms.Remove(node);
            }
            //나머지를 빈 방으로 채움
            foreach (GraphNode node in emptyRooms)
            {
                node.Room = new Room("emptyroom");
            }
            if (GameManager.CurrentStage == null)
                init.Room = new Room("shop1");
            else
                init.Room = new Room(string.Format("shop{0}", GameManager.CurrentStage.CurrentLevel + 1));
            
            return init;
        }

        public static GraphNode GenerateEmptyMap()
        {
            GraphNode node = new GraphNode();
            node.Room = new Room("debugroom");
            return node;
        }

        /// <summary>
        /// size만큼의 하나의 긴 길을 만든다. 시작 지점은 랜덤으로 선택. 중간에 cycle이 생기게 되면 멈추고 길이만큼을 반환한다.
        /// </summary>
        /// <param name="map">해당 node에서 시작함</param>
        /// <param name="maxSize">길의 최대 크기</param>
        /// <returns>실제 생성한 길의 길이</returns>
        private static int AddLongRoad(GraphNode map, int maxSize)
        {
            GraphNode add = map;
            int count = 0;
            Stack<GraphNode> track = new Stack<GraphNode>();
            track.Push(add);
            while (maxSize > 0)
            {

                if (add.AllowedCoord.Count > 0)
                {
                    List<HexCoord> nonCycleCoord = new List<HexCoord>();
                    foreach (HexCoord hex in add.AllowedCoord)
                    {
                        if (!map.WillGenerateCycle(hex))
                            nonCycleCoord.Add(hex);
                    }
                    if (nonCycleCoord.Count == 0)
                    {
                        //track back
                        add = track.Pop();
                        continue;
                    }
                    track.Push(add);
                    add = new GraphNode(nonCycleCoord[Random.Range(0, nonCycleCoord.Count)], add);
                    maxSize--;
                    count++;
                }
                else
                {
                    add = track.Pop();
                }
            }
            return count;
        }

        /// <summary>
        /// 두갈래길을 생성함. Root node를 제외하면 필연적으로 3방향이 됨
        /// </summary>
        /// <param name="map">해당 위치에서 시작</param>
        /// <returns>갈림길 생성 성공 여부</returns>
        private static bool AddSelection(GraphNode map)
        {

            if (map.AdjacentNode.Count != 1) return false;
            HexCoord adjPos = map.AdjacentNode[0].Pos;
            List<HexCoord> adj2 = adjPos.AdjacentList();
            List<HexCoord> add = new List<HexCoord>(2);
            foreach (HexCoord adj in map.AllowedCoord)
            {
                //adjacent node의 adjacent coord와 딱 한개 겹쳐야 함
                int count = 0;
                foreach (HexCoord a3 in adj.AdjacentList())
                {
                    if (a3 == map.Pos) continue;
                    if (adj2.Contains(a3)) count++;
                }
                if (count == 1)
                {
                    add.Add(adj);
                }
            }
            foreach (HexCoord hex in add)
            {
                GraphNode newNode = new GraphNode(hex, map);
            }
            return true;
        }


        private static Room GenerateBattleRoom(int difficulty)
        {
            difficulty = Mathf.Min(3, difficulty);
            difficulty = Mathf.Max(1, difficulty);
            difficulty += Random.Range(0, 1) * 3;
            //return new Room("testroom");
            Room room = new Room(string.Format("room{0}-{1}", difficulty, Random.Range(1, difficulties[difficulty])));
            room.Type = RoomType.BattleRoom;
            return room;
        }

        private static Room GenerateBossRoom(int difficulty)
        {
            Room room = new Room(string.Format("boss{0}", difficulty));
            room.Type = RoomType.BossRoom;
            return room;
        }

        private static Room GenerateNextStageStairRoom()
        {
            Room room = new Room("emptyroom");
            room.Type = RoomType.StartRoom;
            return room;
        }
    }
}
