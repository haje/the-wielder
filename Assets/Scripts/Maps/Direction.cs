﻿namespace Wielder.Maps
{
    public enum Direction
    {
        //counter-clockwise rotation
        UP=0,
        UPPER_LEFT=60,
        LOWER_LEFT =120,
        DOWN=180,
        LOWER_RIGHT=240,
        UPPER_RIGHT =300,
    }
    public static class DirectionUtil
    {
        public static Direction GetOppositeDirection(Direction dir)
        {
            return (Direction)(((int)dir + 180) % 360);
        }

    }
}
