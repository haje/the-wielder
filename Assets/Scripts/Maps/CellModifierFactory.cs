﻿using Wielder.Cards;
using Wielder.Passive.Triggers;
using Wielder.Visual;

namespace Wielder.Maps
{
    public static class CellModifierFactory
    {
        public static CellModifier GetFloorCasterTurnStartDamageModifier(CardEffectParameter param) // Param2 턴 동안 유지되는 장판 위에 있는 유닛은 시전자가 턴을 시작하면 Param1만큼 피해를 입는다
        {
            CellModifier floorDamage = new CellModifier("이름인데 지금 안쓰임", "설명인데 지금 안쓰임", param.Param2);
            floorDamage.DealerSource = param.Card;
            floorDamage.PassiveHandler.TurnStart.Add(TurnStartTrigger.GetCellModifierDurationTickTrigger()); // 이걸 설치한 유닛의 턴이 시작할 때 남은 턴 감소
            floorDamage.PassiveHandler.TurnStart.Add(new TurnStartTrigger(
                (passiveOwner, turnOwner) => 
                {
                    if (param.TargetCell.PlacedUnit != null) // Chain을 실행할 땐 이미 죽어서 없을 수 있으니 한번 더 체크
                        param.TargetCell.PlacedUnit.TakeDamage(param.Param1, param.Card);
                },
                (passiveOwner, turnOwner) => GeneralVisualEffectContainer.GenerateFire(param.TargetCell.Indicator.transform.position),
                (passiveOwner, turnOwner) => param.Caster == turnOwner && param.TargetCell.PlacedUnit != null && param.TargetCell.PlacedUnit != param.Caster)); // 설치한 유닛의 턴이 시작했을 때 TakeDamage 발동

            return floorDamage;
        }

        public static CellModifier GetFloorTurnEndDamageModifier(CardEffectParameter param) // Param2 턴 동안 유지되는 장판 위에 있는 유닛은 턴을 종료하면 Param1 만큼 피해를 입는다
        {
            CellModifier floorDamage = new CellModifier("이름인데 지금 안쓰임", "설명인데 지금 안쓰임", param.Param2);
            floorDamage.DealerSource = param.Card;
            floorDamage.PassiveHandler.TurnStart.Add(TurnStartTrigger.GetCellModifierDurationTickTrigger()); // 이걸 설치한 유닛의 턴이 시작할 때 남은 턴 감소
            floorDamage.PassiveHandler.TurnEnd.Add(new TurnEndTrigger(
                (passiveOwner, turnOwner) =>
                {
                    turnOwner.TakeDamage(param.Param1, param.Card);
                },
                (passiveOwner, turnOwner) => GeneralVisualEffectContainer.GenerateFire(param.TargetCell.Indicator.transform.position),
                (passiveOwner, turnOwner) => (passiveOwner as CellModifier).Owner.PlacedCell == turnOwner.PlacedCell && turnOwner != param.Caster)); // 이 장판 위에 있는 유닛의 턴이 종료했을 때 TakeDamage 발동

            return floorDamage;
        }
    }
}
