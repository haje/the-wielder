﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using Wielder.Passive;
using Wielder.Units;
using Wielder.NonUnits;
using Wielder.Visual;

namespace Wielder.Maps
{
    public enum RoomType
    {
        None,
        StartRoom,
        BattleRoom,
        Shop,
        BossRoom,
        StairRoom
    }

    public class Room
    {
        public RoomBase BaseRoomData;
        public int RoomHeight, RoomWidth;
        public Dictionary<HexCoord, Cell> Cells;
        public bool IsBattleRoom;
        public bool IsCombatFinished;
        public RoomType Type;

        private List<HexCoord> unitLocations;
        private List<string> unitFiles;
        private List<HexCoord> nonUnitLocations;
        private List<string> nonUnitFiles;
        private List<int> rewardProbability;
        private List<string> rewardFiles;
        private string baseFileName;

        private List<Unit> units;
        public ReadOnlyCollection<Unit> Units
        {
            get
            {
                return units.AsReadOnly();
            }
        }

        private List<NonUnit> nonUnits;
        public ReadOnlyCollection<NonUnit> NonUnits
        {
            get
            {
                return nonUnits.AsReadOnly();
            }
        }

        public Room(string fileName)
        {
            baseFileName = fileName;
            Initialize(FileResourceManager.GetRoomBase(fileName));
        }

        public Room(Room room)
        {
            //주어진 room을 복제하여 생성한다.
            units = new List<Unit>();
            nonUnits = new List<NonUnit>();
            Cells = new Dictionary<HexCoord, Cell>();
            rewardFiles = new List<string>();
            rewardProbability = new List<int>();
            RoomHeight = room.RoomHeight;
            RoomWidth = room.RoomWidth;
            foreach (HexCoord hex in room.Cells.Keys)
            {
                Cells[hex] = new Cell(this, hex);
            }
            foreach (Unit unit in room.Units)
            {
                if (unit is Player) continue;
                SpawnUnit(new Monster(unit.BaseUnitData), Cells[unit.PlacedCell.Position]);
            }
        }

        private void Initialize(RoomBase roomBase)
        {
            BaseRoomData = roomBase;
            IsBattleRoom = true;
            IsCombatFinished = false;
            RoomHeight = roomBase.Height;
            RoomWidth = roomBase.Width;
            units = new List<Unit>();
            nonUnits = new List<NonUnit>();
            Cells = new Dictionary<HexCoord, Cell>();
            unitFiles = new List<string>();
            unitLocations = new List<HexCoord>();
            nonUnitFiles = new List<string>();
            nonUnitLocations = new List<HexCoord>();
            rewardFiles = new List<string>();
            rewardProbability = new List<int>();
            foreach (string unit in roomBase.UnitsInRoom)
            {

                string[] unitData = unit.Split(':');
                string[] unitpos = unitData[1].Split('/');
                unitFiles.Add(unitData[0]);
                unitLocations.Add(new HexCoord(int.Parse(unitpos[0]), int.Parse(unitpos[1])));

            }

            foreach (string nonUnit in roomBase.NonUnitsInRoom)
            {
                string[] nonUnitData = nonUnit.Split(':');
                string[] nonUnitpos = nonUnitData[1].Split('/');
                nonUnitFiles.Add(nonUnitData[0]);
                nonUnitLocations.Add(new HexCoord(int.Parse(nonUnitpos[0]), int.Parse(nonUnitpos[1])));
            }

            foreach (string reward in roomBase.RewardsInRoom)
            {
                string[] rewardData = reward.Split(':');
                rewardFiles.Add(rewardData[0]);
                rewardProbability.Add(int.Parse(rewardData[1]));
            }

            foreach (string file in unitFiles)
            {
                UnitBase unitBase = FileResourceManager.GetUnitBase(file);
                units.Add(new Monster(unitBase));
            }
            if (units.Count == 0)
            {
                IsBattleRoom = false;
            }

            foreach (string file in nonUnitFiles)
            {
                string[] fileData = file.Split('/');
                switch (fileData[0])
                {
                    case "obstacle":
                        nonUnits.Add(new Obstacle());
                        break;
                    case "shopkeeper":
                        nonUnits.Add(Shopkeeper.GenerateShopkeeper());
                        break;
                    case "chest":
                        if(fileData.Length != 1)
                        {
                            nonUnits.Add(Chest.GenerateChest(fileData[1]));
                        }
                        else
                        {
                            nonUnits.Add(Chest.GenerateChest());
                        }
                        break;
                    case "coin":
                        if (fileData.Length != 1)
                        {
                            nonUnits.Add(new Gold(int.Parse(fileData[1]))); 
                        }
                        else
                        {
                            nonUnits.Add(new Gold());
                        }
                        break;
                    default:
                        Debug.LogError("Unknown nonUnit type in " + baseFileName + ".json");
                        break;
                }
            }

            GenerateRoom();
        }
        /// <summary>
        /// Call after initializing width, height, and unit list
        /// </summary>
        private void GenerateRoom()
        {
            for (int i = 0; i < RoomWidth; i++)
            {
                for (int j = 0; j < RoomHeight; j++)
                {
                    Cells[new HexCoord(i, j)] = new Cell(this, new HexCoord(i, j));
                }
            }

            for (int i = 0; i < Units.Count; i++)
            {
                try
                {
                    Units[i].PlaceOnCell(Cells[unitLocations[i]]);
                }
                catch (Exception)
                {
                    Debug.Log(baseFileName + "에서 UnitLocation오류");
                }
            }

            for (int i = 0; i < NonUnits.Count; i++)
            {
                try
                {
                    NonUnits[i].PlaceOnCell(Cells[nonUnitLocations[i]]);
                }
                catch (Exception)
                {
                    Debug.Log(baseFileName + "에서 NonUnitLocation오류");

                }
            }
        }

        public List<Cell> GetAvailableTargetCells(Unit caster, int range, CellConditions conditions,
            TargetConditionCheck customCheckFunction = null)
        {
            Func<Cell, bool> conditionCheck = cell =>
            {
                if (caster.PlacedCell.GetDistanceFrom(cell) > range)
                    return false;

                if (conditions.HasFlag(CellConditions.EmptyCell) && cell.Occupied)
                    return false;

                if (conditions.HasFlag(CellConditions.UnitPresent) && cell.PlacedUnit == null)
                    return false;

                if (conditions.HasFlag(CellConditions.PlayerPresent) &&
                    !(cell.PlacedUnit != null && cell.PlacedUnit.UnitType == UnitType.Player))
                    return false;

                if (conditions.HasFlag(CellConditions.MonsterPresent) &&
                    !(cell.PlacedUnit != null && cell.PlacedUnit.UnitType == UnitType.Monster))
                    return false;

                if (conditions.HasFlag(CellConditions.AllyOfCasterPresent) &&
                    !(cell.PlacedUnit != null && cell.PlacedUnit.UnitType.IsAlliedWith(caster.UnitType)))
                    return false;

                if (conditions.HasFlag(CellConditions.EnemyOfCasterPresent) &&
                    !(cell.PlacedUnit != null && cell.PlacedUnit.UnitType.IsHostileAgainst(caster.UnitType)))
                    return false;

                // CellConditions.PassableTerrain 미구현

                if (conditions.HasFlag(CellConditions.Custom) && !customCheckFunction(caster, cell, false))
                    return false;

                return true;
            };

            return Cells.Values.Where(conditionCheck).ToList();
        }

        public List<Cell> GetAvailableRangeCells(Unit caster, int range, CellConditions conditions,
            TargetConditionCheck customCheckFunction = null)
        {
            Func<Cell, bool> conditionCheck = cell =>
            {
                if (conditions.HasFlag(CellConditions.Custom) && !customCheckFunction(caster, cell, true))
                    return false;

                if (caster.PlacedCell.GetDistanceFrom(cell) > range)
                    return false;

                return true;
            };

            return Cells.Values.Where(conditionCheck).ToList();
        }

        public void SpawnUnit(Unit unit, Cell cell)
        {
            units.Add(unit);
            unit.PlaceOnCell(cell);
        }

        public void RemoveUnit(Unit unit)
        {
            unit.PlacedCell.PlacedUnit = null;
            VisualResourceManagerScript.Instance.ReturnUnitIndicatorToPool(unit.Indicator.gameObject);
            unit.Indicator = null;
            units.Remove(unit);
            foreach (UnitModifier modifier in new List<UnitModifier>(unit.Modifiers))
                GameManager.RemovePassiveOwner(modifier);
            UnitInfoManager.Instance.Removed(unit);
        }

        public void SpawnNonUnit(NonUnit nonUnit, Cell cell)
        {
            nonUnits.Add(nonUnit);
            nonUnit.PlaceOnCell(cell);
            VisualResourceManagerScript.Instance.FetchNonUnitIndicator().GetComponent<NonUnitScript>().LinkedComponent = nonUnit;
        }

        public void RemoveNonUnit(NonUnit nonUnit)
        {
            nonUnit.PlacedCell.PlacedNonUnits.Remove(nonUnit);
            VisualResourceManagerScript.Instance.ReturnNonUnitIndicatorToPool(nonUnit.Indicator.gameObject);
            nonUnit.Indicator = null;
            nonUnits.Remove(nonUnit);
            if (nonUnit.NonUnitType == NonUnitType.Floor)
                foreach (CellModifier modifier in nonUnit.Modifiers)
                    GameManager.RemovePassiveOwner(modifier);
            UnitInfoManager.Instance.Removed(nonUnit);
        }

        public bool CheckCombatFinished()
        {
            foreach (Unit unit in Units)
            {
                if (unit.UnitType.IsAlliedWith(UnitType.Player))
                    continue;
                if (unit.CurrentRemainingHP > 0)
                    return IsCombatFinished = false;
            }
            
            if (!IsCombatFinished)
            {
                // 전투 끝나면 장판 제거
                VisualLogicManager.FreeAllFloors();
                // 보상
                GiveReward();
            }

            return IsCombatFinished = true;
        }

        private void GiveReward()
        {
            if (IsBattleRoom)
            {
                NonUnit marble = new RecoveryMarble();
                SpawnNonUnit(marble, FindNearCenterCell());
            }

            int random = UnityEngine.Random.Range(0, 100000);
            int sumProb = 0;
            
            for (int i = 0; i < rewardFiles.Count; i++)
            {
                sumProb += rewardProbability[i];
                if (random < sumProb)
                {
                    NonUnit reward;
                    string[] rewardData = rewardFiles[i].Split('/');
                    switch (rewardData[0])
                    {
                        case "obstacle":
                            reward = new Obstacle();
                            SpawnNonUnit(reward, FindNearCenterCell());
                            break;
                        case "shopkeeper":
                            reward = Shopkeeper.GenerateShopkeeper();
                            SpawnNonUnit(reward, FindNearCenterCell());
                            break;
                        case "chest":
                            if(rewardData.Length != 1)
                            {
                                reward = Chest.GenerateChest(rewardData[1]);
                            }
                            else
                            {
                                reward = Chest.GenerateChest();
                            }
                            SpawnNonUnit(reward, FindNearCenterCell());
                            break;
                        case "coin":
                            if (rewardData.Length != 1)
                            {
                                reward = new Gold(int.Parse(rewardData[1]));
                            }
                            else
                            {
                                reward = new Gold();
                            }
                            SpawnNonUnit(reward, FindNearCenterCell());
                            break;
                        case "bossdrop":
                            reward = Chest.GenerateChest("legend");
                            SpawnNonUnit(reward, FindNearCenterCell());
                            reward = Chest.GenerateChest("hero");
                            SpawnNonUnit(reward, FindNearCenterCell());
                            reward = Chest.GenerateChest("hero");
                            SpawnNonUnit(reward, FindNearCenterCell());
                            reward = new Heart();
                            SpawnNonUnit(reward, FindNearCenterCell());
                            reward = new ManaCrystal();
                            SpawnNonUnit(reward, FindNearCenterCell());
                            break;
                        default:
                            Debug.LogError("Unknown nonUnit type in " + rewardFiles[i] + ".json");
                            break;
                    }

                    break;
                }
            }
        }

        private Cell FindNearCenterCell()
        {
            HexCoord center = new HexCoord(RoomWidth / 2, RoomHeight / 2);
            HexCoord pointing = new HexCoord(RoomWidth / 2, RoomHeight / 2);
            int length = 0;
            int totalLength = 0;
            if(GameManager.CurrentRoom.Type == RoomType.BossRoom)
            {
                length += 1;
                totalLength = length * 6;
                pointing.Y -= 1;
            }
            while (Cells[pointing].PlacedUnit != null || Cells[pointing].PlacedNonUnits.Count != 0)
            {
                if (totalLength == 0)
                {
                    length += 1;
                    totalLength = length * 6;
                    pointing.Y -= 1;
                }
                else
                {
                    totalLength -= 1;
                    pointing = HexCoord.GetClockwise(center, pointing);
                }
            }

            return Cells[pointing];
        }

        public Unit FindUnitByName(string name)
        {
            foreach (Unit unit in Units)
            {
                if (unit.Name == name) return unit;
            }
            return null;
        }

        public void RefreshAllIndicators()
        {
            foreach (Unit unit in units)
            {
                unit.Indicator.RefreshDisplay();
            }

            foreach (NonUnit nonUnit in nonUnits)
            {
                nonUnit.Indicator.RefreshDisplay();
            }
        }
    }
}
