﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Wielder.Maps
{
	public class RoomBase : SerializableBase
	{
		public readonly int Width;
		public readonly int Height;
		public readonly ReadOnlyCollection<string> UnitsInRoom;
        public readonly ReadOnlyCollection<string> NonUnitsInRoom;
        public readonly ReadOnlyCollection<string> RewardsInRoom;
        public readonly int RoomID; //maybe needed for map generation algorithm
        public readonly int RoomDifficulty;
        public readonly string FileName;

		[SerializeField]
		private int width;

		[SerializeField]
		private int height;

		[SerializeField]
		private List<string> unitsInRoom;
        
        [SerializeField]
        private List<string> nonUnitsInRoom;

        [SerializeField]
        private List<string> rewardsInRoom;

        [SerializeField]
		private int roomID;

        [SerializeField]
        private int difficulty;


		protected override string FileDirectory 
		{
			get {
				return "Rooms/";
			}
		}

		public RoomBase (string FileName) : base(FileName)
		{
			Width = width;
			Height = height;
			UnitsInRoom = unitsInRoom.AsReadOnly();
            NonUnitsInRoom = nonUnitsInRoom.AsReadOnly();
            RewardsInRoom = rewardsInRoom.AsReadOnly();
            RoomID = roomID;
            RoomDifficulty = difficulty;
            this.FileName = FileName;
		}
	}
}

