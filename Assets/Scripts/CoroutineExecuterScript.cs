﻿using System.Collections;
using UnityEngine;

namespace Wielder
{
    public class CoroutineExecuterScript : MonoBehaviour
    {
        // Singleton

        private static CoroutineExecuterScript instance;

        public static CoroutineExecuterScript Instance
        {
            get
            {
                if (!instance)
                    instance = new GameObject("Chain Executer Container").AddComponent<CoroutineExecuterScript>();

                return instance;
            }
        }
    }
}
