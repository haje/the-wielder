﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Wielder
{
    public class MainMenuScript : MonoBehaviour
    {
        public void GameStart()
        {
            GameStarter.Instance.EventSystem.SetActive(false);
            StartCoroutine(Load());
        }

        private IEnumerator Load()
        {
            GameStarter.Instance.DebugMode = false;
            AsyncOperation task = SceneManager.LoadSceneAsync("Game Scene", LoadSceneMode.Additive);
            while (!task.isDone) yield return null;
        }
        
        public void GameEnd()
        {
            GameStarter.Instance.EventSystem.SetActive(false);
            Application.Quit();
        }

        public void DebugMode()
        {
            StartCoroutine(StartDebugMode());
        }

        private IEnumerator StartDebugMode()
        {
            GameStarter.Instance.EventSystem.SetActive(false);
            GameStarter.Instance.DebugMode = true;
            AsyncOperation task = SceneManager.LoadSceneAsync("Game Scene", LoadSceneMode.Additive);
            while (!task.isDone) yield return null;   
        }
    }
}
