﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Wielder.Units;

namespace Wielder
{
    public delegate void GameEffect();
    public delegate IEnumerator VisualEffect();

    namespace Cards
    {
        public delegate void CardGameEffect(CardEffectParameter param);
        public delegate IEnumerator CardVisualEffect(CardEffectParameter param);

        public delegate void CardModifierEffect(CardModifier modifier);
        public delegate void CardInitializerEffect(Card card);
    }

    namespace Units
    {
        public delegate void UnitModifierEffect(UnitModifier modifier);
        public delegate void UnitInitializerEffect(Unit unit);
    }

    namespace AI
    {
        public delegate void AIFunction(Unit unit);
    }

    namespace Maps
    {
        public delegate bool TargetConditionCheck(Unit caster, Cell target, bool onlyRange); // onlyRange가 true이면 거리가 닿기만 하면 true를 리턴
        public delegate List<Cell> CardEffectArea(Unit caster, Cell target);
        public delegate void CellModifierEffect(CellModifier modifier);
    }

    public static class DelegateHelper
    {
        public static TDelegate GetDelegateFromName<TDelegate>(Type containingClassType, string functionName)
        {
            MethodInfo mi = containingClassType.GetMethod(functionName, BindingFlags.Static | BindingFlags.NonPublic/* | BindingFlags.Public*/);
            //if (mi == null)
            //Debug.LogError("Wrong Method Name: " + functionName + " from " + containingClassType.Name);

            return (TDelegate)(object)Delegate.CreateDelegate(typeof(TDelegate), mi);
        }
    }
}
