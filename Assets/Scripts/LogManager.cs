﻿using System.Collections.Generic;
using Wielder.Cards;
using Wielder.Maps;
using Wielder.Units;
using UnityEngine;
using Wielder.Visual;

namespace Wielder
{
    public enum LogType
    {
        None,
        Deal,
        Heal,
        Die,
        Move,
        Cast,
        Absorb,
        EndRoom,
        GetMoney,
    }

    public class LogParameter
    {
        //이벤트 주체
        public Unit Actor;

        //이벤트 대상
        public Unit TargetUnit;
        public Cell TargetCell;
        public Card TargetCard;
        public Room TargetRoom;

        //기타 필요한 값
        public int Value;

        //Log 타입
        public LogType Type;

        //Assert 검사용
        private static List<LogType> NeedActorAndValue = new List<LogType>() { LogType.Deal, LogType.Heal, LogType.Absorb };
        private static List<LogType> NeedActor = new List<LogType>() { LogType.Die };
        private static List<LogType> NeedNothing = new List<LogType>() { LogType.EndRoom };

        private LogParameter(Unit actor, Unit targetUnit, Cell targetCell, Card targetCard, Room targetRoom, int value, LogType type)
        {
            Actor = actor;
            TargetUnit = targetUnit;
            TargetCell = targetCell;
            TargetCard = targetCard;
            TargetRoom = targetRoom;
            Value = value;
            Type = type;
        }

        public LogParameter(CardEffectParameter param)
            : this(param.Caster, param.TargetUnit, param.TargetCell, param.Card, null, 0, LogType.Cast)
        { }

        public LogParameter(Unit actor, int value, LogType type)
            : this(actor, null, null, null, null, value, type)
        {
            Debug.Assert(NeedActorAndValue.Contains(type));
        }

        public LogParameter(Unit actor, LogType type)
            : this(actor, null, null, null, null, 0, type)
        {
            Debug.Assert(NeedActor.Contains(type));
        }

        public LogParameter(LogType type)
            : this(null, null, null, null, null, 0, type)
        {
            Debug.Assert(NeedNothing.Contains(type));
        }

    }

    public class LogManager : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static LogManager instance;

        public static LogManager Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        private List<LogScript> logList;

        private const int maxLogNum = 7;

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;

            logList = new List<LogScript>();
        }

        /// <summary>
        /// Log function with parameter
        /// </summary>
        public void Log(LogParameter param)
        {
            Debug.Assert(param != null);
            if (logList.Count == maxLogNum)
            {
                VisualResourceManagerScript.Instance.ReturnLogIndicatorToPool(logList[0].gameObject);
                logList.RemoveAt(0);
            }

            LogScript newLog = VisualResourceManagerScript.Instance.FetchLogIndicator().GetComponent<LogScript>();
            logList.Add(newLog);
            SetLogPosition();
            newLog.LinkedComponent = param;
        }

        private void SetLogPosition()
        {
            for (int i = 0; i < logList.Count; i++)
            {
                logList[i].SetPosition(-i);
            }
        }

        public void ResetLog()
        {
            foreach(LogScript log in logList)
            {
                VisualResourceManagerScript.Instance.ReturnLogIndicatorToPool(log.gameObject);
            }
            logList.Clear();
        }
    }
}
