﻿using Wielder.Units;
using Wielder.Cards;

namespace Wielder.Passive.Replacements
{
    public delegate int CostReplacementEffect(int amount, Card card);
	public delegate int DamageReplacementEffect(int amount, IDamageDealer damageDealer, Unit sufferer);
	public delegate int HealReplacementEffect(int amount, /* Healer 정보도 담을까? */ Unit healTarget);

    public delegate bool CostReplacementCondition(int amount, Card card);
	public delegate bool DamageReplacementCondition(int amount, IDamageDealer damageDealer, Unit damageSufferer);
	public delegate bool HealReplacementCondition(int amount, Unit healTarget);

    public class CostReplacement
    {
        public CostReplacementCondition Condition;
        public CostReplacementEffect Effect;

        public CostReplacement(CostReplacementEffect effect, CostReplacementCondition condition)
        {
            Condition = condition;
            Effect = effect;
        }
    }

	public class DamageReplacement
	{
		public DamageReplacementCondition Condition;
		public DamageReplacementEffect Effect;

		public DamageReplacement(DamageReplacementEffect effect, DamageReplacementCondition condition)
		{
			Condition = condition;
			Effect = effect;
		}

        public static bool DamageDealerBelongsToUnit(IDamageDealer damageDealer, Unit unit)
        {
            return damageDealer == unit || (damageDealer as Card).Owner == unit;
        }
	}

	public class HealReplacement
	{
		public HealReplacementCondition Condition;
		public HealReplacementEffect Effect;

		public HealReplacement(HealReplacementEffect effect, HealReplacementCondition condition)
		{
			Condition = condition;
			Effect = effect;
		}
	}
}
