﻿namespace Wielder.Passive
{
    public interface IPassiveOwner
    {
        PassiveHandler PassiveHandler { get; }
    }
}