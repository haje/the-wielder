﻿using System.Collections.Generic;
using Wielder.Passive.Replacements;
using Wielder.Passive.Triggers;
using Wielder.Passive.Auras;

namespace Wielder.Passive
{
    public class PassiveHandler
    {
        public readonly List<TurnStartTrigger> TurnStart = new List<TurnStartTrigger>();
        public readonly List<TurnEndTrigger> TurnEnd = new List<TurnEndTrigger>();
        public readonly List<UnitDeathTrigger> UnitDeath = new List<UnitDeathTrigger>();
        public readonly List<BeforeCastTrigger> BeforeCast = new List<BeforeCastTrigger>();
        public readonly List<AfterCastTrigger> AfterCast = new List<AfterCastTrigger>();
        public readonly List<UnitMoveTrigger> UnitMove = new List<UnitMoveTrigger>();
        public readonly List<DealDamageTrigger> DealDamage = new List<DealDamageTrigger>();

        public readonly List<CostReplacement> CostReplacement = new List<CostReplacement>();
		public readonly List<DamageReplacement> DamageReplacement = new List<DamageReplacement>();
		public readonly List<HealReplacement> HealReplacement = new List<HealReplacement>();

        public readonly List<UnitAura> UnitAura = new List<UnitAura>();
        public readonly List<CardAura> CardAura = new List<CardAura>();
    }
}