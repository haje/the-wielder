﻿using Wielder.Cards;
using Wielder.Units;

namespace Wielder.Passive.Auras
{
    public delegate UnitModifier UnitModifierGenerator();
    public delegate bool UnitAuraCondition(IPassiveOwner source, Unit target);

    public delegate CardModifier CardModifierGenerator();
    public delegate bool CardAuraCondition(IPassiveOwner source, Card target);

    public class UnitAura
    {
        private UnitModifierGenerator modifierGenerator;
        public UnitAuraCondition Condition;

        public UnitAura(UnitModifierGenerator modifierGenerator, UnitAuraCondition condition)
        {
            this.modifierGenerator = modifierGenerator;
            Condition = condition;
        }

        public UnitModifier GenerateModifier(IPassiveOwner source)
        {
            UnityEngine.Debug.Assert(source.PassiveHandler.UnitAura.Contains(this), "Wrong Source Argument");
            UnitModifier modifier = modifierGenerator.Invoke();
            modifier.Source = source;
            return modifier;
        }
    }

    public class CardAura
    {
        private CardModifierGenerator modifierGenerator;
        public CardAuraCondition Condition;

        public CardAura(CardModifierGenerator modifierGenerator, CardAuraCondition condition)
        {
            this.modifierGenerator = modifierGenerator;
            Condition = condition;
        }

        public CardModifier GenerateModifier(IPassiveOwner source)
        {
            UnityEngine.Debug.Assert(source.PassiveHandler.CardAura.Contains(this), "Wrong Source Argument");
            CardModifier modifier = modifierGenerator.Invoke();
            modifier.Source = source;
            return modifier;
        }
    }
}