﻿namespace Wielder.Passive.Triggers
{
    /// <summary>
    /// 발동 효과의 발동 등 체인을 쓰지 않는 게임 효과를 처리하는 용도의 클래스입니다.
    /// </summary>
    public class TriggerQueueObject
    {
        public readonly ChainObject ChainObjectToPush;
        public readonly VisualEffect VisualEffect;
        /// <summary>
        /// 이 트리거로 인해 발생하는 체인오브젝트는 PushBefore이라는 체인오브젝트 앞에 배치돼야 합니다.
        /// </summary>
        public readonly ChainObject PushBefore;

        public TriggerQueueObject(ChainObject chainObjectToPush, VisualEffect visualEffect, ChainObject pushBefore = null)
        {
            ChainObjectToPush = chainObjectToPush;
            VisualEffect = visualEffect;
            PushBefore = pushBefore;
        }
        
    }
}
