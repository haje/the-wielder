﻿using System.Collections;
using Wielder.Cards;
using Wielder.Maps;
using Wielder.Units;
using Wielder.NonUnits;
using UnityEngine;
using Wielder.Visual;

namespace Wielder.Passive.Triggers
{
    // 여기 있는 코드가 좀 총체적 난국인데
    // 이걸 어떻게 예쁘게 할 수 있을지 잘 모르겠어요
    // 고민할 여지가 많이 있음 - zzt

    public delegate void TurnStartTriggerGameEffect(IPassiveOwner passiveOwner, Unit turnOwner);
    public delegate void TurnEndTriggerGameEffect(IPassiveOwner passiveOwner, Unit turnOwner);
    public delegate void DealDamageTriggerGameEffect(IPassiveOwner passiveOwner, IDamageDealer dealer, Unit damageTaker, int amount);
    public delegate void CastCardTriggerGameEffect(IPassiveOwner passiveOwner, CardEffectParameter cParam);
    public delegate void UnitDeathTriggerGameEffect(IPassiveOwner passiveOwner, Unit deadUnit);
    public delegate void UnitMoveTriggerGameEffect(IPassiveOwner passiveOwner, Unit movedUnit, Cell previousCell, Cell newCell);

    public delegate IEnumerator TurnStartTriggerVisualEffect(IPassiveOwner passiveOwner, Unit turnOwner);
    public delegate IEnumerator TurnEndTriggerVisualEffect(IPassiveOwner passiveOwner, Unit turnOwner);
    public delegate IEnumerator DealDamageTriggerVisualEffect(IPassiveOwner passiveOwner, IDamageDealer dealer, Unit damageTaker, int amount);
    public delegate IEnumerator CastCardTriggerVisualEffect(IPassiveOwner passiveOwner, CardEffectParameter cParam);
    public delegate IEnumerator UnitDeathTriggerVisualEffect(IPassiveOwner passiveOwner, Unit deadUnit);
    public delegate IEnumerator UnitMoveTriggerVisualEffect(IPassiveOwner passiveOwner, Unit movedUnit, Cell previousCell, Cell newCell);

    public delegate bool TurnStartTriggerCondition(IPassiveOwner passiveOwner, Unit turnOwner);
    public delegate bool TurnEndTriggerCondition(IPassiveOwner passiveOwner, Unit turnOwner);
    public delegate bool DealDamageTriggerCondition(IPassiveOwner passiveOwner, IDamageDealer dealer, Unit damageTaker, int amount);
    public delegate bool CastCardTriggerCondition(IPassiveOwner passiveOwner, CardEffectParameter cParam);
    public delegate bool UnitDeathTriggerCondition(IPassiveOwner passiveOwner, Unit deadUnit);
    public delegate bool UnitMoveTriggerCondition(IPassiveOwner passiveOwner, Unit movedUnit, Cell previousCell, Cell newCell);

    public static class TriggerHelper
    {
        public static void MDTickGEffect(IPassiveOwner passiveOwner, Unit turnOwner)
        {
            UnitModifier modifier = passiveOwner as UnitModifier;
            Debug.Assert(modifier != null);
            Debug.Assert(turnOwner.Modifiers.Contains(modifier));

            if (--modifier.RemainingTurn <= 0)
                turnOwner.RemoveModifier(modifier);
        }

        public static void MDCardTickGEffect(IPassiveOwner passiveOwner, CardEffectParameter cParam)
        {
            UnitModifier modifier = passiveOwner as UnitModifier;
            Debug.Assert(modifier != null);
            Debug.Assert(cParam.Caster.Modifiers.Contains(modifier));

            if (--modifier.RemainingCast <= 0)
                cParam.Caster.RemoveModifier(modifier);
        }

        public static void MDCellTickGEffect(IPassiveOwner passiveOwner, Unit turnOwner)
        {
            CellModifier modifier = passiveOwner as CellModifier;
            Debug.Assert(modifier != null);
            Debug.Assert(turnOwner == (modifier.DealerSource as Card).Owner); // 현재 CellModifier를 발생시키는 건 카드만 가능

            if (--modifier.RemainingTurn <= 0)
            {
                NonUnit nonUnit = modifier.Owner;
                nonUnit.RemoveModifier(modifier);
                GameManager.CurrentRoom.RemoveNonUnit(nonUnit);
            }
        }

        public static IEnumerator MDTickVEffect(IPassiveOwner passiveOwner, Unit turnOwner)
        {
            return GeneralVisualEffectContainer.NoVisualEffect();
        }

        public static IEnumerator MDCardTickVEffect(IPassiveOwner passiveOwner, CardEffectParameter cParam)
        {
            return GeneralVisualEffectContainer.NoVisualEffect();
        }

        public static IEnumerator MDCellTickVEffect(IPassiveOwner passiveOwner, Unit turnOwner)
        {
            return GeneralVisualEffectContainer.NoVisualEffect();
        }

        public static bool SelfTurn(IPassiveOwner passiveOwner, Unit turnOwner)
        {
            return turnOwner.Modifiers.Contains(passiveOwner as UnitModifier);
        }

        public static bool DealerTurn(IPassiveOwner passiveOwner, Unit turnOwner)
        {
            return turnOwner == ((passiveOwner as CellModifier).DealerSource as Card).Owner as Unit; // 현재 CellModifier를 발생시키는 건 카드만 가능
        }
    }

    public class TurnStartTrigger
    {
        public TurnStartTriggerGameEffect GameEffect;
        public TurnStartTriggerVisualEffect VisualEffect;
        public TurnStartTriggerCondition Condition;

        public TurnStartTrigger(TurnStartTriggerGameEffect gameEffect, TurnStartTriggerVisualEffect visualEffect, TurnStartTriggerCondition condition)
        {
            GameEffect = gameEffect;
            VisualEffect = visualEffect;
            Condition = condition;
        }

        public ChainObject GenerateChainObject(IPassiveOwner passiveOwner, Unit turnOwner)
        {
            return new ChainObject(() => GameEffect(passiveOwner, turnOwner), () => VisualEffect(passiveOwner, turnOwner));
        }

        public static TurnStartTrigger GetModifierDurationTickTrigger(TurnStartTriggerGameEffect expirationEffect = null) // // condition을 만족하면 remainingTurn이 감소
        {
            return new TurnStartTrigger(expirationEffect == null ? TriggerHelper.MDTickGEffect :
                TriggerHelper.MDTickGEffect + expirationEffect, TriggerHelper.MDTickVEffect, TriggerHelper.SelfTurn);
        }

        public static TurnStartTrigger GetCellModifierDurationTickTrigger() // // condition을 만족하면 remainingTurn이 감소
        {
            return new TurnStartTrigger(TriggerHelper.MDCellTickGEffect, TriggerHelper.MDCellTickVEffect, TriggerHelper.DealerTurn);
        }
    }

    public class TurnEndTrigger
    {
        public TurnEndTriggerGameEffect GameEffect;
        public TurnEndTriggerVisualEffect VisualEffect;
        public TurnStartTriggerCondition Condition;

        public TurnEndTrigger(TurnEndTriggerGameEffect gameEffect, TurnEndTriggerVisualEffect visualEffect, TurnStartTriggerCondition condition)
        {
            GameEffect = gameEffect;
            VisualEffect = visualEffect;
            Condition = condition;
        }

        public ChainObject GenerateChainObject(IPassiveOwner passiveOwner, Unit turnOwner)
        {
            return new ChainObject(() => GameEffect(passiveOwner, turnOwner), () => VisualEffect(passiveOwner, turnOwner));
        }

        public static TurnEndTrigger GetModifierDurationTickTrigger(TurnEndTriggerGameEffect expirationEffect = null) // condition을 만족하면 remainingTurn이 감소
        {
            return new TurnEndTrigger(expirationEffect == null ? TriggerHelper.MDTickGEffect :
                TriggerHelper.MDTickGEffect + expirationEffect, TriggerHelper.MDTickVEffect, TriggerHelper.SelfTurn);
        }
    }

    public class DealDamageTrigger
    {
        public DealDamageTriggerGameEffect GameEffect;
        public DealDamageTriggerVisualEffect VisualEffect;
        public DealDamageTriggerCondition Condition;

        public DealDamageTrigger(DealDamageTriggerGameEffect gameEffect, DealDamageTriggerVisualEffect visualEffect, DealDamageTriggerCondition condition)
        {
            GameEffect = gameEffect;
            VisualEffect = visualEffect;
            Condition = condition;
        }

        public ChainObject GenerateChainObject(IPassiveOwner passiveOwner, IDamageDealer dealer, Unit damageTaker, int amount)
        {
            return new ChainObject(() => GameEffect(passiveOwner, dealer, damageTaker, amount),
                () => VisualEffect(passiveOwner, dealer, damageTaker, amount));
        }
    }

    public class BeforeCastTrigger
    {
        public CastCardTriggerGameEffect GameEffect;
        public CastCardTriggerVisualEffect VisualEffect;
        public CastCardTriggerCondition Condition;

        public BeforeCastTrigger(CastCardTriggerGameEffect gameEffect, CastCardTriggerVisualEffect visualEffect, CastCardTriggerCondition condition)
        {
            GameEffect = gameEffect;
            VisualEffect = visualEffect;
            Condition = condition;
        }

        public ChainObject GenerateChainObject(IPassiveOwner passiveOwner, CardEffectParameter cEParam)
        {
            return new ChainObject(() => GameEffect(passiveOwner, cEParam), () => VisualEffect(passiveOwner, cEParam));
        }

        public static BeforeCastTrigger GetModifierDurationTickTrigger(CastCardTriggerCondition condition) // condition을 만족하면 remainingCast가 감소
        {
            return new BeforeCastTrigger(TriggerHelper.MDCardTickGEffect, TriggerHelper.MDCardTickVEffect, condition);
        }
    }

    public class AfterCastTrigger
    {
        public CastCardTriggerGameEffect GameEffect;
        public CastCardTriggerVisualEffect VisualEffect;
        public CastCardTriggerCondition Condition;

        public AfterCastTrigger(CastCardTriggerGameEffect gameEffect, CastCardTriggerVisualEffect visualEffect, CastCardTriggerCondition condition)
        {
            GameEffect = gameEffect;
            VisualEffect = visualEffect;
            Condition = condition;
        }

        public ChainObject GenerateChainObject(IPassiveOwner passiveOwner, CardEffectParameter cEParam)
        {
            return new ChainObject(() => GameEffect(passiveOwner, cEParam), () => VisualEffect(passiveOwner, cEParam));
        }

        public static AfterCastTrigger GetModifierDurationTickTrigger(CastCardTriggerCondition condition) // condition을 만족하면 remainingCast가 감소
        {
            return new AfterCastTrigger(TriggerHelper.MDCardTickGEffect, TriggerHelper.MDCardTickVEffect, condition);
        }
    }

    public class UnitDeathTrigger
    {
        public UnitDeathTriggerGameEffect GameEffect;
        public UnitDeathTriggerVisualEffect VisualEffect;
        public UnitDeathTriggerCondition Condition;

        public UnitDeathTrigger(UnitDeathTriggerGameEffect gameEffect, UnitDeathTriggerVisualEffect visualEffect, UnitDeathTriggerCondition condition)
        {
            GameEffect = gameEffect;
            VisualEffect = visualEffect;
            Condition = condition;
        }

        public ChainObject GenerateChainObject(IPassiveOwner passiveOwner, Unit deadUnit)
        {
            return new ChainObject(() => GameEffect(passiveOwner, deadUnit), () => VisualEffect(passiveOwner, deadUnit));
        }
    }

    public class UnitMoveTrigger
    {
        public UnitMoveTriggerGameEffect GameEffect;
        public UnitMoveTriggerVisualEffect VisualEffect;
        public UnitMoveTriggerCondition Condition;

        public UnitMoveTrigger(UnitMoveTriggerGameEffect gameEffect, UnitMoveTriggerVisualEffect visualEffect, UnitMoveTriggerCondition condition)
        {
            GameEffect = gameEffect;
            VisualEffect = visualEffect;
            Condition = condition;
        }

        public ChainObject GenerateChainObject(IPassiveOwner passiveOwner, Unit movedUnit, Cell previousCell, Cell newCell)
        {
            return new ChainObject(() => GameEffect(passiveOwner, movedUnit, previousCell, newCell),
                () => VisualEffect(passiveOwner, movedUnit, previousCell, newCell));
        }
    }
}