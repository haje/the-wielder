﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Wielder.Maps;
using Wielder.NonUnits;
using Wielder.Units;
using Wielder.Visual;

namespace Wielder
{
    public enum UIState
    {
        Disabled,
        PlayerTurnBasic,
        ChoosingTarget,
        RoomEnded,
        GameOver,
        YouWin,
        Debug_Waiting,
        Debug_Monster,
    }

    public class UIManagerScript : MonoBehaviour
    {
        // 변형 Singleton Pattern - 로딩되기 전에 참조하려 하면 디버그 어서션 페일 띄움
        private static UIManagerScript instance;

        public static UIManagerScript Instance
        {
            get
            {
                Debug.Assert(instance != null);
                return instance;
            }
        }

        [SerializeField]
        private GameObject hexagon;

        [SerializeField]
        private List<RoomMoveButtonScript> roomMoveButtons;

        [SerializeField]
        private GameObject turnEndText;

        private List<Cell> availableTargetCells; // 마우스를 올리면 반응할 수 있는 셀들

        private List<Cell> effectAreaCells; // 특정 셀에 마우스를 올렸을 때 그 외에도 동시에 반응하는 셀들

        private CardScript selectedCard;

        private ItemScript selectedItem;

        public bool DebugMode;

        public CardScript GetCurrentCard()
        {
            return selectedCard;
        }

        private void Awake()
        {
            Debug.Assert(instance == null);
            instance = this;

            int i = 0;
            foreach (Direction dir in Enum.GetValues(typeof(Direction)))
            {
                roomMoveButtons[i].Dir = dir;
                i++;
            }
        }

        private UIState uIState;

        public UIState GetUIState()
        {
            return uIState;
        }

        public void SetUIState(UIState value)
        {
            ResetAllCellColors();
            uIState = value;
            switch (uIState)
            {
                case UIState.Disabled:
                    hexagon.SetActive(false);
                    break;
                case UIState.PlayerTurnBasic:
                    ActivateTurnEndButton();
                    RefreshPlayerMovableCells();
                    PlayerInfoManager.Instance.UpdatePlayerInfo();
                    break;
                case UIState.ChoosingTarget:
                    hexagon.SetActive(false);
                    availableTargetCells = GameManager.CurrentRoom.GetAvailableTargetCells(
                        GameManager.Player, selectedCard.LinkedComponent.Range, selectedCard.LinkedComponent.BaseCardData.TargetConditions,
                        selectedCard.LinkedComponent.BaseCardData.CustomTargetCheckFunction);
                    effectAreaCells = new List<Cell>();

                    foreach (Cell cell in availableTargetCells)
                    {
                        cell.Indicator.BaseColor = Color.yellow;
                        cell.Indicator.PingPongColor = Color.red;
                    }
                    break;
                case UIState.RoomEnded:
                    ActivateRoomMoveButton();
                    PlayerInfoManager.Instance.UpdatePlayerInfo();
                    availableTargetCells = GameManager.CurrentRoom.GetAvailableTargetCells(
                    GameManager.Player, 100, CellConditions.EmptyCell | CellConditions.PassableTerrain);

                    foreach (Cell cell in availableTargetCells)
                    {
                        cell.Indicator.BaseColor = Color.cyan;
                    }

                    if (GameManager.CurrentRoom.Type == RoomType.BossRoom)
                    {
                        Cell center = GameManager.CurrentRoom.Cells[new HexCoord(GameManager.CurrentRoom.RoomWidth / 2, GameManager.CurrentRoom.RoomHeight / 2)];
                        center.Indicator.BaseColor = Color.yellow;
                    }
                    if (DebugMode)
                    {
                        DebugManagerScript.Instance.Show();
                        DebugManagerScript.Instance.RestoreTestRoom();
                    }
                    break;
                case UIState.Debug_Waiting:
                    ActivateRoomMoveButton();
                    foreach (Cell cell in GameManager.CurrentRoom.Cells.Values)
                    {
                        if (cell.PlacedUnit != null && cell.PlacedUnit is Monster)
                        {
                            cell.Indicator.BaseColor = Color.cyan;
                        }
                    }
                    break;
                case UIState.YouWin:
                case UIState.GameOver:
                    if (DebugMode)
                    {
                        SetUIState(UIState.Debug_Waiting);
                        GameManager.Player.GetHealed(1000);
                        Room room = GameManager.CurrentRoom;
                        room.SpawnUnit(GameManager.Player, room.Cells[new HexCoord(room.RoomWidth / 2, room.RoomHeight / 2)]);
                        return;
                    }
                    BannerScript.Instance.Message = uIState == UIState.GameOver ? BannerScript.MessageType.GameOver : BannerScript.MessageType.YouWin;
                    PopupUIScript.Instance.ShowBanner();
                    hexagon.SetActive(false);
                    break;
                case UIState.Debug_Monster:
                    List<Cell> validTargetCells = GameManager.CurrentRoom.GetAvailableTargetCells(GameManager.Player, 100, CellConditions.EmptyCell | CellConditions.PassableTerrain);
                    foreach (Cell cell in validTargetCells)
                    {
                        cell.Indicator.BaseColor = Color.cyan;
                        cell.Indicator.PingPongColor = Color.blue;
                    }
                    break;
            }
        }

        private void Update()
        {
            int result;
            if (int.TryParse(Input.inputString, out result) && (result <= PlayerInfoManager.Instance.DisplayingCards.Count))
            {
                if (uIState == UIState.PlayerTurnBasic || uIState == UIState.ChoosingTarget)
                    CardClicked(PlayerInfoManager.Instance.DisplayingCards[result - 1]);
            }

        }

        private void ResetAllCellColors()
        {
            foreach (Cell cell in GameManager.CurrentRoom.Cells.Values)
            {
                cell.Indicator.BaseColor = cell.Indicator.PingPongColor = Color.white;
                cell.Indicator.PingPong = false;
            }
        }

        private void RefreshPlayerMovableCells()
        {
            if (GameManager.Player.CurrentAP >= GameManager.Player.MovementAP)
            {
                availableTargetCells = GameManager.CurrentRoom.GetAvailableTargetCells(
                    GameManager.Player, 1, CellConditions.EmptyCell | CellConditions.PassableTerrain);
                foreach (Cell cell in availableTargetCells)
                {
                    cell.Indicator.BaseColor = Color.cyan;
                    cell.Indicator.PingPongColor = Color.blue;
                }
            }
            else
                availableTargetCells = new List<Cell>();
        }

        private void ActivateTurnEndButton()
        {
            hexagon.SetActive(true);
            turnEndText.SetActive(true);
            foreach (RoomMoveButtonScript script in roomMoveButtons)
            {
                script.Active = true;
                script.Pingpong = false;
            }
        }

        private void ActivateRoomMoveButton()
        {
            if (DebugMode)
            {
                hexagon.SetActive(true);
                foreach (RoomMoveButtonScript script in roomMoveButtons)
                {
                    script.Active = true;
                    script.Pingpong = false;
                }
                return;
            }
            hexagon.SetActive(true);
            turnEndText.SetActive(false);
            List<Direction> dirs = GameManager.CurrentStage.NextRoomDirections();
            foreach (RoomMoveButtonScript script in roomMoveButtons)
            {
                if (dirs.Contains(script.Dir))
                    script.Active = true;
                else
                    script.Active = false;
                script.Pingpong = false;
            }
        }

        public void RoomMoveButtonMouseOver(RoomMoveButtonScript mouseOvered)
        {
            switch (uIState)
            {
                case UIState.PlayerTurnBasic: // 턴 종료
                    foreach (RoomMoveButtonScript button in roomMoveButtons)
                    {
                        button.Pingpong = true;
                    }
                    break;
                case UIState.Debug_Waiting:
                case UIState.RoomEnded: // 방 이동
                    mouseOvered.Pingpong = true;
                    break;
                default:
                    break;
            }
        }

        public void RoomMoveButtonMouseExit(RoomMoveButtonScript mouseExited)
        {
            switch (uIState)
            {
                case UIState.PlayerTurnBasic: // 턴 종료
                    foreach (RoomMoveButtonScript button in roomMoveButtons)
                    {
                        button.Pingpong = false;
                    }
                    break;
                case UIState.RoomEnded: // 방 이동
                    mouseExited.Pingpong = false;
                    break;
                default:
                    break;
            }
        }

        public void RoomMoveButtonClicked(Direction dir)
        {
            switch (uIState)
            {
                case UIState.PlayerTurnBasic: // 턴 종료
                    SetUIState(UIState.Disabled);
                    GameManager.EndUnitTurn();
                    break;
                case UIState.RoomEnded: // 방 이동
                    hexagon.SetActive(false);
                    GameManager.MoveRoom(dir);
                    break;
                case UIState.Debug_Waiting: //방 재시작
                    DebugManagerScript.Instance.RestartRoom(dir);
                    break;
                default:
                    break;
            }
        }

        public void CellMouseOvered(CellScript cellScript)
        {
            switch (uIState)
            {
                case UIState.PlayerTurnBasic:
                case UIState.RoomEnded:
                    if (!availableTargetCells.Contains(cellScript.LinkedComponent))
                        break;

                    cellScript.PingPongColor = Color.blue;
                    cellScript.PingPong = true;
                    break;
                case UIState.ChoosingTarget:
                    if (!availableTargetCells.Contains(cellScript.LinkedComponent))
                        break;

                    effectAreaCells = selectedCard.LinkedComponent.BaseCardData.CustomAreaFunction(GameManager.Player, cellScript.LinkedComponent);
                    foreach (Cell cell in effectAreaCells)
                    {
                        cell.Indicator.PingPongColor = Color.red;
                        cell.Indicator.PingPong = true;
                    }
                    break;
                default:
                    break;
            }
        }

        public void CellMouseExited(CellScript cellScript)
        {
            switch (uIState)
            {
                case UIState.PlayerTurnBasic:
                    if (availableTargetCells.Contains(cellScript.LinkedComponent))
                        cellScript.PingPong = false;
                    break;
                case UIState.ChoosingTarget:
                    if (availableTargetCells.Contains(cellScript.LinkedComponent))
                    {
                        foreach (Cell cell in effectAreaCells)
                        {
                            cell.Indicator.PingPong = false;
                        }
                    }
                    break;
                case UIState.RoomEnded:
                    if (availableTargetCells.Contains(cellScript.LinkedComponent))
                        cellScript.PingPong = false;
                    break;
                default:
                    break;
            }
        }

        public void CellClicked(CellScript cellScript)
        {
            switch (uIState)
            {
                case UIState.PlayerTurnBasic:
                    if (availableTargetCells.Contains(cellScript.LinkedComponent))
                    {
                        GameManager.Player.AddMovementToChain(cellScript.LinkedComponent);
                    }
                    break;
                case UIState.ChoosingTarget:
                    selectedCard.Unselect();
                    if (availableTargetCells.Contains(cellScript.LinkedComponent))
                        GameManager.Player.UseCardInHand(selectedCard.LinkedComponent, cellScript.LinkedComponent);
                    else
                        SetUIState(UIState.PlayerTurnBasic);
                    break;
                case UIState.RoomEnded:
                    foreach (NonUnit nonUnit in cellScript.LinkedComponent.PlacedNonUnits)
                    {
                        nonUnit.Clicked();
                    }
                    if (availableTargetCells.Contains(cellScript.LinkedComponent))
                        GameManager.Player.AddMovementToChain(cellScript.LinkedComponent);
                    if (GameManager.CurrentRoom.Type == RoomType.BossRoom)
                    {
                        Cell center = GameManager.CurrentRoom.Cells[new HexCoord(GameManager.CurrentRoom.RoomWidth / 2, GameManager.CurrentRoom.RoomHeight / 2)];
                        if (center == cellScript.LinkedComponent)
                        {
                            if (GameManager.CurrentStage.CurrentLevel == 2)
                                SetUIState(UIState.YouWin);
                            else
                            {
                                VisualLogicManager.FreeCurrentRoomResources();
                                Map newStage = new Map(GameManager.CurrentStage.CurrentLevel + 1); //TODO GameManager CurrentStage를 수정해야 함
                                GameManager.StartStage(newStage);
                            }
                        }
                    }
                    break;
                case UIState.Debug_Waiting:
                    if (cellScript.LinkedComponent.PlacedUnit != null && cellScript.LinkedComponent.PlacedUnit is Monster)
                    {
                        GameManager.CurrentRoom.RemoveUnit(cellScript.LinkedComponent.PlacedUnit);
                        cellScript.BaseColor = Color.white;
                    }
                    break;
                case UIState.Debug_Monster:
                    DebugManagerScript.Instance.CellClicked(cellScript.LinkedComponent);
                    break;
                default:
                    break;
            }
        }

        public void CardMouseOvered(CardScript cardScript)
        {
            switch (uIState)
            {
                case UIState.PlayerTurnBasic:
                    ResetAllCellColors();
                    availableTargetCells = GameManager.CurrentRoom.GetAvailableRangeCells(
                        GameManager.Player, cardScript.LinkedComponent.Range, cardScript.LinkedComponent.BaseCardData.TargetConditions,
                        cardScript.LinkedComponent.BaseCardData.CustomTargetCheckFunction);
                    foreach (Cell cell in availableTargetCells)
                    {
                        cell.Indicator.BaseColor = Color.green;
                        cell.Indicator.PingPongColor = Color.green;
                    }
                    break;
                default:
                    break;
            }
        }

        public void CardMouseExited(CardScript cardScript)
        {
            switch (uIState)
            {
                case UIState.PlayerTurnBasic:
                    ResetAllCellColors();
                    RefreshPlayerMovableCells();
                    break;
            }
        }

        public void CardClicked(CardScript cardScript)
        {
            // 지금 단계에서는 이 카드는 무조건 플레이어 손 안의 카드 -> 이제 아님
            //Debug.Assert(GameManager.Player.Hand.Contains(cardScript.LinkedComponent));

            if (GameManager.Player.Hand.Contains(cardScript.LinkedComponent))
            {
                if (selectedCard != null && selectedCard == cardScript)   // 같은 카드를 다시 클릭했을 때
                {
                    cardScript.Unselect();
                    selectedCard = null;
                    SetUIState(UIState.PlayerTurnBasic);
                    return;
                }

                if (uIState != UIState.PlayerTurnBasic && uIState != UIState.ChoosingTarget)
                    return;

                if (cardScript.LinkedComponent.Cost > GameManager.Player.CurrentAP ||
                    cardScript.LinkedComponent.BaseCardData.MoneyCost > GameManager.Player.CurrentMoney)
                    return;

                if (selectedCard != null)
                    selectedCard.Unselect();

                if (cardScript.LinkedComponent.BaseCardData.HasTarget)
                {
                    selectedCard = cardScript;
                    cardScript.Select();
                    SetUIState(UIState.ChoosingTarget);
                }
                else
                {
                    GameManager.Player.UseCardInHand(cardScript.LinkedComponent, null);
                }
            }
        }


    }
}
