﻿using Wielder.Maps;
using Wielder.Units;
using Wielder.NonUnits;

namespace Wielder.Cards
{
    public static class CardGameEffectContainer
    {
        public static CardGameEffect GetEffectFromName(string functionName)
        {
            return DelegateHelper.GetDelegateFromName<CardGameEffect>(typeof(CardGameEffectContainer), functionName);
        }

        static void NoGameEffect(CardEffectParameter param)
        { }

        static void NotImplemented(CardEffectParameter param)
        {
            UnityEngine.Debug.LogWarning(param.Card.Name + "은 아직 구현되지 않았습니다.");
        }

        static void AdditionalDraw(CardEffectParameter param) // Param2 턴 동안 Param1 만큼 카드를 추가로 뽑는다
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetIncreaseHandModifier(param));
        }

        static void AddShield(CardEffectParameter param) // Param1 만큼 방어막을 얻는다
        {
            param.Caster.GetShield(param.Param1);
        }

        static void APMultiDamage(CardEffectParameter param) // 행동력을 모두 소모하고 그 수만큼 피해를 1씩 준다
        {
            if (param.TargetUnit == null)
                return;

            for (int i = 0; i < param.Caster.CurrentAP; i++)
                DealDamage(param);

            param.Caster.CurrentAP = 0;
        }

        static void Assasinate(CardEffectParameter param) // Param1 만큼 피해를 주고, 죽으면 Param2 만큼 현재 행동력 회복
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.TakeDamage(param.Param1, param.Card);
            if (param.TargetUnit.CurrentRemainingHP <= 0)
                param.Caster.CurrentAP += param.Param2;
        }

        static void Berserk(CardEffectParameter param)
        {
            CardEffectParameter newParam = new CardEffectParameter(param.Card, param.TargetCell);
            newParam.Param1 = 3;
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetReduceAPModifier(newParam)); // Param2 턴 동안 최대 행동력 3 감소
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetEnrageModifier(param)); // Param2 턴 동안 카드 피해 Param1 만큼 증가
        }

        static void Bind(CardEffectParameter param) // 속박 : Param1 턴 동안 이동 비용 999
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.AddModifier(Unit.UnitModifierFactory.GetBindModifier(param));
        }

        static void Boost(CardEffectParameter param) // 이번 턴에 Param1 만큼 행동력을 얻는다
        {
            param.Caster.CurrentAP += param.Param1;
        }

        static void BulletSlug(CardEffectParameter param) // (탄환 강화) 2턴 간 Param2 클래스의 데미지를 Param1 만큼 더함
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetBulletSlugModifier(param));
        }

        static void Burn(CardEffectParameter param) // 화상 : Param2 턴 동안 턴 종료 시 Param1 만큼 피해를 입는다
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.AddModifier(Unit.UnitModifierFactory.GetBurnModifier(param));
        }

        static void ClassAddition(CardEffectParameter param) // Param2 클래스의 데미지를 Param1 만큼 더함
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetClassAdditionModifier(param));
        }

        static void ClassCost(CardEffectParameter param) // 다음에 사용하는 카드가 Param2 클래스이면 코스트가 Param1 만큼 증가
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetClassCostModifier(param));
        }

        static void ClassMultiply(CardEffectParameter param) // Param2 클래스의 데미지를 Param1 만큼 곱함
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetClassMultiplyModifier(param));
        }

        /// <summary>
        /// 디버그용 - 한방에 주님 곁으로
        /// </summary>
        static void ClearRoom(CardEffectParameter param)
        {
            foreach (Unit unit in GameManager.CurrentRoom.Units)
            {
                if (unit is Player) continue;
                unit.TakeDamage(500, param.Card);
            }
        }

        static void Courage(CardEffectParameter param)
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetEnrageModifier(param));
        }

        static void Crouch(CardEffectParameter param) // Param1 만큼 방어도를 얻고, 다음 턴 시작에 모든 방어도를 잃는다
        {
            param.Caster.GetShield(param.Param1);
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetCrouchModifier(param));
        }

        static void DealAndGetMoney(CardEffectParameter param) // Param1 만큼 피해를 입히고 Param2 만큼 재화를 얻는다
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.TakeDamage(param.Param1, param.Card);
            param.Caster.GetMoney(param.Param2);
        }

        /* 안 쓰임 */
        static void DealAndHealIfItKills(CardEffectParameter param) // Param1 만큼 피해를 입히고 죽을 시 Param2 만큼 회복한다
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.TakeDamage(param.Param1, param.Card);
            if (param.TargetUnit.CurrentRemainingHP <= 0)
                param.Caster.GetHealed(param.Param2);
        }

        static void DealDamage(CardEffectParameter param) // Param1 만큼 피해를 입힌다
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.TakeDamage(param.Param1, param.Card);
        }

        static void DealDamageAndGetShield(CardEffectParameter param) // Param1 만큼 피해를 입히고 Param2 만큼 방어도를 얻는다
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.TakeDamage(param.Param1, param.Card);
            param.Caster.GetShield(param.Param2);
        }

        static void DealDamageAndReduceAP(CardEffectParameter param) // Param1 만큼 피해를 입히고 Param2 턴 동안 Param1 만큼 행동력 감소 디버프
        {
            if (param.TargetUnit == null)
                return;

            DealDamage(param);
            ReduceAP(param);
        }

        static void DealDamageMulti(CardEffectParameter param) // Param1 만큼의 피해를 총 Param2번 입힌다
        {
            if (param.TargetUnit == null)
                return;

            for (int i = 0; i < param.Param2; i++)
                DealDamage(param);
        }

        static void DelayedDamage(CardEffectParameter param) // Param1 턴 뒤의 턴 끝에 Param2 만큼 피해를 입힌다
        {
            if (param.TargetUnit == null)
                return;

            param.Caster.AddModifier(Unit.UnitModifierFactory.GetArtilleryModifier(param));
            param.Caster.SealCard(param.Card);
        }

        static void Dodge(CardEffectParameter param)
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetDodgeModifier(param));
        }

        static void DrawAndHurt(CardEffectParameter param) // Param1 만큼 피해를 입고 Param2 만큼 카드를 뽑는다
        {
            param.Caster.TakeDamage(param.Param1, param.Card);
            param.Caster.Draw(param.Param2);
        }

        static void DrawCards(CardEffectParameter param) // Param1 만큼 카드를 뽑는다
        {
            param.Caster.Draw(param.Param1);
        }

        static void DrawOnePlusOne(CardEffectParameter param) // 카드를 1장 뽑고 Param2 턴 동안 Param1 만큼 카드를 추가로 뽑는다
        {
            param.Caster.Draw(1);
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetIncreaseHandModifier(param));
        }

        static void Earthquake(CardEffectParameter param)
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetEarthquakeModifier(param));
        }

        static void Enrage(CardEffectParameter param)
        {
            param.Caster.TakeDamage(2, param.Card);
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetEnrageModifier(param));
        }

        static void Fear(CardEffectParameter param) // Param1 턴 동안 이동 비용 999 및 모든 피해를 -999로
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.AddModifier(Unit.UnitModifierFactory.GetCleanseModifier(param));
        }

        static void FinalStrike(CardEffectParameter param) // 모든 행동력을 소모하고 그 만큼 피해를 준다. 처치하면 소모한 행동력을 돌려받는다
        {
            if (param.TargetUnit == null)
                return;

            int amount = param.Caster.CurrentAP;
            param.TargetUnit.TakeDamage(amount, param.Card);
            if (param.TargetUnit.CurrentRemainingHP > 0)
                param.Caster.CurrentAP = 0;
        }

        static void FloorCasterTurnStartDamage(CardEffectParameter param) // Param2 턴 동안 유지되는 장판 위에 있는 유닛은 시전자가 턴을 시작하면 Param1만큼 피해를 입는다
        {
            Floor floor = new Floor(param.Card.Name, param.Card.BaseCardData.FloorImage, string.Format("이 칸에 있는 적은 시전자의 턴이 시작할 때 피해를 {0} 받습니다.", param.Param1));
            GameManager.CurrentRoom.SpawnNonUnit(floor, param.TargetCell);
            CellModifier modifier = CellModifierFactory.GetFloorCasterTurnStartDamageModifier(param);
            floor.AddCellModifier(modifier);
        }

        static void FloorTurnEndDamage(CardEffectParameter param) // Param2 턴 동안 유지되는 장판 위에 있는 유닛은 턴을 종료하면 Param1 만큼 피해를 입는다
        {
            Floor floor = new Floor(param.Card.Name, param.Card.BaseCardData.FloorImage, string.Format("이 칸에 있는 적은 턴을 종료할 때 피해를 {0} 받습니다.", param.Param1));
            GameManager.CurrentRoom.SpawnNonUnit(floor, param.TargetCell);
            CellModifier modifier = CellModifierFactory.GetFloorTurnEndDamageModifier(param);
            floor.AddCellModifier(modifier);
        }

        static void Fury(CardEffectParameter param) 
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetFuryModifier(param));
        }

        static void IncreaseAP(CardEffectParameter param)
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetIncreaseAPModifier(param));
        }

        static void IncreaseHand(CardEffectParameter param)
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetIncreaseHandModifier(param));
        }

        static void InvokeBleed(CardEffectParameter param)  // Param2 턴 동안 턴 시작마다 Param1 만큼 피해를 입는다
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.AddModifier(Unit.UnitModifierFactory.GetBleedModifier(param));
        }

        static void LoadArtillery(CardEffectParameter param)
        {
            foreach (Unit unit in GameManager.CurrentRoom.Units)
                if (unit.Name == "포병" && unit.CurrentRemainingHP > 0)
                {
                    unit.AddCardToDeck(new Card("artillery"));
                    return;
                }

            param.Caster.AddCardToDeck(new Card("artillery"));
        }

        static void MultiplyAll(CardEffectParameter param)
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetMultiplyAllDamageModifier(param));
        }

        static void Pillage(CardEffectParameter param) // 약탈
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.TakeDamage(param.Param1, param.Card);
            if (param.TargetUnit.CurrentRemainingHP <= 0)
                param.Caster.GetMoney(param.TargetUnit.CurrentMaxHP);
        }

        static void RandomMultiDamage(CardEffectParameter param) // Param1 만큼의 데미지를 2 ~ 5회 준다
        {
            if (param.TargetUnit == null)
                return;

            for (int i = 0; i < UnityEngine.Random.Range(2, 6) ; i++)
                DealDamage(param);
        }
        
        static void ReduceAllDamageFrom(CardEffectParameter param) // Param1 턴 동안 목표가 주는 모든 피해를 -999로
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.AddModifier(Unit.UnitModifierFactory.GetReduceAllDamageFromModifier(param));
        }

        static void ReduceAP(CardEffectParameter param) // Param2 턴 동안 Param1 만큼 턴당 행동력을 낮춘다
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.AddModifier(Unit.UnitModifierFactory.GetReduceAPModifier(param));
        }

        static void ReduceDamageTo(CardEffectParameter param) // Param2 턴 동안 들어오는 피해가 Param1 만큼 감소함
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetReduceDamageToModifier(param));
        }

        static void Retaliate(CardEffectParameter param)
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.TakeDamage(param.Caster.CurrentMaxHP - param.Caster.CurrentRemainingHP, param.Card);
        }

        static void Sadism(CardEffectParameter param)
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetSadismModifier(param));
        }

        static void SpellChain(CardEffectParameter param)
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.TakeDamage(param.Caster.UsedCardInTurn - 1/* 카드 트리거가 UsedCardInTurn을 증가하고 나서 실행됨*/, param.Card);
        }

        static void Spirit(CardEffectParameter param)
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetSpiritModifier(param));
        }

        static void Stun(CardEffectParameter param) // Param1 턴 동안 스턴
        {
            if (param.TargetUnit == null)
                return;

            param.TargetUnit.AddModifier(Unit.UnitModifierFactory.GetStunModifier(param));
        }

        static void TeleportTo(CardEffectParameter param)
        {
            param.Caster.PlaceOnCell(param.TargetCell);
        }

        static void ThreateningRoar(CardEffectParameter param) // Range 내의 유닛들은 Param2 턴 동안 행동력이 Param1 만큼 감소함
        {
            foreach (Cell cell in GameManager.CurrentRoom.GetAvailableTargetCells(param.Caster, param.Card.Range, CellConditions.EnemyOfCasterPresent))
                cell.PlacedUnit.AddModifier(Unit.UnitModifierFactory.GetReduceAPModifier(param));
        }

        static void TurnEndShield(CardEffectParameter param)
        {
            param.Caster.AddModifier(Unit.UnitModifierFactory.GetTurnEndShieldModifier(param));
        }

        static void SummonGhoul(CardEffectParameter param)
        {
            Unit newUnit = new Monster(FileResourceManager.GetUnitBase("ghoul"));
            GameManager.CurrentRoom.SpawnUnit(newUnit, param.TargetCell);
            Visual.VisualLogicManager.SpawnUnit(newUnit);
        }

        static void SummonZombie(CardEffectParameter param)
        {
            Unit newUnit = new Monster(FileResourceManager.GetUnitBase("zombie"));
            GameManager.CurrentRoom.SpawnUnit(newUnit, param.TargetCell);
            Visual.VisualLogicManager.SpawnUnit(newUnit);
        }
    }
}
