﻿namespace Wielder.Cards
{
    public partial class Card
    {
        public static class CardInitializerEffectContainer
        {
            public static CardInitializerEffect GetUnitInitializerEffectFromName(string functionName)
            {
                return DelegateHelper.GetDelegateFromName<CardInitializerEffect>
                    (typeof(CardInitializerEffectContainer), functionName);
            }

            static void IdolofVitatlityInit(Card card)
            {
                card.AddModifier(CardModifierFactory.GetIdolofVitalityModifier());
            }
        }
    }
}
