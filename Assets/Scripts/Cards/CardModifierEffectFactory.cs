﻿using Wielder.Passive.Auras;
using Wielder.Units;

namespace Wielder.Cards
{
    public partial class Card
    {
        public static class CardModifierFactory
        {
            public static CardModifier GetIdolofVitalityModifier()
            {
                CardModifier ownerAPBuff = new CardModifier("소유자 행동력 증가", "활력의 우상의 소유자의 턴당 행동력이 3 증가한다.",
                    CardModifierEffectContainer.NoModifierEffect);

                ownerAPBuff.PassiveHandler.UnitAura.Add(new UnitAura(Unit.UnitModifierFactory.GetIdolOfVitatlityOwnerModifier,
                    (passiveOwner, unit) => unit.GetCards().Contains(ownerAPBuff.Owner)));

                return ownerAPBuff;
            }
        }
    }
}
