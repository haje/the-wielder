﻿using System;
using System.Collections.Generic;
using Wielder.Units;
using Wielder.Maps;

namespace Wielder.Cards
{
    public static class CardEffectAreaContainer
    {
        public static CardEffectArea GetAreaFromName(string functionName)
        {
            return DelegateHelper.GetDelegateFromName<CardEffectArea>(typeof(CardEffectAreaContainer), functionName);
        }

        static List<Cell> Normal(Unit caster, Cell target)
        {
            List<Cell> result = new List<Cell>();
            result.Add(target);

            return result;
        }

        static List<Cell> WideAreaOne(Unit caster, Cell target) // 대상 주변 거리 1 이내의 셀 포함
        {
            List<Cell> result = new List<Cell>();
            Dictionary<HexCoord, Cell> cells = GameManager.CurrentRoom.Cells;

            result.Add(target);

            foreach (HexCoord hexCoord in target.Position.AdjacentList())
                if (cells.ContainsKey(hexCoord))
                    result.Add(cells[hexCoord]);

            return result;
        }

        static List<Cell> WideAreaOneNotMe(Unit caster, Cell target) // 대상 주변 거리 1 이내의 셀 포함, 자신이 있는 곳은 제외
        {
            List<Cell> result = WideAreaOne(caster, target);

            foreach (Cell cell in result)
                if (cell.PlacedUnit == caster)
                {
                    result.Remove(cell);
                    break;
                }

            return result;
        }

        static List<Cell> Swing(Unit caster, Cell target) // 대상으로부터 시계 방향과 반시계 방향으로 셀 한칸씩 포함
        {
            List<Cell> result = new List<Cell>();
            Dictionary<HexCoord, Cell> cells = GameManager.CurrentRoom.Cells;

            HexCoord hexCoord = HexCoord.GetClockwise(caster.PlacedCell.Position, target.Position);
            if (cells.ContainsKey(hexCoord))
                result.Add(cells[hexCoord]);

            hexCoord = HexCoord.GetCounterClockwise(caster.PlacedCell.Position, target.Position);
            if (cells.ContainsKey(hexCoord))
                result.Add(cells[hexCoord]);

            result.Add(target);

            return result;
        }
    }
}
