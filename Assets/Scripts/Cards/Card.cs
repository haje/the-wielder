﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Wielder.Item;
using Wielder.Maps;
using Wielder.Passive;
using Wielder.Passive.Triggers;
using Wielder.Visual;

namespace Wielder.Cards
{
    public enum CardClass
    {
        CC0,
        CC1,
        CC2,
        CC3,
        CC4,
        CC5,
        CC6,
        CC7,
        CC8
    }

    public partial class Card : IDamageDealer, IItem
    {
        public readonly CardBase BaseCardData;
        public IItemOwner Owner
        {
            get; set;
        }

        private List<CardModifier> modifiers = new List<CardModifier>();

        public ReadOnlyCollection<CardModifier> Modifiers
        {
            get { return modifiers.AsReadOnly(); }
        }

        public string Name { get; private set; }
        public string Description { get; private set; }
        public int Cost
        {
            get
            {
                int result = BaseCardData.Cost;

                foreach (IPassiveOwner listener in GameManager.PassiveOwners)
                    foreach (var trigger in listener.PassiveHandler.CostReplacement)
                    {
                        if (trigger.Condition(result, this))
                            result = trigger.Effect(result, this);
                    }

                return Mathf.Max(0, result);
            }
        }
        public int Range { get; private set; }

        public int Price
        {
            get
            {
                return 3; //임시 코드
            }
        }

        public Sprite Image
        {
            get
            {
                return BaseCardData.CardImage;
            }
        }

        // 나중에 언젠가 TargetConditions, Effect, VisualEffect, CardImage도 모디파이어의 타겟으로 추가할지도

        public Card(string fileName)
            : this(FileResourceManager.GetCardBase(fileName))
        { }

        public Card(CardBase cardData)
        {
            BaseCardData = cardData;
            RefreshModifiers();

            if (BaseCardData.Initializer != null)
                BaseCardData.Initializer(this);
        }

        public void Activate(Cell target)
        {
            CardEffectParameter cParam = new CardEffectParameter(this, target);
            ChainObject cObj = new ChainObject(
                () => 
                {
                    LogManager.Instance.Log(new LogParameter(cParam));

                    if (BaseCardData.CustomAreaFunction == null)
                        BaseCardData.GameEffect(cParam);
                    else
                    {
                        foreach (Cell cell in BaseCardData.CustomAreaFunction(cParam.Caster, cParam.TargetCell))
                        {
                            CardEffectParameter newParam = new CardEffectParameter(this, cell);
                            BaseCardData.GameEffect(newParam);
                        }
                    }

                    foreach (IPassiveOwner listener in GameManager.PassiveOwners)
                        foreach (var trigger in listener.PassiveHandler.AfterCast)
                        {
                            if (trigger.Condition(listener, cParam))
                                GameManager.AddTriggerToQueue(new TriggerQueueObject(trigger.GenerateChainObject(listener, cParam),
                                    GeneralVisualEffectContainer.GenerateTempFlashEffect(listener)));
                        }
                },
                () => BaseCardData.VisualEffect(cParam), cParam);
            GameManager.PushBackToChain(cObj);

            foreach (IPassiveOwner listener in GameManager.PassiveOwners)
                foreach (var trigger in listener.PassiveHandler.BeforeCast)
                {
                    if (trigger.Condition(listener, cParam))
                        GameManager.AddTriggerToQueue(new TriggerQueueObject(trigger.GenerateChainObject(listener, cParam),
                            GeneralVisualEffectContainer.GenerateTempFlashEffect(listener), cObj));
                }
        }

        private void RefreshModifiers()
        {
            Name = BaseCardData.Name;
            Description = BaseCardData.Description;
            Range = BaseCardData.Range;

            foreach (CardModifier modifier in Modifiers)
                modifier.Effect(modifier);
        }

        public void AddModifier(CardModifier modifier)
        {
            modifier.Owner = this;
            if (modifier.Source != null)
            {
                int i;
                for (i = 0; i < modifiers.Count; i++)
                {
                    if (modifiers[i].Source == null)
                        break;
                }
                modifiers.Insert(i, modifier);
            }
            else
                modifiers.Add(modifier);

            GameManager.AddPassiveOwner(modifier);
            RefreshModifiers();
        }

        public void RemoveModifier(CardModifier modifier)
        {
            modifiers.Remove(modifier);
            GameManager.RemovePassiveOwner(modifier);
            RefreshModifiers();
        }
    }
}
