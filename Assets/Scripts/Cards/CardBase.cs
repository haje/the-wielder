﻿using System;
using UnityEngine;
using Wielder.Item;
using Wielder.Maps;

namespace Wielder.Cards
{
    /// <summary>
    /// 카드에 대한 정보. JSON으로부터 읽어들여 정보 기록. 이후 카드를 만들 때 이 데이터로부터 구성함.
    /// </summary>
    [Serializable]
    public class CardBase : SerializableBase
    {
        public readonly string Name;
        public readonly int Cost;
        public readonly int MoneyCost;
        public readonly int Range;
        public readonly int EffectRange;
        public readonly CellConditions TargetConditions;
        public readonly CardGameEffect GameEffect;
        public readonly int EffectParam1;
        public readonly int EffectParam2;
        public readonly CardVisualEffect VisualEffect;
        public readonly Sprite CardImage;
        public readonly Sprite FloorImage;
        public readonly string Description;
        public readonly CardClass CardClass;
        public readonly CardTerms CardTerms;
        public readonly CardClassTerms CardClassTerms;
        public readonly Rarity Rarity;
        public readonly bool HasTarget;
        public readonly CardInitializerEffect Initializer;
        public readonly TargetConditionCheck CustomTargetCheckFunction;
        public readonly CardEffectArea CustomAreaFunction;

        [SerializeField]
        private string name;

        [SerializeField]
        private string text;

        [SerializeField]
        private int cost;

        [SerializeField]
        private int moneyCost;

        [SerializeField]
        private int range;

        [SerializeField]
        private CellConditions targetConditions;

        [SerializeField]
        private string effectFunctionName;

        [SerializeField]
        private int effectParam1;

        [SerializeField]
        private int effectParam2;

        [SerializeField]
        private string visualEffectFunctionName;

        [SerializeField]
        private string cardImageFileName;

        [SerializeField]
        private string floorImageFileName;

        [SerializeField]
        private string description;

        [SerializeField]
        private int cardClass;

        [SerializeField]
        private int cardTerms;

        [SerializeField]
        private int cardClassTerms;

        [SerializeField]
        private Rarity rarity;

        [SerializeField]
        private bool hasTarget;

        [SerializeField]
        private string initializerFunctionName;

        [SerializeField]
        private string customTargetCheckFunctionName;

        [SerializeField]
        private string customAreaFunctionName;

        protected override string FileDirectory
        {
            get
            {
                return "Cards/";
            }
        }

        public CardBase(string fileName)
            : base(fileName)
        {
            Name = name;
            Cost = cost;
            MoneyCost = moneyCost;
            Range = range;
            TargetConditions = targetConditions;
            GameEffect = CardGameEffectContainer.GetEffectFromName(effectFunctionName);
            EffectParam1 = effectParam1;
            EffectParam2 = effectParam2;
            VisualEffect = Visual.CardVisualEffectContainer.GetVisualEffectFromName(visualEffectFunctionName);
            CardImage = FileResourceManager.GetSprite(cardImageFileName);
            Description = description;
            CardClass = (CardClass)cardClass;
            CardTerms = (CardTerms)cardTerms;
            CardClassTerms = (CardClassTerms)cardClassTerms;
            Rarity = rarity;
            HasTarget = hasTarget;
            if (initializerFunctionName != null)
                Initializer = Card.CardInitializerEffectContainer.GetUnitInitializerEffectFromName(initializerFunctionName);
            if (customTargetCheckFunctionName != null)
                CustomTargetCheckFunction = CardConditionContainer.GetConditionFromName(customTargetCheckFunctionName);
            if (customAreaFunctionName != null)
                CustomAreaFunction = CardEffectAreaContainer.GetAreaFromName(customAreaFunctionName);
            if (floorImageFileName != null)
                FloorImage = FileResourceManager.GetSprite(floorImageFileName);

            FileResourceManager.AddCardToDictionary(this, fileName);
        }
    }
}
