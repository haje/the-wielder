﻿using System;
using Wielder.Passive.Auras;
using Wielder.Passive.Replacements;

namespace Wielder.Cards
{
    public partial class Card
    {
        public static class CardModifierEffectContainer
        {
            public static CardModifierEffect GetUnitModifierEffectFromName(string functionName)
            {
                return DelegateHelper.GetDelegateFromName<CardModifierEffect>
                    (typeof(CardModifierEffectContainer), functionName);
            }

            public static void NoModifierEffect(CardModifier modifier)
            { }            
        }
    }
}
