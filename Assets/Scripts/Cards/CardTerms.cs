﻿using System;

namespace Wielder.Cards
{
    [Flags]
    public enum CardTerms
    {
        NoTerm = 0,
        Burn = 0x1, // 화상
        Bind = 0x2, // 속박
        Stun = 0x4, // 기절
        Overwhelm = 0x8, // 압도
        Bleed = 0x10, // 출혈 ?
        Penetrate = 0x20, // 관통 ?
        Silence = 0x40, // 침묵 ?
        Curse = 0x80 // 저주 ?
    }

    [Flags]
    public enum CardClassTerms
    {
        NoClassTerm = 0, // 생존술
        CCT1 = 0x1, // 교전술
        CCT2 = 0x2, // 사격술
        CCT3 = 0x4, // 대지 마법
        CCT4 = 0x8, // 가호
        CCT5 = 0x10, // 주술
        CCT6 = 0x20, // 지략
        CCT7 = 0x40 // 금전주의
    }

    public static class CardTermHelper
    {
        public static bool HasFlag(this CardTerms cardTerms, CardTerms flagToCheck)
        {
            return (cardTerms & flagToCheck) != 0;
        }

        public static bool HasFlag(this CardClassTerms cardClassTerms, CardClassTerms flagToCheck)
        {
            return (cardClassTerms & flagToCheck) != 0;
        }
    }
}

