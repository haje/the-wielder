﻿using Wielder.Units;
using Wielder.Maps;

namespace Wielder.Cards
{
    public static class CardConditionContainer
    {
        public static TargetConditionCheck GetConditionFromName(string functionName)
        {
            return DelegateHelper.GetDelegateFromName<TargetConditionCheck>(typeof(CardConditionContainer), functionName);
        }

        /*static bool Explosion(Unit unit, Cell cell, bool onlyRange)
        {
            int distance = cell.GetDistanceFrom(unit.PlacedCell);
            if (distance == 2 || distance == 3)
                return true;

            return false;
        }*/

        static bool Swing(Unit unit, Cell cell, bool onlyRange)
        {
            if (cell.GetDistanceFrom(unit.PlacedCell) == 1)
                return true;

            return false;
        }
    }
}
