﻿using Wielder.Maps;
using Wielder.Units;

namespace Wielder.Cards
{
    public class CardEffectParameter
    {
        public Card Card;
        public Unit Caster;
        public Unit TargetUnit;
        public Cell TargetCell;
        public int Param1;
        public int Param2;

        public CardEffectParameter(Card card, Unit mainUnit, Cell targetCell, Unit targetUnit, int param1, int param2)
        {
            Card = card;
            Caster = mainUnit;
            TargetUnit = targetUnit;
            TargetCell = targetCell;
            Param1 = param1;
            Param2 = param2;
        }

        public CardEffectParameter(Card card, Cell targetCell)
            : this(card, (Unit)card.Owner, targetCell, targetCell == null ? null : targetCell.PlacedUnit,
                  card.BaseCardData.EffectParam1, card.BaseCardData.EffectParam2)
        {

        }
    }
}
