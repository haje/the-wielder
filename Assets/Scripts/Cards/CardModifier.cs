﻿using Wielder.Passive;

namespace Wielder.Cards
{
    public class CardModifier : IPassiveOwner
    {
        public CardModifierEffect Effect;

        public string Name;
        public string Description;

        public int RemainingTurn;
        public bool RemoveOnEndCombat;

        private Card owner;
        public Card Owner
        {
            get
            {
                return owner;
            }
            set
            {
                UnityEngine.Debug.Assert(owner == null);
                owner = value;
            }
        }

        public PassiveHandler PassiveHandler
        {
            get; set;
        }

        private IPassiveOwner source;
        public IPassiveOwner Source
        {
            get
            {
                return source;
            }
            set
            {
                UnityEngine.Debug.Assert(source == null);
                source = value;
            }
        }

        private CardModifier(string name, string description, CardModifierEffect effect,
            int remainingTurn = -1, bool removeOnEndCombat = true)
        {
            Name = name;
            Description = description;
            Effect = effect;
            RemainingTurn = remainingTurn;
            RemoveOnEndCombat = removeOnEndCombat;
            PassiveHandler = new PassiveHandler();
        }

        public CardModifier(string name, string description, CardModifierEffect effect)
            : this(name, description, effect, -1, true)
        { }

        public CardModifier(string name, string description, CardModifierEffect effect, int remainingTurn = -1)
            : this(name, description, effect, remainingTurn, true)
        { }

        public CardModifier(string name, string description, CardModifierEffect effect, bool removeOnEndCombat = true)
            : this(name, description, effect, -1, removeOnEndCombat)
        { }

    }
}
