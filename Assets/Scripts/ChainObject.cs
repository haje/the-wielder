﻿using Wielder.Cards;

namespace Wielder
{
    public class ChainObject
    {
        public readonly VisualEffect VisualEffect;
        public readonly GameEffect GameEffect;
        public readonly CardEffectParameter CParam;
        
        public ChainObject(GameEffect gameEffect, VisualEffect visualEffect, CardEffectParameter cParam = null)
        {
            GameEffect = gameEffect;
            VisualEffect = visualEffect;
            CParam = CParam;
        }        
    }
}
